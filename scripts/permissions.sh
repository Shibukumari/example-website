#!/bin/bash


PROJECT_PATH='/var/www/bmmipim/'
WEB_USER='ec2-user'
WEB_GROUP='webgroup'

chown -R ${WEB_USER}:${WEB_GROUP} ${PROJECT_PATH}
chmod -R 664 ${PROJECT_PATH} # files permission to entier directory as its faster 
find ${PROJECT_PATH} -type d -exec chmod 2775 {} \; # set GID bit to retain group permissions
# find ${PROJECT_PATH} -type f -exec chmod 664 {} \;
chmod -R ug+x ${PROJECT_PATH}/htdocs/bin/console
