<?php

return [
	"views" => [
		[
			"treetype" => "object",
			"name" => "Select Options",
			"condition" => NULL,
			"icon" => "/bundles/pimcoreadmin/img/flat-white-icons/pimcore-main-icon-product.svg",
			"id" => 1,
			"rootfolder" => "/BMMI/SelectOptions",
			"showroot" => true,
			"classes" => "51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,68",
			"position" => "left",
			"sort" => "4",
			"expanded" => false,
			'where' => '',

			'treeContextMenu' => [
				'object' => [
					'items' => [
						'add' => 1,
						'addFolder' => 1,
						'importCsv' => 1,
						'paste' => 1,
						'copy' => 1,
						'cut' => 1,
						'publish' => 1,
						'unpublish' => 1,
						'delete' => 1,
						'rename' => 1,
						'searchAndMove' => 1,
						'lock' => 1,
						'unlock' => 1,
						'lockAndPropagate' => 1,
						'unlockAndPropagete' => 1,
						'reload' => 1,
					],
				],
			],
		],

	],
];