<?php 

/** 
* Generated at: 2020-04-04T09:04:58+03:00
* Changed by: system (0)
* IP: 127.0.0.1
*/ 


return Pimcore\Model\DataObject\ClassDefinition\CustomLayout::__set_state(array(
   'id' => '2',
   'name' => 'reviewlayout',
   'description' => '',
   'creationDate' => 1585980283,
   'modificationDate' => 1585980298,
   'userOwner' => 2,
   'userModification' => 0,
   'classId' => '42',
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'fieldtype' => 'panel',
     'labelWidth' => 100,
     'layout' => NULL,
     'border' => false,
     'icon' => NULL,
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => NULL,
     'height' => NULL,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'permissions' => NULL,
     'childs' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Tabpanel::__set_state(array(
         'fieldtype' => 'tabpanel',
         'border' => false,
         'tabPosition' => NULL,
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => NULL,
         'height' => NULL,
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'permissions' => NULL,
         'childs' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 130,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'General Information',
             'type' => NULL,
             'region' => NULL,
             'title' => 'General Information',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Accordion::__set_state(array(
                 'fieldtype' => 'accordion',
                 'border' => false,
                 'name' => 'General',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => 'General',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
                     'fieldtype' => 'panel',
                     'labelWidth' => 130,
                     'layout' => NULL,
                     'border' => false,
                     'icon' => '',
                     'name' => 'Basic Information',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => 'Basic Information',
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                         'fieldtype' => 'fieldcontainer',
                         'labelWidth' => 130,
                         'layout' => 'hbox',
                         'fieldLabel' => '',
                         'name' => 'Field Container',
                         'type' => NULL,
                         'region' => NULL,
                         'title' => '',
                         'width' => NULL,
                         'height' => NULL,
                         'collapsible' => false,
                         'collapsed' => false,
                         'bodyStyle' => '',
                         'datatype' => 'layout',
                         'permissions' => NULL,
                         'childs' => 
                        array (
                          0 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                             'fieldtype' => 'input',
                             'width' => 220,
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'regex' => '',
                             'unique' => false,
                             'showCharCount' => false,
                             'name' => 'name',
                             'title' => 'Name',
                             'tooltip' => '',
                             'mandatory' => true,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => true,
                             'visibleSearch' => true,
                          )),
                          1 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                             'fieldtype' => 'input',
                             'width' => 220,
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'regex' => '',
                             'unique' => true,
                             'showCharCount' => false,
                             'name' => 'sku',
                             'title' => 'SKU',
                             'tooltip' => '',
                             'mandatory' => true,
                             'noteditable' => true,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => true,
                             'visibleSearch' => true,
                          )),
                        ),
                         'locked' => false,
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Wysiwyg::__set_state(array(
                         'fieldtype' => 'wysiwyg',
                         'width' => 720,
                         'height' => 200,
                         'queryColumnType' => 'longtext',
                         'columnType' => 'longtext',
                         'phpdocType' => 'string',
                         'toolbarConfig' => '',
                         'excludeFromSearchIndex' => false,
                         'name' => 'description',
                         'title' => 'Description',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => false,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Wysiwyg::__set_state(array(
                         'fieldtype' => 'wysiwyg',
                         'width' => 720,
                         'height' => 200,
                         'queryColumnType' => 'longtext',
                         'columnType' => 'longtext',
                         'phpdocType' => 'string',
                         'toolbarConfig' => '',
                         'excludeFromSearchIndex' => false,
                         'name' => 'short_description',
                         'title' => 'Short Description',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => false,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      3 => 
                      Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                         'fieldtype' => 'fieldcontainer',
                         'labelWidth' => 130,
                         'layout' => 'hbox',
                         'fieldLabel' => '',
                         'name' => 'Field Container',
                         'type' => NULL,
                         'region' => NULL,
                         'title' => '',
                         'width' => NULL,
                         'height' => NULL,
                         'collapsible' => false,
                         'collapsed' => false,
                         'bodyStyle' => '',
                         'datatype' => 'layout',
                         'permissions' => NULL,
                         'childs' => 
                        array (
                          0 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                             'fieldtype' => 'input',
                             'width' => 220,
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'regex' => '',
                             'unique' => false,
                             'showCharCount' => false,
                             'name' => 'url_key',
                             'title' => 'Url Key',
                             'tooltip' => '',
                             'mandatory' => true,
                             'noteditable' => true,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                          1 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\BooleanSelect::__set_state(array(
                             'fieldtype' => 'booleanSelect',
                             'yesLabel' => 'Active',
                             'noLabel' => 'Inactive',
                             'emptyLabel' => '',
                             'options' => 
                            array (
                              0 => 
                              array (
                                'key' => '',
                                'value' => 0,
                              ),
                              1 => 
                              array (
                                'key' => 'Active',
                                'value' => 1,
                              ),
                              2 => 
                              array (
                                'key' => 'Inactive',
                                'value' => -1,
                              ),
                            ),
                             'width' => 220,
                             'queryColumnType' => 'tinyint(1) null',
                             'columnType' => 'tinyint(1) null',
                             'phpdocType' => 'boolean',
                             'name' => 'status',
                             'title' => 'Status',
                             'tooltip' => '',
                             'mandatory' => true,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => true,
                             'visibleSearch' => false,
                          )),
                        ),
                         'locked' => false,
                      )),
                      4 => 
                      Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                         'fieldtype' => 'fieldcontainer',
                         'labelWidth' => 130,
                         'layout' => 'hbox',
                         'fieldLabel' => '',
                         'name' => 'Field Container',
                         'type' => NULL,
                         'region' => NULL,
                         'title' => '',
                         'width' => NULL,
                         'height' => NULL,
                         'collapsible' => false,
                         'collapsed' => false,
                         'bodyStyle' => '',
                         'datatype' => 'layout',
                         'permissions' => NULL,
                         'childs' => 
                        array (
                          0 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                             'fieldtype' => 'select',
                             'options' => 
                            array (
                              0 => 
                              array (
                                'key' => 'Catalog, Search',
                                'value' => 'Catalog, Search',
                              ),
                              1 => 
                              array (
                                'key' => 'Not Visible Individually',
                                'value' => 'Not Visible Individually',
                              ),
                              2 => 
                              array (
                                'key' => 'Search',
                                'value' => 'Search',
                              ),
                              3 => 
                              array (
                                'key' => 'Catalog',
                                'value' => 'Catalog',
                              ),
                            ),
                             'width' => 220,
                             'defaultValue' => '',
                             'optionsProviderClass' => '',
                             'optionsProviderData' => '',
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'dynamicOptions' => false,
                             'name' => 'visibility',
                             'title' => 'Visibility',
                             'tooltip' => '',
                             'mandatory' => true,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => true,
                             'visibleSearch' => false,
                          )),
                          1 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                             'fieldtype' => 'select',
                             'options' => 
                            array (
                              0 => 
                              array (
                                'key' => 'yes',
                                'value' => 'yes',
                              ),
                              1 => 
                              array (
                                'key' => 'no',
                                'value' => 'no',
                              ),
                            ),
                             'width' => 220,
                             'defaultValue' => '',
                             'optionsProviderClass' => '',
                             'optionsProviderData' => '',
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'dynamicOptions' => false,
                             'name' => 'is_active_in_pim',
                             'title' => 'is_active_in_pim',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                        ),
                         'locked' => false,
                      )),
                      5 => 
                      Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                         'fieldtype' => 'fieldcontainer',
                         'labelWidth' => 130,
                         'layout' => 'hbox',
                         'fieldLabel' => '',
                         'name' => 'Field Container',
                         'type' => NULL,
                         'region' => NULL,
                         'title' => '',
                         'width' => NULL,
                         'height' => NULL,
                         'collapsible' => false,
                         'collapsed' => false,
                         'bodyStyle' => '',
                         'datatype' => 'layout',
                         'permissions' => NULL,
                         'childs' => 
                        array (
                          0 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                             'fieldtype' => 'select',
                             'options' => 
                            array (
                              0 => 
                              array (
                                'key' => 'None',
                                'value' => '0',
                              ),
                              1 => 
                              array (
                                'key' => 'Taxable Goods',
                                'value' => '2',
                              ),
                              2 => 
                              array (
                                'key' => 'Refund Adjustments',
                                'value' => '4',
                              ),
                              3 => 
                              array (
                                'key' => 'Gift Options',
                                'value' => '5',
                              ),
                              4 => 
                              array (
                                'key' => 'Order Gift Wrapping',
                                'value' => '6',
                              ),
                              5 => 
                              array (
                                'key' => 'Item Gift Wrapping',
                                'value' => '7',
                              ),
                              6 => 
                              array (
                                'key' => 'Printed Gift Card',
                                'value' => '8',
                              ),
                              7 => 
                              array (
                                'key' => 'Reward Points',
                                'value' => '9',
                              ),
                            ),
                             'width' => 220,
                             'defaultValue' => '',
                             'optionsProviderClass' => '',
                             'optionsProviderData' => '',
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'dynamicOptions' => false,
                             'name' => 'tax_class_id',
                             'title' => 'Tax Class',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                          1 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                             'fieldtype' => 'multiselect',
                             'options' => 
                            array (
                              0 => 
                              array (
                                'key' => 'Base',
                                'value' => 'base',
                              ),
                              1 => 
                              array (
                                'key' => 'Bmmishops',
                                'value' => 'bmmishops',
                              ),
                              2 => 
                              array (
                                'key' => 'Alosra',
                                'value' => 'alosra',
                              ),
                              3 => 
                              array (
                                'key' => 'My1883',
                                'value' => 'my1883',
                              ),
                            ),
                             'width' => 220,
                             'height' => '',
                             'maxItems' => '',
                             'renderType' => 'list',
                             'optionsProviderClass' => 'BmmiBundle\\StoreBundle\\Website\\WebsitesOptionsProvider',
                             'optionsProviderData' => '',
                             'queryColumnType' => 'text',
                             'columnType' => 'text',
                             'phpdocType' => 'array',
                             'dynamicOptions' => false,
                             'name' => 'websites',
                             'title' => 'Product Websites',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                        ),
                         'locked' => false,
                      )),
                      6 => 
                      Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                         'fieldtype' => 'fieldcontainer',
                         'labelWidth' => 130,
                         'layout' => 'hbox',
                         'fieldLabel' => '',
                         'name' => 'Field Container',
                         'type' => NULL,
                         'region' => NULL,
                         'title' => '',
                         'width' => NULL,
                         'height' => NULL,
                         'collapsible' => false,
                         'collapsed' => false,
                         'bodyStyle' => '',
                         'datatype' => 'layout',
                         'permissions' => NULL,
                         'childs' => 
                        array (
                          0 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Date::__set_state(array(
                             'fieldtype' => 'date',
                             'queryColumnType' => 'bigint(20)',
                             'columnType' => 'bigint(20)',
                             'phpdocType' => '\\Carbon\\Carbon',
                             'defaultValue' => NULL,
                             'useCurrentDate' => false,
                             'name' => 'new_start_date',
                             'title' => 'New Start Date',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                          1 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Date::__set_state(array(
                             'fieldtype' => 'date',
                             'queryColumnType' => 'bigint(20)',
                             'columnType' => 'bigint(20)',
                             'phpdocType' => '\\Carbon\\Carbon',
                             'defaultValue' => NULL,
                             'useCurrentDate' => false,
                             'name' => 'new_end_date',
                             'title' => 'New End Date',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                        ),
                         'locked' => false,
                      )),
                      7 => 
                      Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                         'fieldtype' => 'fieldcontainer',
                         'labelWidth' => 100,
                         'layout' => 'hbox',
                         'fieldLabel' => '',
                         'name' => 'Field Container',
                         'type' => NULL,
                         'region' => NULL,
                         'title' => '',
                         'width' => NULL,
                         'height' => NULL,
                         'collapsible' => false,
                         'collapsed' => false,
                         'bodyStyle' => '',
                         'datatype' => 'layout',
                         'permissions' => NULL,
                         'childs' => 
                        array (
                          0 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\ManyToManyObjectRelation::__set_state(array(
                             'fieldtype' => 'manyToManyObjectRelation',
                             'width' => 710,
                             'height' => '',
                             'maxItems' => '',
                             'queryColumnType' => 'text',
                             'phpdocType' => 'array',
                             'relationType' => true,
                             'visibleFields' => 'name,fullpath,id',
                             'optimizedAdminLoading' => false,
                             'visibleFieldDefinitions' => 
                            array (
                            ),
                             'lazyLoading' => true,
                             'classes' => 
                            array (
                              0 => 
                              array (
                                'classes' => 'BMMICategory',
                              ),
                            ),
                             'pathFormatterClass' => '',
                             'name' => 'category_ids',
                             'title' => 'Categories',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                        ),
                         'locked' => false,
                      )),
                      8 => 
                      Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                         'fieldtype' => 'fieldcontainer',
                         'labelWidth' => 130,
                         'layout' => 'hbox',
                         'fieldLabel' => '',
                         'name' => 'Field Container',
                         'type' => NULL,
                         'region' => NULL,
                         'title' => '',
                         'width' => NULL,
                         'height' => NULL,
                         'collapsible' => false,
                         'collapsed' => false,
                         'bodyStyle' => '',
                         'datatype' => 'layout',
                         'permissions' => NULL,
                         'childs' => 
                        array (
                          0 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                             'fieldtype' => 'input',
                             'width' => 220,
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'regex' => '',
                             'unique' => false,
                             'showCharCount' => false,
                             'name' => 'promo_text',
                             'title' => 'Promo Text',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                          1 => 
                          Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                             'fieldtype' => 'input',
                             'width' => 220,
                             'queryColumnType' => 'varchar',
                             'columnType' => 'varchar',
                             'columnLength' => 190,
                             'phpdocType' => 'string',
                             'regex' => '',
                             'unique' => false,
                             'showCharCount' => false,
                             'name' => 'parent_sku',
                             'title' => 'parent_sku',
                             'tooltip' => '',
                             'mandatory' => false,
                             'noteditable' => false,
                             'index' => false,
                             'locked' => false,
                             'style' => '',
                             'permissions' => NULL,
                             'datatype' => 'data',
                             'relationType' => false,
                             'invisible' => false,
                             'visibleGridView' => false,
                             'visibleSearch' => false,
                          )),
                        ),
                         'locked' => false,
                      )),
                    ),
                     'locked' => false,
                  )),
                ),
                 'locked' => false,
              )),
            ),
             'locked' => false,
          )),
          1 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 130,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'Product Details',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Product Details',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                     'fieldtype' => 'multiselect',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Blend',
                        'value' => 'Blend',
                      ),
                      1 => 
                      array (
                        'key' => 'Cabernet Franc',
                        'value' => 'Cabernet Franc',
                      ),
                      2 => 
                      array (
                        'key' => 'Cabernet Sauvignon',
                        'value' => 'Cabernet Sauvignon',
                      ),
                      3 => 
                      array (
                        'key' => 'Canaiolo Bianco',
                        'value' => 'Canaiolo Bianco',
                      ),
                      4 => 
                      array (
                        'key' => 'Chardonnay',
                        'value' => 'Chardonnay',
                      ),
                      5 => 
                      array (
                        'key' => 'Cinsault',
                        'value' => 'Cinsault',
                      ),
                      6 => 
                      array (
                        'key' => 'Cortese',
                        'value' => 'Cortese',
                      ),
                      7 => 
                      array (
                        'key' => 'Dolcetto',
                        'value' => 'Dolcetto',
                      ),
                      8 => 
                      array (
                        'key' => 'Garnacha Tinta',
                        'value' => 'Garnacha Tinta',
                      ),
                      9 => 
                      array (
                        'key' => 'Graciano',
                        'value' => 'Graciano',
                      ),
                      10 => 
                      array (
                        'key' => 'Grechetto',
                        'value' => 'Grechetto',
                      ),
                      11 => 
                      array (
                        'key' => 'Mazuelo',
                        'value' => 'Mazuelo',
                      ),
                      12 => 
                      array (
                        'key' => 'Merlot',
                        'value' => 'Merlot',
                      ),
                      13 => 
                      array (
                        'key' => 'Nebbiolo',
                        'value' => 'Nebbiolo',
                      ),
                      14 => 
                      array (
                        'key' => 'Petit Verdot',
                        'value' => 'Petit Verdot',
                      ),
                      15 => 
                      array (
                        'key' => 'Pinot Noir',
                        'value' => 'Pinot Noir',
                      ),
                      16 => 
                      array (
                        'key' => 'Procanico',
                        'value' => 'Procanico',
                      ),
                      17 => 
                      array (
                        'key' => 'Sauvignon Blanc',
                        'value' => 'Sauvignon Blanc',
                      ),
                      18 => 
                      array (
                        'key' => 'Semillon',
                        'value' => 'Semillon',
                      ),
                      19 => 
                      array (
                        'key' => 'Shiraz',
                        'value' => 'Shiraz',
                      ),
                      20 => 
                      array (
                        'key' => 'Syrah',
                        'value' => 'Syrah',
                      ),
                      21 => 
                      array (
                        'key' => 'Tempranillo',
                        'value' => 'Tempranillo',
                      ),
                      22 => 
                      array (
                        'key' => 'Zinfandel',
                        'value' => 'Zinfandel',
                      ),
                    ),
                     'width' => 220,
                     'height' => '',
                     'maxItems' => '',
                     'renderType' => 'list',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'text',
                     'columnType' => 'text',
                     'phpdocType' => 'array',
                     'dynamicOptions' => false,
                     'name' => 'grapevariety',
                     'title' => 'Grapevariety',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => true,
                     'visibleSearch' => true,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                     'fieldtype' => 'multiselect',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Standard Mode',
                        'value' => 'Standard Mode',
                      ),
                      1 => 
                      array (
                        'key' => 'Express Mode',
                        'value' => 'Express Mode',
                      ),
                      2 => 
                      array (
                        'key' => 'Pickup',
                        'value' => 'Pickup',
                      ),
                    ),
                     'width' => 220,
                     'height' => '',
                     'maxItems' => '',
                     'renderType' => 'list',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'text',
                     'columnType' => 'text',
                     'phpdocType' => 'array',
                     'dynamicOptions' => false,
                     'name' => 'delivery_type',
                     'title' => 'Delivery Type',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'pack',
                     'title' => 'Pack',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'packsize',
                     'title' => 'PackSize',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'producer',
                     'title' => 'Producer',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 590,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'brand_info',
                     'title' => 'Brand Info',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'vintage',
                     'title' => 'Vintage',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 580,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'taste',
                     'title' => 'Tasting Notes',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => '',
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => false,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'item_weight',
                     'title' => 'Item Weight',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'NA',
                        'value' => 'NA',
                      ),
                      1 => 
                      array (
                        'key' => 'L',
                        'value' => 'L',
                      ),
                      2 => 
                      array (
                        'key' => 'Kg',
                        'value' => 'Kg',
                      ),
                      3 => 
                      array (
                        'key' => 'ml',
                        'value' => 'ml',
                      ),
                      4 => 
                      array (
                        'key' => 'g',
                        'value' => 'g',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'item_volume',
                     'title' => 'Item Volume',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'vbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 580,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'serving_suggestions',
                     'title' => 'Serving Suggestions',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              6 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'vbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 590,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'allergy_advise',
                     'title' => 'Allergy Advise',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 590,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'ingredients',
                     'title' => 'Ingredients',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              7 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'vbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => 'Field Container',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Accessories',
                        'value' => 'Accessories',
                      ),
                      1 => 
                      array (
                        'key' => 'Activity and Picture Books',
                        'value' => 'Activity and Picture Books',
                      ),
                      2 => 
                      array (
                        'key' => 'Adaptor',
                        'value' => 'Adaptor',
                      ),
                      3 => 
                      array (
                        'key' => 'Animals&figures',
                        'value' => 'Animals&figures',
                      ),
                      4 => 
                      array (
                        'key' => 'Anti Ageing',
                        'value' => 'Anti Ageing',
                      ),
                      5 => 
                      array (
                        'key' => 'Apple Corers',
                        'value' => 'Apple Corers',
                      ),
                      6 => 
                      array (
                        'key' => 'Baby & Children Health & Beauty',
                        'value' => 'Baby & Children Health & Beauty',
                      ),
                      7 => 
                      array (
                        'key' => 'Baby Carriers',
                        'value' => 'Baby Carriers',
                      ),
                      8 => 
                      array (
                        'key' => 'Baby Cream',
                        'value' => 'Baby Cream',
                      ),
                      9 => 
                      array (
                        'key' => 'Baby Food',
                        'value' => 'Baby Food',
                      ),
                      10 => 
                      array (
                        'key' => 'Baby Lotion',
                        'value' => 'Baby Lotion',
                      ),
                      11 => 
                      array (
                        'key' => 'Baby Monitors',
                        'value' => 'Baby Monitors',
                      ),
                      12 => 
                      array (
                        'key' => 'Baby Oil',
                        'value' => 'Baby Oil',
                      ),
                      13 => 
                      array (
                        'key' => 'Baby Powder',
                        'value' => 'Baby Powder',
                      ),
                      14 => 
                      array (
                        'key' => 'Baby Shampoo',
                        'value' => 'Baby Shampoo',
                      ),
                      15 => 
                      array (
                        'key' => 'Baby Soap',
                        'value' => 'Baby Soap',
                      ),
                      16 => 
                      array (
                        'key' => 'Baby Sunblock',
                        'value' => 'Baby Sunblock',
                      ),
                      17 => 
                      array (
                        'key' => 'Baby Wipes',
                        'value' => 'Baby Wipes',
                      ),
                      18 => 
                      array (
                        'key' => 'Backpacks & Lunch kits',
                        'value' => 'Backpacks & Lunch kits',
                      ),
                      19 => 
                      array (
                        'key' => 'Backpacks & Lunch kits',
                        'value' => 'Backpacks & Lunch kits',
                      ),
                      20 => 
                      array (
                        'key' => 'Baking & Cooking Supplies',
                        'value' => 'Baking & Cooking Supplies',
                      ),
                      21 => 
                      array (
                        'key' => 'Baking Mixes',
                        'value' => 'Baking Mixes',
                      ),
                      22 => 
                      array (
                        'key' => 'Baking Soda, Powder & Starch',
                        'value' => 'Baking Soda, Powder & Starch',
                      ),
                      23 => 
                      array (
                        'key' => 'Bassinets',
                        'value' => 'Bassinets',
                      ),
                      24 => 
                      array (
                        'key' => 'Basters',
                        'value' => 'Basters',
                      ),
                      25 => 
                      array (
                        'key' => 'Bath Accessories',
                        'value' => 'Bath Accessories',
                      ),
                      26 => 
                      array (
                        'key' => 'Bath Chair',
                        'value' => 'Bath Chair',
                      ),
                      27 => 
                      array (
                        'key' => 'Bath Sets',
                        'value' => 'Bath Sets',
                      ),
                      28 => 
                      array (
                        'key' => 'Bath Tub',
                        'value' => 'Bath Tub',
                      ),
                      29 => 
                      array (
                        'key' => 'Bathrobe',
                        'value' => 'Bathrobe',
                      ),
                      30 => 
                      array (
                        'key' => 'Batteries',
                        'value' => 'Batteries',
                      ),
                      31 => 
                      array (
                        'key' => 'Beads & Jewelry Kits',
                        'value' => 'Beads & Jewelry Kits',
                      ),
                      32 => 
                      array (
                        'key' => 'Beaker',
                        'value' => 'Beaker',
                      ),
                      33 => 
                      array (
                        'key' => 'Beauty & Fashion',
                        'value' => 'Beauty & Fashion',
                      ),
                      34 => 
                      array (
                        'key' => 'Bedrail',
                        'value' => 'Bedrail',
                      ),
                      35 => 
                      array (
                        'key' => 'Beef',
                        'value' => 'Beef',
                      ),
                      36 => 
                      array (
                        'key' => 'Bibs',
                        'value' => 'Bibs',
                      ),
                      37 => 
                      array (
                        'key' => 'Bikes',
                        'value' => 'Bikes',
                      ),
                      38 => 
                      array (
                        'key' => 'Bikes, Scooters & Ride-Ons',
                        'value' => 'Bikes, Scooters & Ride-Ons',
                      ),
                      39 => 
                      array (
                        'key' => 'Bikini Trimmers',
                        'value' => 'Bikini Trimmers',
                      ),
                      40 => 
                      array (
                        'key' => 'Biscuit',
                        'value' => 'Biscuit',
                      ),
                      41 => 
                      array (
                        'key' => 'Black Grapes',
                        'value' => 'Black Grapes',
                      ),
                      42 => 
                      array (
                        'key' => 'Blanket',
                        'value' => 'Blanket',
                      ),
                      43 => 
                      array (
                        'key' => 'Blenders',
                        'value' => 'Blenders',
                      ),
                      44 => 
                      array (
                        'key' => 'Blubs',
                        'value' => 'Blubs',
                      ),
                      45 => 
                      array (
                        'key' => 'Body Groomers',
                        'value' => 'Body Groomers',
                      ),
                      46 => 
                      array (
                        'key' => 'Body Lotion',
                        'value' => 'Body Lotion',
                      ),
                      47 => 
                      array (
                        'key' => 'Body Wash',
                        'value' => 'Body Wash',
                      ),
                      48 => 
                      array (
                        'key' => 'Booster Seats',
                        'value' => 'Booster Seats',
                      ),
                      49 => 
                      array (
                        'key' => 'Booties',
                        'value' => 'Booties',
                      ),
                      50 => 
                      array (
                        'key' => 'Bottle Cleaning Brush',
                        'value' => 'Bottle Cleaning Brush',
                      ),
                      51 => 
                      array (
                        'key' => 'Bottles',
                        'value' => 'Bottles',
                      ),
                      52 => 
                      array (
                        'key' => 'Bouncers',
                        'value' => 'Bouncers',
                      ),
                      53 => 
                      array (
                        'key' => 'Bowls',
                        'value' => 'Bowls',
                      ),
                      54 => 
                      array (
                        'key' => 'Bread Knives',
                        'value' => 'Bread Knives',
                      ),
                      55 => 
                      array (
                        'key' => 'Bread Makers',
                        'value' => 'Bread Makers',
                      ),
                      56 => 
                      array (
                        'key' => 'Breast Pumps',
                        'value' => 'Breast Pumps',
                      ),
                      57 => 
                      array (
                        'key' => 'Bricks & Blocks',
                        'value' => 'Bricks & Blocks',
                      ),
                      58 => 
                      array (
                        'key' => 'Bubble Launcher',
                        'value' => 'Bubble Launcher',
                      ),
                      59 => 
                      array (
                        'key' => 'Building Sets',
                        'value' => 'Building Sets',
                      ),
                      60 => 
                      array (
                        'key' => 'Burpcloth',
                        'value' => 'Burpcloth',
                      ),
                      61 => 
                      array (
                        'key' => 'Butter & Margarin',
                        'value' => 'Butter & Margarin',
                      ),
                      62 => 
                      array (
                        'key' => 'Butter Milk',
                        'value' => 'Butter Milk',
                      ),
                      63 => 
                      array (
                        'key' => 'Cake Decorations',
                        'value' => 'Cake Decorations',
                      ),
                      64 => 
                      array (
                        'key' => 'Can Openers',
                        'value' => 'Can Openers',
                      ),
                      65 => 
                      array (
                        'key' => 'Canned & Dried Tomatoes',
                        'value' => 'Canned & Dried Tomatoes',
                      ),
                      66 => 
                      array (
                        'key' => 'Canned & Powdered Milk',
                        'value' => 'Canned & Powdered Milk',
                      ),
                      67 => 
                      array (
                        'key' => 'Canned Beans',
                        'value' => 'Canned Beans',
                      ),
                      68 => 
                      array (
                        'key' => 'Canned Fruit',
                        'value' => 'Canned Fruit',
                      ),
                      69 => 
                      array (
                        'key' => 'Canned Meat & Poultry',
                        'value' => 'Canned Meat & Poultry',
                      ),
                      70 => 
                      array (
                        'key' => 'Canned Tuna & Seafood',
                        'value' => 'Canned Tuna & Seafood',
                      ),
                      71 => 
                      array (
                        'key' => 'Canned Vegetables',
                        'value' => 'Canned Vegetables',
                      ),
                      72 => 
                      array (
                        'key' => 'Cap',
                        'value' => 'Cap',
                      ),
                      73 => 
                      array (
                        'key' => 'Cat Food',
                        'value' => 'Cat Food',
                      ),
                      74 => 
                      array (
                        'key' => 'Caviar',
                        'value' => 'Caviar',
                      ),
                      75 => 
                      array (
                        'key' => 'Cereal',
                        'value' => 'Cereal',
                      ),
                      76 => 
                      array (
                        'key' => 'Changing Mat',
                        'value' => 'Changing Mat',
                      ),
                      77 => 
                      array (
                        'key' => 'Cheese Knives',
                        'value' => 'Cheese Knives',
                      ),
                      78 => 
                      array (
                        'key' => 'Chicken',
                        'value' => 'Chicken',
                      ),
                      79 => 
                      array (
                        'key' => 'Children\'s Fiction',
                        'value' => 'Children\'s Fiction',
                      ),
                      80 => 
                      array (
                        'key' => 'Children\'s Non-fiction',
                        'value' => 'Children\'s Non-fiction',
                      ),
                      81 => 
                      array (
                        'key' => 'Chips',
                        'value' => 'Chips',
                      ),
                      82 => 
                      array (
                        'key' => 'Chocolate',
                        'value' => 'Chocolate',
                      ),
                      83 => 
                      array (
                        'key' => 'Chopper',
                        'value' => 'Chopper',
                      ),
                      84 => 
                      array (
                        'key' => 'Clay, Dough & Modeling',
                        'value' => 'Clay, Dough & Modeling',
                      ),
                      85 => 
                      array (
                        'key' => 'Cleaning Brushes',
                        'value' => 'Cleaning Brushes',
                      ),
                      86 => 
                      array (
                        'key' => 'Cleaning Equipment',
                        'value' => 'Cleaning Equipment',
                      ),
                      87 => 
                      array (
                        'key' => 'Cleanser',
                        'value' => 'Cleanser',
                      ),
                      88 => 
                      array (
                        'key' => 'Cocktail & Bar Accessories',
                        'value' => 'Cocktail & Bar Accessories',
                      ),
                      89 => 
                      array (
                        'key' => 'Coconut Milk',
                        'value' => 'Coconut Milk',
                      ),
                      90 => 
                      array (
                        'key' => 'Coffee',
                        'value' => 'Coffee',
                      ),
                      91 => 
                      array (
                        'key' => 'Coffee Makers',
                        'value' => 'Coffee Makers',
                      ),
                      92 => 
                      array (
                        'key' => 'Colanders',
                        'value' => 'Colanders',
                      ),
                      93 => 
                      array (
                        'key' => 'Cold, Allergy, Sinus & Flu',
                        'value' => 'Cold, Allergy, Sinus & Flu',
                      ),
                      94 => 
                      array (
                        'key' => 'Collectible Dolls',
                        'value' => 'Collectible Dolls',
                      ),
                      95 => 
                      array (
                        'key' => 'Collectible Vehicles',
                        'value' => 'Collectible Vehicles',
                      ),
                      96 => 
                      array (
                        'key' => 'Condiments & Sandwich Spreads',
                        'value' => 'Condiments & Sandwich Spreads',
                      ),
                      97 => 
                      array (
                        'key' => 'Construction & Model Kits',
                        'value' => 'Construction & Model Kits',
                      ),
                      98 => 
                      array (
                        'key' => 'Cook\'s & Chef\'s Knives',
                        'value' => 'Cook\'s & Chef\'s Knives',
                      ),
                      99 => 
                      array (
                        'key' => 'Cookie Cutters',
                        'value' => 'Cookie Cutters',
                      ),
                      100 => 
                      array (
                        'key' => 'Cookies',
                        'value' => 'Cookies',
                      ),
                      101 => 
                      array (
                        'key' => 'Cooking Forks',
                        'value' => 'Cooking Forks',
                      ),
                      102 => 
                      array (
                        'key' => 'Cooking Oils & Sprays',
                        'value' => 'Cooking Oils & Sprays',
                      ),
                      103 => 
                      array (
                        'key' => 'Cooking Spoons',
                        'value' => 'Cooking Spoons',
                      ),
                      104 => 
                      array (
                        'key' => 'Cooking Wine & Vinegar',
                        'value' => 'Cooking Wine & Vinegar',
                      ),
                      105 => 
                      array (
                        'key' => 'Corkscrews',
                        'value' => 'Corkscrews',
                      ),
                      106 => 
                      array (
                        'key' => 'Cottage Cheese',
                        'value' => 'Cottage Cheese',
                      ),
                      107 => 
                      array (
                        'key' => 'Cotton Balls & Swabs',
                        'value' => 'Cotton Balls & Swabs',
                      ),
                      108 => 
                      array (
                        'key' => 'Couscous',
                        'value' => 'Couscous',
                      ),
                      109 => 
                      array (
                        'key' => 'Crackers',
                        'value' => 'Crackers',
                      ),
                      110 => 
                      array (
                        'key' => 'Craft Kits',
                        'value' => 'Craft Kits',
                      ),
                      111 => 
                      array (
                        'key' => 'Cream',
                        'value' => 'Cream',
                      ),
                      112 => 
                      array (
                        'key' => 'Cups',
                        'value' => 'Cups',
                      ),
                      113 => 
                      array (
                        'key' => 'Curlers, Stylers And Straightener',
                        'value' => 'Curlers, Stylers And Straightener',
                      ),
                      114 => 
                      array (
                        'key' => 'Cushions',
                        'value' => 'Cushions',
                      ),
                      115 => 
                      array (
                        'key' => 'Cutlery Set',
                        'value' => 'Cutlery Set',
                      ),
                      116 => 
                      array (
                        'key' => 'Cutting & Chopping Boards',
                        'value' => 'Cutting & Chopping Boards',
                      ),
                      117 => 
                      array (
                        'key' => 'Decals',
                        'value' => 'Decals',
                      ),
                      118 => 
                      array (
                        'key' => 'Deep Fryers',
                        'value' => 'Deep Fryers',
                      ),
                      119 => 
                      array (
                        'key' => 'Dehumidifer, Air Purifiers',
                        'value' => 'Dehumidifer, Air Purifiers',
                      ),
                      120 => 
                      array (
                        'key' => 'Dental care',
                        'value' => 'Dental care',
                      ),
                      121 => 
                      array (
                        'key' => 'Deodorant',
                        'value' => 'Deodorant',
                      ),
                      122 => 
                      array (
                        'key' => 'Diaper Bags',
                        'value' => 'Diaper Bags',
                      ),
                      123 => 
                      array (
                        'key' => 'Diapers & Wipes',
                        'value' => 'Diapers & Wipes',
                      ),
                      124 => 
                      array (
                        'key' => 'Dips, Spreads & Salsa',
                        'value' => 'Dips, Spreads & Salsa',
                      ),
                      125 => 
                      array (
                        'key' => 'Dish & Cutlery Racks',
                        'value' => 'Dish & Cutlery Racks',
                      ),
                      126 => 
                      array (
                        'key' => 'Dishwashing',
                        'value' => 'Dishwashing',
                      ),
                      127 => 
                      array (
                        'key' => 'Disposable',
                        'value' => 'Disposable',
                      ),
                      128 => 
                      array (
                        'key' => 'Disposal Tub',
                        'value' => 'Disposal Tub',
                      ),
                      129 => 
                      array (
                        'key' => 'Disposal Tub Cassette',
                        'value' => 'Disposal Tub Cassette',
                      ),
                      130 => 
                      array (
                        'key' => 'Dog Food',
                        'value' => 'Dog Food',
                      ),
                      131 => 
                      array (
                        'key' => 'Doll Houses',
                        'value' => 'Doll Houses',
                      ),
                      132 => 
                      array (
                        'key' => 'Dollhouses & Accessories',
                        'value' => 'Dollhouses & Accessories',
                      ),
                      133 => 
                      array (
                        'key' => 'Dolls',
                        'value' => 'Dolls',
                      ),
                      134 => 
                      array (
                        'key' => 'Doodles',
                        'value' => 'Doodles',
                      ),
                      135 => 
                      array (
                        'key' => 'Drawing & Coloring',
                        'value' => 'Drawing & Coloring',
                      ),
                      136 => 
                      array (
                        'key' => 'Dried Beans & Peas',
                        'value' => 'Dried Beans & Peas',
                      ),
                      137 => 
                      array (
                        'key' => 'Drums & Percussion',
                        'value' => 'Drums & Percussion',
                      ),
                      138 => 
                      array (
                        'key' => 'Early Development Toys',
                        'value' => 'Early Development Toys',
                      ),
                      139 => 
                      array (
                        'key' => 'Early learning',
                        'value' => 'Early learning',
                      ),
                      140 => 
                      array (
                        'key' => 'Easels / Art Table',
                        'value' => 'Easels / Art Table',
                      ),
                      141 => 
                      array (
                        'key' => 'Eggs & Egg Substitutes',
                        'value' => 'Eggs & Egg Substitutes',
                      ),
                      142 => 
                      array (
                        'key' => 'Electric Slot Car Racing',
                        'value' => 'Electric Slot Car Racing',
                      ),
                      143 => 
                      array (
                        'key' => 'Energy Saving Bulbs',
                        'value' => 'Energy Saving Bulbs',
                      ),
                      144 => 
                      array (
                        'key' => 'Epilators',
                        'value' => 'Epilators',
                      ),
                      145 => 
                      array (
                        'key' => 'Extension Cord',
                        'value' => 'Extension Cord',
                      ),
                      146 => 
                      array (
                        'key' => 'Extracts & Flavorings',
                        'value' => 'Extracts & Flavorings',
                      ),
                      147 => 
                      array (
                        'key' => 'Eye & Ear Care',
                        'value' => 'Eye & Ear Care',
                      ),
                      148 => 
                      array (
                        'key' => 'Face Wash',
                        'value' => 'Face Wash',
                      ),
                      149 => 
                      array (
                        'key' => 'Fans',
                        'value' => 'Fans',
                      ),
                      150 => 
                      array (
                        'key' => 'Fashion & Design',
                        'value' => 'Fashion & Design',
                      ),
                      151 => 
                      array (
                        'key' => 'Feeding Sets',
                        'value' => 'Feeding Sets',
                      ),
                      152 => 
                      array (
                        'key' => 'Feeding Spoons',
                        'value' => 'Feeding Spoons',
                      ),
                      153 => 
                      array (
                        'key' => 'Feminine Care',
                        'value' => 'Feminine Care',
                      ),
                      154 => 
                      array (
                        'key' => 'First Aid',
                        'value' => 'First Aid',
                      ),
                      155 => 
                      array (
                        'key' => 'Flavoured Milk',
                        'value' => 'Flavoured Milk',
                      ),
                      156 => 
                      array (
                        'key' => 'Flour & Meal',
                        'value' => 'Flour & Meal',
                      ),
                      157 => 
                      array (
                        'key' => 'Food Pots',
                        'value' => 'Food Pots',
                      ),
                      158 => 
                      array (
                        'key' => 'Food Processors & Accessories',
                        'value' => 'Food Processors & Accessories',
                      ),
                      159 => 
                      array (
                        'key' => 'Food Storage & Wraps',
                        'value' => 'Food Storage & Wraps',
                      ),
                      160 => 
                      array (
                        'key' => 'Food Storage Containers',
                        'value' => 'Food Storage Containers',
                      ),
                      161 => 
                      array (
                        'key' => 'Food Warmer',
                        'value' => 'Food Warmer',
                      ),
                      162 => 
                      array (
                        'key' => 'Food and Drink',
                        'value' => 'Food and Drink',
                      ),
                      163 => 
                      array (
                        'key' => 'Foot Care',
                        'value' => 'Foot Care',
                      ),
                      164 => 
                      array (
                        'key' => 'Foot Cream',
                        'value' => 'Foot Cream',
                      ),
                      165 => 
                      array (
                        'key' => 'Fresh Bakery Bread',
                        'value' => 'Fresh Bakery Bread',
                      ),
                      166 => 
                      array (
                        'key' => 'Fresh Fruit & Veggie Trays',
                        'value' => 'Fresh Fruit & Veggie Trays',
                      ),
                      167 => 
                      array (
                        'key' => 'Fresh Fruits',
                        'value' => 'Fresh Fruits',
                      ),
                      168 => 
                      array (
                        'key' => 'Fresh Vegetables',
                        'value' => 'Fresh Vegetables',
                      ),
                      169 => 
                      array (
                        'key' => 'Frosting & Cake Decorations',
                        'value' => 'Frosting & Cake Decorations',
                      ),
                      170 => 
                      array (
                        'key' => 'Frozen Cheese',
                        'value' => 'Frozen Cheese',
                      ),
                      171 => 
                      array (
                        'key' => 'Frozen Chicken',
                        'value' => 'Frozen Chicken',
                      ),
                      172 => 
                      array (
                        'key' => 'Frozen French Fries',
                        'value' => 'Frozen French Fries',
                      ),
                      173 => 
                      array (
                        'key' => 'Frozen Meat',
                        'value' => 'Frozen Meat',
                      ),
                      174 => 
                      array (
                        'key' => 'Frozen Seafood',
                        'value' => 'Frozen Seafood',
                      ),
                      175 => 
                      array (
                        'key' => 'Frozen Turkey',
                        'value' => 'Frozen Turkey',
                      ),
                      176 => 
                      array (
                        'key' => 'Fruit & Vegetable Tools',
                        'value' => 'Fruit & Vegetable Tools',
                      ),
                      177 => 
                      array (
                        'key' => 'Fruit Snacks & Dried Fruit',
                        'value' => 'Fruit Snacks & Dried Fruit',
                      ),
                      178 => 
                      array (
                        'key' => 'Games',
                        'value' => 'Games',
                      ),
                      179 => 
                      array (
                        'key' => 'Games & Puzzles',
                        'value' => 'Games & Puzzles',
                      ),
                      180 => 
                      array (
                        'key' => 'Garlic Presses',
                        'value' => 'Garlic Presses',
                      ),
                      181 => 
                      array (
                        'key' => 'Gas Lighter',
                        'value' => 'Gas Lighter',
                      ),
                      182 => 
                      array (
                        'key' => 'Gift Sets',
                        'value' => 'Gift Sets',
                      ),
                      183 => 
                      array (
                        'key' => 'Gifts',
                        'value' => 'Gifts',
                      ),
                      184 => 
                      array (
                        'key' => 'Gluten Free',
                        'value' => 'Gluten Free',
                      ),
                      185 => 
                      array (
                        'key' => 'Graters & Shavers',
                        'value' => 'Graters & Shavers',
                      ),
                      186 => 
                      array (
                        'key' => 'Gravy',
                        'value' => 'Gravy',
                      ),
                      187 => 
                      array (
                        'key' => 'Grills',
                        'value' => 'Grills',
                      ),
                      188 => 
                      array (
                        'key' => 'Grooming Kits',
                        'value' => 'Grooming Kits',
                      ),
                      189 => 
                      array (
                        'key' => 'Guitars',
                        'value' => 'Guitars',
                      ),
                      190 => 
                      array (
                        'key' => 'Gyms & Playmats',
                        'value' => 'Gyms & Playmats',
                      ),
                      191 => 
                      array (
                        'key' => 'Hair Care',
                        'value' => 'Hair Care',
                      ),
                      192 => 
                      array (
                        'key' => 'Hair Clipper',
                        'value' => 'Hair Clipper',
                      ),
                      193 => 
                      array (
                        'key' => 'Hair Dryer',
                        'value' => 'Hair Dryer',
                      ),
                      194 => 
                      array (
                        'key' => 'Hand Mixers',
                        'value' => 'Hand Mixers',
                      ),
                      195 => 
                      array (
                        'key' => 'Hand Sanitizer',
                        'value' => 'Hand Sanitizer',
                      ),
                      196 => 
                      array (
                        'key' => 'Hand Wash',
                        'value' => 'Hand Wash',
                      ),
                      197 => 
                      array (
                        'key' => 'Health and Well being',
                        'value' => 'Health and Well being',
                      ),
                      198 => 
                      array (
                        'key' => 'Herbs, Spices & Seasonings',
                        'value' => 'Herbs, Spices & Seasonings',
                      ),
                      199 => 
                      array (
                        'key' => 'High Chair',
                        'value' => 'High Chair',
                      ),
                      200 => 
                      array (
                        'key' => 'Hobbies and Games',
                        'value' => 'Hobbies and Games',
                      ),
                      201 => 
                      array (
                        'key' => 'Honey',
                        'value' => 'Honey',
                      ),
                      202 => 
                      array (
                        'key' => 'Horseradish',
                        'value' => 'Horseradish',
                      ),
                      203 => 
                      array (
                        'key' => 'Hot Cocoa & Milk Mixers',
                        'value' => 'Hot Cocoa & Milk Mixers',
                      ),
                      204 => 
                      array (
                        'key' => 'Household Cleaners',
                        'value' => 'Household Cleaners',
                      ),
                      205 => 
                      array (
                        'key' => 'Hummus',
                        'value' => 'Hummus',
                      ),
                      206 => 
                      array (
                        'key' => 'Ice Buckets & Tongs',
                        'value' => 'Ice Buckets & Tongs',
                      ),
                      207 => 
                      array (
                        'key' => 'Ice Cream & Dessert Dishes',
                        'value' => 'Ice Cream & Dessert Dishes',
                      ),
                      208 => 
                      array (
                        'key' => 'Ice Makers',
                        'value' => 'Ice Makers',
                      ),
                      209 => 
                      array (
                        'key' => 'Induction Cookers',
                        'value' => 'Induction Cookers',
                      ),
                      210 => 
                      array (
                        'key' => 'Infant Formula',
                        'value' => 'Infant Formula',
                      ),
                      211 => 
                      array (
                        'key' => 'Inflatable Bouncers & Ball Pits',
                        'value' => 'Inflatable Bouncers & Ball Pits',
                      ),
                      212 => 
                      array (
                        'key' => 'Insect Repellant',
                        'value' => 'Insect Repellant',
                      ),
                      213 => 
                      array (
                        'key' => 'Interactive Toys',
                        'value' => 'Interactive Toys',
                      ),
                      214 => 
                      array (
                        'key' => 'Irons Dry',
                        'value' => 'Irons Dry',
                      ),
                      215 => 
                      array (
                        'key' => 'Irons Steam',
                        'value' => 'Irons Steam',
                      ),
                      216 => 
                      array (
                        'key' => 'Jams, Jelly & Fruit Spreads',
                        'value' => 'Jams, Jelly & Fruit Spreads',
                      ),
                      217 => 
                      array (
                        'key' => 'Jar Openers',
                        'value' => 'Jar Openers',
                      ),
                      218 => 
                      array (
                        'key' => 'Juice',
                        'value' => 'Juice',
                      ),
                      219 => 
                      array (
                        'key' => 'Juicers',
                        'value' => 'Juicers',
                      ),
                      220 => 
                      array (
                        'key' => 'Karaoke',
                        'value' => 'Karaoke',
                      ),
                      221 => 
                      array (
                        'key' => 'Kettles',
                        'value' => 'Kettles',
                      ),
                      222 => 
                      array (
                        'key' => 'Keyboards & Pianos',
                        'value' => 'Keyboards & Pianos',
                      ),
                      223 => 
                      array (
                        'key' => 'Kids\' Electronics',
                        'value' => 'Kids\' Electronics',
                      ),
                      224 => 
                      array (
                        'key' => 'Kids\' Tables, Chairs & Sofas',
                        'value' => 'Kids\' Tables, Chairs & Sofas',
                      ),
                      225 => 
                      array (
                        'key' => 'Kitchens & Housekeeping',
                        'value' => 'Kitchens & Housekeeping',
                      ),
                      226 => 
                      array (
                        'key' => 'Kits',
                        'value' => 'Kits',
                      ),
                      227 => 
                      array (
                        'key' => 'Knife Sets',
                        'value' => 'Knife Sets',
                      ),
                      228 => 
                      array (
                        'key' => 'Laban',
                        'value' => 'Laban',
                      ),
                      229 => 
                      array (
                        'key' => 'Labaneh',
                        'value' => 'Labaneh',
                      ),
                      230 => 
                      array (
                        'key' => 'Labneh',
                        'value' => 'Labneh',
                      ),
                      231 => 
                      array (
                        'key' => 'Lamb',
                        'value' => 'Lamb',
                      ),
                      232 => 
                      array (
                        'key' => 'Laundry',
                        'value' => 'Laundry',
                      ),
                      233 => 
                      array (
                        'key' => 'Layette',
                        'value' => 'Layette',
                      ),
                      234 => 
                      array (
                        'key' => 'Learning',
                        'value' => 'Learning',
                      ),
                      235 => 
                      array (
                        'key' => 'Lifestyle, Sport and Leisure',
                        'value' => 'Lifestyle, Sport and Leisure',
                      ),
                      236 => 
                      array (
                        'key' => 'Magic',
                        'value' => 'Magic',
                      ),
                      237 => 
                      array (
                        'key' => 'Makeup kits',
                        'value' => 'Makeup kits',
                      ),
                      238 => 
                      array (
                        'key' => 'Mandolin Slicers',
                        'value' => 'Mandolin Slicers',
                      ),
                      239 => 
                      array (
                        'key' => 'Massage and relaxation',
                        'value' => 'Massage and relaxation',
                      ),
                      240 => 
                      array (
                        'key' => 'Mat',
                        'value' => 'Mat',
                      ),
                      241 => 
                      array (
                        'key' => 'Mattresses',
                        'value' => 'Mattresses',
                      ),
                      242 => 
                      array (
                        'key' => 'Measuring Cups',
                        'value' => 'Measuring Cups',
                      ),
                      243 => 
                      array (
                        'key' => 'Meat Tenderisers',
                        'value' => 'Meat Tenderisers',
                      ),
                      244 => 
                      array (
                        'key' => 'Men Hair Straightner',
                        'value' => 'Men Hair Straightner',
                      ),
                      245 => 
                      array (
                        'key' => 'Men\'s shavers',
                        'value' => 'Men\'s shavers',
                      ),
                      246 => 
                      array (
                        'key' => 'Microwave Oven',
                        'value' => 'Microwave Oven',
                      ),
                      247 => 
                      array (
                        'key' => 'Milk & Cream',
                        'value' => 'Milk & Cream',
                      ),
                      248 => 
                      array (
                        'key' => 'Mincers',
                        'value' => 'Mincers',
                      ),
                      249 => 
                      array (
                        'key' => 'Mini Collectibles',
                        'value' => 'Mini Collectibles',
                      ),
                      250 => 
                      array (
                        'key' => 'Miniature Dolls & Play Sets',
                        'value' => 'Miniature Dolls & Play Sets',
                      ),
                      251 => 
                      array (
                        'key' => 'Mixing Bowls',
                        'value' => 'Mixing Bowls',
                      ),
                      252 => 
                      array (
                        'key' => 'Moisturiser',
                        'value' => 'Moisturiser',
                      ),
                      253 => 
                      array (
                        'key' => 'Mop',
                        'value' => 'Mop',
                      ),
                      254 => 
                      array (
                        'key' => 'Mouth Freshners',
                        'value' => 'Mouth Freshners',
                      ),
                      255 => 
                      array (
                        'key' => 'Mugs',
                        'value' => 'Mugs',
                      ),
                      256 => 
                      array (
                        'key' => 'Musical Toys',
                        'value' => 'Musical Toys',
                      ),
                      257 => 
                      array (
                        'key' => 'Nail Balm',
                        'value' => 'Nail Balm',
                      ),
                      258 => 
                      array (
                        'key' => 'Nail Care',
                        'value' => 'Nail Care',
                      ),
                      259 => 
                      array (
                        'key' => 'Nebulizer',
                        'value' => 'Nebulizer',
                      ),
                      260 => 
                      array (
                        'key' => 'Night Lamp',
                        'value' => 'Night Lamp',
                      ),
                      261 => 
                      array (
                        'key' => 'Noodles',
                        'value' => 'Noodles',
                      ),
                      262 => 
                      array (
                        'key' => 'Novelty & Gifts',
                        'value' => 'Novelty & Gifts',
                      ),
                      263 => 
                      array (
                        'key' => 'Nutritional & Diet Drinks',
                        'value' => 'Nutritional & Diet Drinks',
                      ),
                      264 => 
                      array (
                        'key' => 'Nuts, Seeds, Granola & Trail Mixes',
                        'value' => 'Nuts, Seeds, Granola & Trail Mixes',
                      ),
                      265 => 
                      array (
                        'key' => 'Office Supplies',
                        'value' => 'Office Supplies',
                      ),
                      266 => 
                      array (
                        'key' => 'Office, School & Art Supplies',
                        'value' => 'Office, School & Art Supplies',
                      ),
                      267 => 
                      array (
                        'key' => 'Olives & Capers',
                        'value' => 'Olives & Capers',
                      ),
                      268 => 
                      array (
                        'key' => 'Oral Hygiene',
                        'value' => 'Oral Hygiene',
                      ),
                      269 => 
                      array (
                        'key' => 'Organic',
                        'value' => 'Organic',
                      ),
                      270 => 
                      array (
                        'key' => 'Organic Baby',
                        'value' => 'Organic Baby',
                      ),
                      271 => 
                      array (
                        'key' => 'Organic Beverages',
                        'value' => 'Organic Beverages',
                      ),
                      272 => 
                      array (
                        'key' => 'Organic Breakfast',
                        'value' => 'Organic Breakfast',
                      ),
                      273 => 
                      array (
                        'key' => 'Organic Dairy',
                        'value' => 'Organic Dairy',
                      ),
                      274 => 
                      array (
                        'key' => 'Organic Fruits',
                        'value' => 'Organic Fruits',
                      ),
                      275 => 
                      array (
                        'key' => 'Organic Grains & Pasta',
                        'value' => 'Organic Grains & Pasta',
                      ),
                      276 => 
                      array (
                        'key' => 'Organic Produce',
                        'value' => 'Organic Produce',
                      ),
                      277 => 
                      array (
                        'key' => 'Organic Snacks',
                        'value' => 'Organic Snacks',
                      ),
                      278 => 
                      array (
                        'key' => 'Organic Soups & Canned',
                        'value' => 'Organic Soups & Canned',
                      ),
                      279 => 
                      array (
                        'key' => 'Organic Vegetables',
                        'value' => 'Organic Vegetables',
                      ),
                      280 => 
                      array (
                        'key' => 'Others',
                        'value' => 'Others',
                      ),
                      281 => 
                      array (
                        'key' => 'Outdoor Play Toys',
                        'value' => 'Outdoor Play Toys',
                      ),
                      282 => 
                      array (
                        'key' => 'Oyster Knives',
                        'value' => 'Oyster Knives',
                      ),
                      283 => 
                      array (
                        'key' => 'Packaged Bread',
                        'value' => 'Packaged Bread',
                      ),
                      284 => 
                      array (
                        'key' => 'Packaged Cheese',
                        'value' => 'Packaged Cheese',
                      ),
                      285 => 
                      array (
                        'key' => 'Packaged Meat',
                        'value' => 'Packaged Meat',
                      ),
                      286 => 
                      array (
                        'key' => 'Pads',
                        'value' => 'Pads',
                      ),
                      287 => 
                      array (
                        'key' => 'Pain Relief',
                        'value' => 'Pain Relief',
                      ),
                      288 => 
                      array (
                        'key' => 'Paint & Painting Supplies',
                        'value' => 'Paint & Painting Supplies',
                      ),
                      289 => 
                      array (
                        'key' => 'Pancake & Waffle Mixes',
                        'value' => 'Pancake & Waffle Mixes',
                      ),
                      290 => 
                      array (
                        'key' => 'Paper & Boards',
                        'value' => 'Paper & Boards',
                      ),
                      291 => 
                      array (
                        'key' => 'Paper Products',
                        'value' => 'Paper Products',
                      ),
                      292 => 
                      array (
                        'key' => 'Pasta',
                        'value' => 'Pasta',
                      ),
                      293 => 
                      array (
                        'key' => 'Pasta & Pizza Sauce',
                        'value' => 'Pasta & Pizza Sauce',
                      ),
                      294 => 
                      array (
                        'key' => 'Pastry Brushes & Tools',
                        'value' => 'Pastry Brushes & Tools',
                      ),
                      295 => 
                      array (
                        'key' => 'Peanut Butter',
                        'value' => 'Peanut Butter',
                      ),
                      296 => 
                      array (
                        'key' => 'Peelers',
                        'value' => 'Peelers',
                      ),
                      297 => 
                      array (
                        'key' => 'Peeling & Paring Knives',
                        'value' => 'Peeling & Paring Knives',
                      ),
                      298 => 
                      array (
                        'key' => 'Pet Supplies',
                        'value' => 'Pet Supplies',
                      ),
                      299 => 
                      array (
                        'key' => 'Pickles, Peppers & Relish',
                        'value' => 'Pickles, Peppers & Relish',
                      ),
                      300 => 
                      array (
                        'key' => 'Pie & Pastry Filling',
                        'value' => 'Pie & Pastry Filling',
                      ),
                      301 => 
                      array (
                        'key' => 'Pizza Cutters & Wheels',
                        'value' => 'Pizza Cutters & Wheels',
                      ),
                      302 => 
                      array (
                        'key' => 'Plates',
                        'value' => 'Plates',
                      ),
                      303 => 
                      array (
                        'key' => 'Playmat',
                        'value' => 'Playmat',
                      ),
                      304 => 
                      array (
                        'key' => 'Playsets & Figures',
                        'value' => 'Playsets & Figures',
                      ),
                      305 => 
                      array (
                        'key' => 'Popcorn',
                        'value' => 'Popcorn',
                      ),
                      306 => 
                      array (
                        'key' => 'Pork',
                        'value' => 'Pork',
                      ),
                      307 => 
                      array (
                        'key' => 'Potato Mashers',
                        'value' => 'Potato Mashers',
                      ),
                      308 => 
                      array (
                        'key' => 'Pots',
                        'value' => 'Pots',
                      ),
                      309 => 
                      array (
                        'key' => 'Potty',
                        'value' => 'Potty',
                      ),
                      310 => 
                      array (
                        'key' => 'Prepared Pasta, Sides & Others',
                        'value' => 'Prepared Pasta, Sides & Others',
                      ),
                      311 => 
                      array (
                        'key' => 'Pressure Monitors',
                        'value' => 'Pressure Monitors',
                      ),
                      312 => 
                      array (
                        'key' => 'Pressure Washers',
                        'value' => 'Pressure Washers',
                      ),
                      313 => 
                      array (
                        'key' => 'Pretend Play',
                        'value' => 'Pretend Play',
                      ),
                      314 => 
                      array (
                        'key' => 'Pudding & Gelatin Mix',
                        'value' => 'Pudding & Gelatin Mix',
                      ),
                      315 => 
                      array (
                        'key' => 'Puzzles',
                        'value' => 'Puzzles',
                      ),
                      316 => 
                      array (
                        'key' => 'Rattles & Teethers',
                        'value' => 'Rattles & Teethers',
                      ),
                      317 => 
                      array (
                        'key' => 'Ready To Eat Meals',
                        'value' => 'Ready To Eat Meals',
                      ),
                      318 => 
                      array (
                        'key' => 'Ready To Eat Pudding & Gelatin',
                        'value' => 'Ready To Eat Pudding & Gelatin',
                      ),
                      319 => 
                      array (
                        'key' => 'Rice & Rice Mixes',
                        'value' => 'Rice & Rice Mixes',
                      ),
                      320 => 
                      array (
                        'key' => 'Rice Cookers',
                        'value' => 'Rice Cookers',
                      ),
                      321 => 
                      array (
                        'key' => 'Riding Toys & Wagons',
                        'value' => 'Riding Toys & Wagons',
                      ),
                      322 => 
                      array (
                        'key' => 'Robots',
                        'value' => 'Robots',
                      ),
                      323 => 
                      array (
                        'key' => 'Role Play',
                        'value' => 'Role Play',
                      ),
                      324 => 
                      array (
                        'key' => 'Room Decor',
                        'value' => 'Room Decor',
                      ),
                      325 => 
                      array (
                        'key' => 'Safety',
                        'value' => 'Safety',
                      ),
                      326 => 
                      array (
                        'key' => 'Salad Dressing & Toppings',
                        'value' => 'Salad Dressing & Toppings',
                      ),
                      327 => 
                      array (
                        'key' => 'Salad Spinners',
                        'value' => 'Salad Spinners',
                      ),
                      328 => 
                      array (
                        'key' => 'Salt & Pepper Mills',
                        'value' => 'Salt & Pepper Mills',
                      ),
                      329 => 
                      array (
                        'key' => 'Sand pits and sand',
                        'value' => 'Sand pits and sand',
                      ),
                      330 => 
                      array (
                        'key' => 'Sandwich Makers & Griddlers',
                        'value' => 'Sandwich Makers & Griddlers',
                      ),
                      331 => 
                      array (
                        'key' => 'Santoku Knives',
                        'value' => 'Santoku Knives',
                      ),
                      332 => 
                      array (
                        'key' => 'Sauces & Marinades',
                        'value' => 'Sauces & Marinades',
                      ),
                      333 => 
                      array (
                        'key' => 'Science & Discovery',
                        'value' => 'Science & Discovery',
                      ),
                      334 => 
                      array (
                        'key' => 'Scissors & Shears',
                        'value' => 'Scissors & Shears',
                      ),
                      335 => 
                      array (
                        'key' => 'Scrapbooking',
                        'value' => 'Scrapbooking',
                      ),
                      336 => 
                      array (
                        'key' => 'Seafood',
                        'value' => 'Seafood',
                      ),
                      337 => 
                      array (
                        'key' => 'Seafood Tools',
                        'value' => 'Seafood Tools',
                      ),
                      338 => 
                      array (
                        'key' => 'Sets',
                        'value' => 'Sets',
                      ),
                      339 => 
                      array (
                        'key' => 'Sexual Health',
                        'value' => 'Sexual Health',
                      ),
                      340 => 
                      array (
                        'key' => 'Shampoo',
                        'value' => 'Shampoo',
                      ),
                      341 => 
                      array (
                        'key' => 'Sharpening Steels',
                        'value' => 'Sharpening Steels',
                      ),
                      342 => 
                      array (
                        'key' => 'Shaving Needs',
                        'value' => 'Shaving Needs',
                      ),
                      343 => 
                      array (
                        'key' => 'Shower Gel',
                        'value' => 'Shower Gel',
                      ),
                      344 => 
                      array (
                        'key' => 'Shredded Coconut',
                        'value' => 'Shredded Coconut',
                      ),
                      345 => 
                      array (
                        'key' => 'Sieves & Sifters',
                        'value' => 'Sieves & Sifters',
                      ),
                      346 => 
                      array (
                        'key' => 'Sink Top Strainers',
                        'value' => 'Sink Top Strainers',
                      ),
                      347 => 
                      array (
                        'key' => 'Sippy Cup',
                        'value' => 'Sippy Cup',
                      ),
                      348 => 
                      array (
                        'key' => 'Skateboards',
                        'value' => 'Skateboards',
                      ),
                      349 => 
                      array (
                        'key' => 'Skates',
                        'value' => 'Skates',
                      ),
                      350 => 
                      array (
                        'key' => 'Skin Care',
                        'value' => 'Skin Care',
                      ),
                      351 => 
                      array (
                        'key' => 'Sleeping Aids',
                        'value' => 'Sleeping Aids',
                      ),
                      352 => 
                      array (
                        'key' => 'Snack Bars',
                        'value' => 'Snack Bars',
                      ),
                      353 => 
                      array (
                        'key' => 'Snack Cakes & Desserts',
                        'value' => 'Snack Cakes & Desserts',
                      ),
                      354 => 
                      array (
                        'key' => 'Soap',
                        'value' => 'Soap',
                      ),
                      355 => 
                      array (
                        'key' => 'Soda & Tonic',
                        'value' => 'Soda & Tonic',
                      ),
                      356 => 
                      array (
                        'key' => 'Soft & Plush Toys',
                        'value' => 'Soft & Plush Toys',
                      ),
                      357 => 
                      array (
                        'key' => 'Soft Drinks',
                        'value' => 'Soft Drinks',
                      ),
                      358 => 
                      array (
                        'key' => 'Soothers',
                        'value' => 'Soothers',
                      ),
                      359 => 
                      array (
                        'key' => 'Soup',
                        'value' => 'Soup',
                      ),
                      360 => 
                      array (
                        'key' => 'Spaghetti & Pasta Servers',
                        'value' => 'Spaghetti & Pasta Servers',
                      ),
                      361 => 
                      array (
                        'key' => 'Spatulas',
                        'value' => 'Spatulas',
                      ),
                      362 => 
                      array (
                        'key' => 'Sporting Goods',
                        'value' => 'Sporting Goods',
                      ),
                      363 => 
                      array (
                        'key' => 'Sports & Energy Drinks',
                        'value' => 'Sports & Energy Drinks',
                      ),
                      364 => 
                      array (
                        'key' => 'Stamps & Stamp',
                        'value' => 'Stamps & Stamp',
                      ),
                      365 => 
                      array (
                        'key' => 'Steamers',
                        'value' => 'Steamers',
                      ),
                      366 => 
                      array (
                        'key' => 'Step Stools',
                        'value' => 'Step Stools',
                      ),
                      367 => 
                      array (
                        'key' => 'Sterliser',
                        'value' => 'Sterliser',
                      ),
                      368 => 
                      array (
                        'key' => 'Stomach Remedies',
                        'value' => 'Stomach Remedies',
                      ),
                      369 => 
                      array (
                        'key' => 'Storage & Organization',
                        'value' => 'Storage & Organization',
                      ),
                      370 => 
                      array (
                        'key' => 'Strainers',
                        'value' => 'Strainers',
                      ),
                      371 => 
                      array (
                        'key' => 'Straw Cup',
                        'value' => 'Straw Cup',
                      ),
                      372 => 
                      array (
                        'key' => 'Strollers',
                        'value' => 'Strollers',
                      ),
                      373 => 
                      array (
                        'key' => 'Sugar & Sweeteners',
                        'value' => 'Sugar & Sweeteners',
                      ),
                      374 => 
                      array (
                        'key' => 'Sunblock',
                        'value' => 'Sunblock',
                      ),
                      375 => 
                      array (
                        'key' => 'Swaddler',
                        'value' => 'Swaddler',
                      ),
                      376 => 
                      array (
                        'key' => 'Swimming Pools & Water Fun',
                        'value' => 'Swimming Pools & Water Fun',
                      ),
                      377 => 
                      array (
                        'key' => 'Swings',
                        'value' => 'Swings',
                      ),
                      378 => 
                      array (
                        'key' => 'Syrup',
                        'value' => 'Syrup',
                      ),
                      379 => 
                      array (
                        'key' => 'Tea',
                        'value' => 'Tea',
                      ),
                      380 => 
                      array (
                        'key' => 'Teats',
                        'value' => 'Teats',
                      ),
                      381 => 
                      array (
                        'key' => 'Teethers',
                        'value' => 'Teethers',
                      ),
                      382 => 
                      array (
                        'key' => 'Thermometer',
                        'value' => 'Thermometer',
                      ),
                      383 => 
                      array (
                        'key' => 'Thermometers',
                        'value' => 'Thermometers',
                      ),
                      384 => 
                      array (
                        'key' => 'Timers',
                        'value' => 'Timers',
                      ),
                      385 => 
                      array (
                        'key' => 'Tissue',
                        'value' => 'Tissue',
                      ),
                      386 => 
                      array (
                        'key' => 'Toasters & Accessories',
                        'value' => 'Toasters & Accessories',
                      ),
                      387 => 
                      array (
                        'key' => 'Tofu',
                        'value' => 'Tofu',
                      ),
                      388 => 
                      array (
                        'key' => 'Toilet Seat',
                        'value' => 'Toilet Seat',
                      ),
                      389 => 
                      array (
                        'key' => 'Toner',
                        'value' => 'Toner',
                      ),
                      390 => 
                      array (
                        'key' => 'Tongs',
                        'value' => 'Tongs',
                      ),
                      391 => 
                      array (
                        'key' => 'Toy Figures & Playsets',
                        'value' => 'Toy Figures & Playsets',
                      ),
                      392 => 
                      array (
                        'key' => 'Toys',
                        'value' => 'Toys',
                      ),
                      393 => 
                      array (
                        'key' => 'Training Cup',
                        'value' => 'Training Cup',
                      ),
                      394 => 
                      array (
                        'key' => 'Trains',
                        'value' => 'Trains',
                      ),
                      395 => 
                      array (
                        'key' => 'Trash Bags',
                        'value' => 'Trash Bags',
                      ),
                      396 => 
                      array (
                        'key' => 'Travel Accessories',
                        'value' => 'Travel Accessories',
                      ),
                      397 => 
                      array (
                        'key' => 'Treatments',
                        'value' => 'Treatments',
                      ),
                      398 => 
                      array (
                        'key' => 'Trimmers',
                        'value' => 'Trimmers',
                      ),
                      399 => 
                      array (
                        'key' => 'Turners',
                        'value' => 'Turners',
                      ),
                      400 => 
                      array (
                        'key' => 'Usb',
                        'value' => 'Usb',
                      ),
                      401 => 
                      array (
                        'key' => 'Usb Flash Drive',
                        'value' => 'Usb Flash Drive',
                      ),
                      402 => 
                      array (
                        'key' => 'Utensil Holders',
                        'value' => 'Utensil Holders',
                      ),
                      403 => 
                      array (
                        'key' => 'Utility Knives',
                        'value' => 'Utility Knives',
                      ),
                      404 => 
                      array (
                        'key' => 'Vaccum Cleaners',
                        'value' => 'Vaccum Cleaners',
                      ),
                      405 => 
                      array (
                        'key' => 'Vehicles & Play Sets',
                        'value' => 'Vehicles & Play Sets',
                      ),
                      406 => 
                      array (
                        'key' => 'Vitamins & Supplements',
                        'value' => 'Vitamins & Supplements',
                      ),
                      407 => 
                      array (
                        'key' => 'Walkers',
                        'value' => 'Walkers',
                      ),
                      408 => 
                      array (
                        'key' => 'Wash Cloth',
                        'value' => 'Wash Cloth',
                      ),
                      409 => 
                      array (
                        'key' => 'Water',
                        'value' => 'Water',
                      ),
                      410 => 
                      array (
                        'key' => 'Weaning Spoons',
                        'value' => 'Weaning Spoons',
                      ),
                      411 => 
                      array (
                        'key' => 'Weighing Scale',
                        'value' => 'Weighing Scale',
                      ),
                      412 => 
                      array (
                        'key' => 'Whisks',
                        'value' => 'Whisks',
                      ),
                      413 => 
                      array (
                        'key' => 'White Grapes',
                        'value' => 'White Grapes',
                      ),
                      414 => 
                      array (
                        'key' => 'Wind Instruments',
                        'value' => 'Wind Instruments',
                      ),
                      415 => 
                      array (
                        'key' => 'Wipes',
                        'value' => 'Wipes',
                      ),
                      416 => 
                      array (
                        'key' => 'Yeast',
                        'value' => 'Yeast',
                      ),
                      417 => 
                      array (
                        'key' => 'Yoghurt',
                        'value' => 'Yoghurt',
                      ),
                      418 => 
                      array (
                        'key' => 'Zesters',
                        'value' => 'Zesters',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'product_type',
                     'title' => 'Product Type',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 580,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'meta_title',
                     'title' => 'Meta Title',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              8 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'vbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 580,
                     'height' => 150,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'meta_keyword',
                     'title' => 'Meta Keyword',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 580,
                     'height' => 150,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'meta_description',
                     'title' => 'Meta Description',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              9 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'weight',
                     'title' => 'Item Weight',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'abv',
                     'title' => 'ABV',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
            ),
             'locked' => false,
          )),
          2 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'Additonal Info',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Additonal Info',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToManyObjectRelation::__set_state(array(
                 'fieldtype' => 'manyToManyObjectRelation',
                 'width' => '',
                 'height' => '',
                 'maxItems' => '',
                 'queryColumnType' => 'text',
                 'phpdocType' => 'array',
                 'relationType' => true,
                 'visibleFields' => 'name,sku,classname,filename',
                 'optimizedAdminLoading' => false,
                 'visibleFieldDefinitions' => 
                array (
                ),
                 'lazyLoading' => true,
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'BMMIProducts',
                  ),
                ),
                 'pathFormatterClass' => '',
                 'name' => 'alternate_product',
                 'title' => 'alternate_product',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'itemfrom',
                     'title' => 'Item From',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => '',
                     'height' => '',
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'substitute_item',
                     'title' => 'substitute_item',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 90,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'secondref',
                     'title' => 'VisionRef',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'closure',
                     'title' => 'Closure',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'vbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 580,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'light_full',
                     'title' => 'light_full',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 600,
                     'height' => '',
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'recipe_id',
                     'title' => 'Recipe Id',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'blog_id',
                     'title' => 'Blog Id',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Item1',
                        'value' => 'Item1',
                      ),
                      1 => 
                      array (
                        'key' => 'Item2',
                        'value' => 'Item2',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'item_from',
                     'title' => 'Item From',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'lead_time',
                     'title' => 'Lead Time',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => true,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Yes',
                        'value' => 'Yes',
                      ),
                      1 => 
                      array (
                        'key' => 'No',
                        'value' => 'No',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => 'No',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'confectionary_item',
                     'title' => 'Confectionary Item',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => true,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              6 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                ),
                 'locked' => false,
              )),
            ),
             'locked' => false,
          )),
          3 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 130,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'Classification',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Classification',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'brand',
                     'title' => 'Brand',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'P',
                        'value' => 'P',
                      ),
                      1 => 
                      array (
                        'key' => 'A',
                        'value' => 'A',
                      ),
                      2 => 
                      array (
                        'key' => 'B',
                        'value' => 'B',
                      ),
                      3 => 
                      array (
                        'key' => 'C',
                        'value' => 'C',
                      ),
                      4 => 
                      array (
                        'key' => 'D',
                        'value' => 'D',
                      ),
                      5 => 
                      array (
                        'key' => 'E',
                        'value' => 'E',
                      ),
                      6 => 
                      array (
                        'key' => 'F',
                        'value' => 'F',
                      ),
                      7 => 
                      array (
                        'key' => 'G',
                        'value' => 'G',
                      ),
                      8 => 
                      array (
                        'key' => 'H',
                        'value' => 'H',
                      ),
                      9 => 
                      array (
                        'key' => 'I',
                        'value' => 'I',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'relevance',
                     'title' => 'Relevance',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Vietnam',
                        'value' => ' Vietnam',
                      ),
                      1 => 
                      array (
                        'key' => 'Argentina',
                        'value' => 'Argentina',
                      ),
                      2 => 
                      array (
                        'key' => 'Australia',
                        'value' => 'Australia',
                      ),
                      3 => 
                      array (
                        'key' => 'Bahrain',
                        'value' => 'Bahrain',
                      ),
                      4 => 
                      array (
                        'key' => 'Bahrain',
                        'value' => 'Bahrain',
                      ),
                      5 => 
                      array (
                        'key' => 'Belgium',
                        'value' => 'Belgium',
                      ),
                      6 => 
                      array (
                        'key' => 'Brazil',
                        'value' => 'Brazil',
                      ),
                      7 => 
                      array (
                        'key' => 'Canada',
                        'value' => 'Canada',
                      ),
                      8 => 
                      array (
                        'key' => 'Chile',
                        'value' => 'Chile',
                      ),
                      9 => 
                      array (
                        'key' => 'China',
                        'value' => 'China',
                      ),
                      10 => 
                      array (
                        'key' => 'Denmark',
                        'value' => 'Denmark',
                      ),
                      11 => 
                      array (
                        'key' => 'EU',
                        'value' => 'EU',
                      ),
                      12 => 
                      array (
                        'key' => 'Egypt',
                        'value' => 'Egypt',
                      ),
                      13 => 
                      array (
                        'key' => 'England',
                        'value' => 'England',
                      ),
                      14 => 
                      array (
                        'key' => 'France',
                        'value' => 'France',
                      ),
                      15 => 
                      array (
                        'key' => 'Georgia',
                        'value' => 'Georgia',
                      ),
                      16 => 
                      array (
                        'key' => 'Germany',
                        'value' => 'Germany',
                      ),
                      17 => 
                      array (
                        'key' => 'Gloucester, Massachusetts (UK)',
                        'value' => 'Gloucester, Massachusetts (UK)',
                      ),
                      18 => 
                      array (
                        'key' => 'Grimsby',
                        'value' => 'Grimsby',
                      ),
                      19 => 
                      array (
                        'key' => 'Holland',
                        'value' => 'Holland',
                      ),
                      20 => 
                      array (
                        'key' => 'Hong Kong',
                        'value' => 'Hong Kong',
                      ),
                      21 => 
                      array (
                        'key' => 'India',
                        'value' => 'India',
                      ),
                      22 => 
                      array (
                        'key' => 'Ireland',
                        'value' => 'Ireland',
                      ),
                      23 => 
                      array (
                        'key' => 'Italy',
                        'value' => 'Italy',
                      ),
                      24 => 
                      array (
                        'key' => 'Japan',
                        'value' => 'Japan',
                      ),
                      25 => 
                      array (
                        'key' => 'KSA',
                        'value' => 'KSA',
                      ),
                      26 => 
                      array (
                        'key' => 'Lebanon',
                        'value' => 'Lebanon',
                      ),
                      27 => 
                      array (
                        'key' => 'Los Angeles',
                        'value' => 'Los Angeles',
                      ),
                      28 => 
                      array (
                        'key' => 'Malaysia',
                        'value' => 'Malaysia',
                      ),
                      29 => 
                      array (
                        'key' => 'Mexico',
                        'value' => 'Mexico',
                      ),
                      30 => 
                      array (
                        'key' => 'Netherlands',
                        'value' => 'Netherlands',
                      ),
                      31 => 
                      array (
                        'key' => 'New Zealand',
                        'value' => 'New Zealand',
                      ),
                      32 => 
                      array (
                        'key' => 'Novato, California',
                        'value' => 'Novato, California',
                      ),
                      33 => 
                      array (
                        'key' => 'Portugal',
                        'value' => 'Portugal',
                      ),
                      34 => 
                      array (
                        'key' => 'Romania',
                        'value' => 'Romania',
                      ),
                      35 => 
                      array (
                        'key' => 'Saudi Arabia',
                        'value' => 'Saudi Arabia',
                      ),
                      36 => 
                      array (
                        'key' => 'Scarborough',
                        'value' => 'Scarborough',
                      ),
                      37 => 
                      array (
                        'key' => 'Scotland',
                        'value' => 'Scotland',
                      ),
                      38 => 
                      array (
                        'key' => 'Singapore',
                        'value' => 'Singapore',
                      ),
                      39 => 
                      array (
                        'key' => 'South Africa',
                        'value' => 'South Africa',
                      ),
                      40 => 
                      array (
                        'key' => 'Spain',
                        'value' => 'Spain',
                      ),
                      41 => 
                      array (
                        'key' => 'Sweden',
                        'value' => 'Sweden',
                      ),
                      42 => 
                      array (
                        'key' => 'Swiss',
                        'value' => 'Swiss',
                      ),
                      43 => 
                      array (
                        'key' => 'Thailand',
                        'value' => 'Thailand',
                      ),
                      44 => 
                      array (
                        'key' => 'UAE',
                        'value' => 'UAE',
                      ),
                      45 => 
                      array (
                        'key' => 'UK',
                        'value' => 'UK',
                      ),
                      46 => 
                      array (
                        'key' => 'UNITED ARAB EMIRATES',
                        'value' => 'UNITED ARAB EMIRATES',
                      ),
                      47 => 
                      array (
                        'key' => 'USA',
                        'value' => 'USA',
                      ),
                      48 => 
                      array (
                        'key' => 'USA / Canada',
                        'value' => 'USA / Canada',
                      ),
                      49 => 
                      array (
                        'key' => 'United Kingdom',
                        'value' => 'United Kingdom',
                      ),
                      50 => 
                      array (
                        'key' => 'United States',
                        'value' => 'United States',
                      ),
                      51 => 
                      array (
                        'key' => 'Victoria, Australia',
                        'value' => 'Victoria, Australia',
                      ),
                      52 => 
                      array (
                        'key' => 'Vietnam',
                        'value' => 'Vietnam',
                      ),
                      53 => 
                      array (
                        'key' => 'Worcester',
                        'value' => 'Worcester',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'country',
                     'title' => 'Country',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Featured',
                        'value' => 'Featured',
                      ),
                      1 => 
                      array (
                        'key' => 'Best Seller',
                        'value' => 'Best Seller',
                      ),
                      2 => 
                      array (
                        'key' => 'New',
                        'value' => 'New',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'product_badge',
                     'title' => 'Product Badge',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'region',
                     'title' => 'Region',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Speyside',
                        'value' => 'Speyside',
                      ),
                      1 => 
                      array (
                        'key' => 'South France',
                        'value' => 'South France',
                      ),
                      2 => 
                      array (
                        'key' => 'Rhone',
                        'value' => 'Rhone',
                      ),
                      3 => 
                      array (
                        'key' => 'Rhone',
                        'value' => 'Rhone',
                      ),
                      4 => 
                      array (
                        'key' => 'Lowland',
                        'value' => 'Lowland',
                      ),
                      5 => 
                      array (
                        'key' => 'Loire Valley',
                        'value' => 'Loire Valley',
                      ),
                      6 => 
                      array (
                        'key' => 'Islay',
                        'value' => 'Islay',
                      ),
                      7 => 
                      array (
                        'key' => 'Highland',
                        'value' => 'Highland',
                      ),
                      8 => 
                      array (
                        'key' => 'Burgundy',
                        'value' => 'Burgundy',
                      ),
                      9 => 
                      array (
                        'key' => 'Bordeaux',
                        'value' => 'Bordeaux',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'subregion',
                     'title' => 'Sub Region',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'vbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 580,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'style',
                     'title' => 'Style',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Multiselect::__set_state(array(
                     'fieldtype' => 'multiselect',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Style1',
                        'value' => 'Style1',
                      ),
                      1 => 
                      array (
                        'key' => 'Style2',
                        'value' => 'Style2',
                      ),
                    ),
                     'width' => 220,
                     'height' => '',
                     'maxItems' => '',
                     'renderType' => 'list',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'text',
                     'columnType' => 'text',
                     'phpdocType' => 'array',
                     'dynamicOptions' => false,
                     'name' => 'lifestyle',
                     'title' => 'Lifestyle',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'White',
                        'value' => 'White',
                      ),
                      1 => 
                      array (
                        'key' => 'Stainless Steel',
                        'value' => 'Stainless Steel',
                      ),
                      2 => 
                      array (
                        'key' => 'Silver',
                        'value' => 'Silver',
                      ),
                      3 => 
                      array (
                        'key' => 'Red',
                        'value' => 'Red',
                      ),
                      4 => 
                      array (
                        'key' => 'Purple',
                        'value' => 'Purple',
                      ),
                      5 => 
                      array (
                        'key' => 'Orange',
                        'value' => 'Orange',
                      ),
                      6 => 
                      array (
                        'key' => 'Multi',
                        'value' => 'Multi',
                      ),
                      7 => 
                      array (
                        'key' => 'Metallic',
                        'value' => 'Metallic',
                      ),
                      8 => 
                      array (
                        'key' => 'Grey',
                        'value' => 'Grey',
                      ),
                      9 => 
                      array (
                        'key' => 'Green',
                        'value' => 'Green',
                      ),
                      10 => 
                      array (
                        'key' => 'Brushed Metal',
                        'value' => 'Brushed Metal',
                      ),
                      11 => 
                      array (
                        'key' => 'Blue',
                        'value' => 'Blue',
                      ),
                      12 => 
                      array (
                        'key' => 'Black',
                        'value' => 'Black',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'color',
                     'title' => 'Color',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'cross_tier_id',
                     'title' => 'Cross Tier Id',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
            ),
             'locked' => false,
          )),
          4 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'Product Media',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Product Media',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
                 'fieldtype' => 'panel',
                 'labelWidth' => 100,
                 'layout' => NULL,
                 'border' => false,
                 'icon' => '',
                 'name' => 'Layout',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 150,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => 'Field Container',
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Image::__set_state(array(
                         'fieldtype' => 'image',
                         'width' => '',
                         'height' => '',
                         'uploadPath' => '',
                         'queryColumnType' => 'int(11)',
                         'columnType' => 'int(11)',
                         'phpdocType' => '\\Pimcore\\Model\\Asset\\Image',
                         'name' => 'image',
                         'title' => 'base_image',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => false,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => true,
                         'visibleSearch' => true,
                      )),
                      1 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Image::__set_state(array(
                         'fieldtype' => 'image',
                         'width' => '',
                         'height' => '',
                         'uploadPath' => '',
                         'queryColumnType' => 'int(11)',
                         'columnType' => 'int(11)',
                         'phpdocType' => '\\Pimcore\\Model\\Asset\\Image',
                         'name' => 'small_image',
                         'title' => 'Small Image',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => false,
                         'index' => false,
                         'locked' => false,
                         'style' => 'float:left; padding-left:20px;',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                      2 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Image::__set_state(array(
                         'fieldtype' => 'image',
                         'width' => '',
                         'height' => '',
                         'uploadPath' => '',
                         'queryColumnType' => 'int(11)',
                         'columnType' => 'int(11)',
                         'phpdocType' => '\\Pimcore\\Model\\Asset\\Image',
                         'name' => 'thumbnail',
                         'title' => 'Thumbnail',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => false,
                         'index' => false,
                         'locked' => false,
                         'style' => 'float:left; padding-left:20px;',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                    ),
                     'locked' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 100,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => '',
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\ImageGallery::__set_state(array(
                         'fieldtype' => 'imageGallery',
                         'queryColumnType' => 
                        array (
                          'images' => 'text',
                          'hotspots' => 'text',
                        ),
                         'columnType' => 
                        array (
                          'images' => 'text',
                          'hotspots' => 'text',
                        ),
                         'phpdocType' => '\\Pimcore\\Model\\DataObject\\Data\\ImageGallery',
                         'width' => 300,
                         'height' => NULL,
                         'uploadPath' => '',
                         'ratioX' => NULL,
                         'ratioY' => NULL,
                         'predefinedDataTemplates' => '',
                         'name' => 'media_gallery',
                         'title' => 'image gallery',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => false,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                    ),
                     'locked' => false,
                  )),
                  2 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 100,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => '',
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                      0 => 
                      Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                         'fieldtype' => 'input',
                         'width' => NULL,
                         'queryColumnType' => 'varchar',
                         'columnType' => 'varchar',
                         'columnLength' => 190,
                         'phpdocType' => 'string',
                         'regex' => '',
                         'unique' => false,
                         'showCharCount' => false,
                         'name' => 'video',
                         'title' => 'Video Link',
                         'tooltip' => '',
                         'mandatory' => false,
                         'noteditable' => false,
                         'index' => false,
                         'locked' => false,
                         'style' => '',
                         'permissions' => NULL,
                         'datatype' => 'data',
                         'relationType' => false,
                         'invisible' => false,
                         'visibleGridView' => false,
                         'visibleSearch' => false,
                      )),
                    ),
                     'locked' => false,
                  )),
                  3 => 
                  Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                     'fieldtype' => 'fieldcontainer',
                     'labelWidth' => 100,
                     'layout' => 'hbox',
                     'fieldLabel' => '',
                     'name' => 'Field Container',
                     'type' => NULL,
                     'region' => NULL,
                     'title' => '',
                     'width' => NULL,
                     'height' => NULL,
                     'collapsible' => false,
                     'collapsed' => false,
                     'bodyStyle' => '',
                     'datatype' => 'layout',
                     'permissions' => NULL,
                     'childs' => 
                    array (
                    ),
                     'locked' => false,
                  )),
                ),
                 'locked' => false,
              )),
            ),
             'locked' => false,
          )),
          5 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 130,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'ERP Data',
             'type' => NULL,
             'region' => NULL,
             'title' => 'ERP Data',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_vat_code',
                     'title' => 'VAT Code',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Date::__set_state(array(
                     'fieldtype' => 'date',
                     'queryColumnType' => 'bigint(20)',
                     'columnType' => 'bigint(20)',
                     'phpdocType' => '\\Carbon\\Carbon',
                     'defaultValue' => NULL,
                     'useCurrentDate' => false,
                     'name' => 'erp_last_update',
                     'title' => 'Last Update',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => NULL,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_brand',
                     'title' => 'erp_brand',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                     'fieldtype' => 'textarea',
                     'width' => 580,
                     'height' => 100,
                     'maxLength' => NULL,
                     'showCharCount' => false,
                     'excludeFromSearchIndex' => false,
                     'queryColumnType' => 'longtext',
                     'columnType' => 'longtext',
                     'phpdocType' => 'string',
                     'name' => 'erp_su_description',
                     'title' => 'Su Description',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => true,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Active',
                        'value' => 'Active',
                      ),
                      1 => 
                      array (
                        'key' => 'Inactive',
                        'value' => 'Inactive',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'erp_article_status',
                     'title' => 'Article Status',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Active',
                        'value' => 'Active',
                      ),
                      1 => 
                      array (
                        'key' => 'Inactive',
                        'value' => 'Inactive',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => 'Active',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'erp_su_status',
                     'title' => 'Su Status',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Yes',
                        'value' => 'Yes',
                      ),
                      1 => 
                      array (
                        'key' => 'No',
                        'value' => 'No',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'erp_su_link',
                     'title' => 'Su Link',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => false,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'erp_su_coeff',
                     'title' => 'Su Coeff',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_su',
                     'title' => 'Linked To SU',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_billing_unit',
                     'title' => 'Billing Unit',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_stock_unit',
                     'title' => 'Stock Unit',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_article_type',
                     'title' => 'Article Type',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              6 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_seasonal',
                     'title' => 'Seasonal',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_oracle_category',
                     'title' => 'Oracle Category',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              7 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_oracle_ref',
                     'title' => 'Oracle Ref',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'erp_oracle_uom',
                     'title' => 'Oracle Uom',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              8 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => NULL,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_mms_depth',
                     'title' => 'MMS_DEPTH',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              9 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_mms_l1',
                     'title' => 'MMS_L1',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'erp_mms_l1_desc',
                     'title' => 'MMS_L1_DESC',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              10 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_mms_l2',
                     'title' => 'MMS_L2',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'erp_mms_l2_desc',
                     'title' => 'MMS_L2_DESC',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              11 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_mms_l3',
                     'title' => 'MMS_L3',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'erp_mms_l3_desc',
                     'title' => 'MMS_L3_DESC',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              12 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_mms_l4',
                     'title' => 'MMS_L4',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'erp_mms_l4_desc',
                     'title' => 'MMS_L4_DESC',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              13 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'erp_mms_l5',
                     'title' => 'MMS_L5',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'erp_mms_l5_desc',
                     'title' => 'MMS_L5_DESC',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => true,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
            ),
             'locked' => false,
          )),
          6 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 130,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'Pricing',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Pricing',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => '15%',
                        'value' => '15',
                        'id' => 'extModel8735-1',
                      ),
                      1 => 
                      array (
                        'key' => '20%',
                        'value' => '20',
                        'id' => 'extModel8735-2',
                      ),
                      2 => 
                      array (
                        'key' => '30%',
                        'value' => '30',
                        'id' => 'extModel8735-3',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => '',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'discountable',
                     'title' => 'Discountable',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'tier_price',
                     'title' => 'Tier Price',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                     'fieldtype' => 'input',
                     'width' => 220,
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'regex' => '',
                     'unique' => false,
                     'showCharCount' => false,
                     'name' => 'price',
                     'title' => 'Price',
                     'tooltip' => '',
                     'mandatory' => true,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => true,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToManyObjectRelation::__set_state(array(
                 'fieldtype' => 'manyToManyObjectRelation',
                 'width' => '',
                 'height' => '',
                 'maxItems' => '',
                 'queryColumnType' => 'text',
                 'phpdocType' => 'array',
                 'relationType' => true,
                 'visibleFields' => 'StoreId,Price,SpecialPrice,FromDate,ToDate',
                 'optimizedAdminLoading' => false,
                 'visibleFieldDefinitions' => 
                array (
                ),
                 'lazyLoading' => true,
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'BMMIProductPrice',
                  ),
                ),
                 'pathFormatterClass' => '',
                 'name' => 'price_details',
                 'title' => 'price_details',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'datatype' => 'data',
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
              )),
            ),
             'locked' => false,
          )),
          7 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'fieldtype' => 'panel',
             'labelWidth' => 100,
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'name' => 'Inventory',
             'type' => NULL,
             'region' => NULL,
             'title' => 'Inventory',
             'width' => NULL,
             'height' => NULL,
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'permissions' => NULL,
             'childs' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Yes',
                        'value' => 'Yes',
                      ),
                      1 => 
                      array (
                        'key' => 'No',
                        'value' => 'No',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => 'No',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'manage_stock',
                     'title' => 'Manage Stock',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Yes',
                        'value' => 'Yes',
                      ),
                      1 => 
                      array (
                        'key' => 'No',
                        'value' => 'No',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => 'No',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'allow_backorders',
                     'title' => 'Allow Backorders',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => true,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'qty_oos',
                     'title' => 'Qty OOS',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Yes',
                        'value' => 'Yes',
                      ),
                      1 => 
                      array (
                        'key' => 'No',
                        'value' => 'No',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => 'No',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'qty_withdecimals',
                     'title' => 'Qty Withdecimals',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Select::__set_state(array(
                     'fieldtype' => 'select',
                     'options' => 
                    array (
                      0 => 
                      array (
                        'key' => 'Yes',
                        'value' => 'Yes',
                      ),
                      1 => 
                      array (
                        'key' => 'No',
                        'value' => 'No',
                      ),
                    ),
                     'width' => 220,
                     'defaultValue' => 'No',
                     'optionsProviderClass' => '',
                     'optionsProviderData' => '',
                     'queryColumnType' => 'varchar',
                     'columnType' => 'varchar',
                     'columnLength' => 190,
                     'phpdocType' => 'string',
                     'dynamicOptions' => false,
                     'name' => 'enable_qty_increments',
                     'title' => 'Enable Qty Increments',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => '',
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => false,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'increment_qty',
                     'title' => 'increment_qty',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Layout\Fieldcontainer::__set_state(array(
                 'fieldtype' => 'fieldcontainer',
                 'labelWidth' => 130,
                 'layout' => 'hbox',
                 'fieldLabel' => '',
                 'name' => 'Field Container',
                 'type' => NULL,
                 'region' => NULL,
                 'title' => '',
                 'width' => NULL,
                 'height' => NULL,
                 'collapsible' => false,
                 'collapsed' => false,
                 'bodyStyle' => '',
                 'datatype' => 'layout',
                 'permissions' => NULL,
                 'childs' => 
                array (
                  0 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => 220,
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => false,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'min_cart_qty',
                     'title' => 'Min Cart Qty',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                  1 => 
                  Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                     'fieldtype' => 'numeric',
                     'width' => '',
                     'defaultValue' => NULL,
                     'queryColumnType' => 'double',
                     'columnType' => 'double',
                     'phpdocType' => 'float',
                     'integer' => false,
                     'unsigned' => false,
                     'minValue' => NULL,
                     'maxValue' => NULL,
                     'unique' => false,
                     'decimalSize' => NULL,
                     'decimalPrecision' => NULL,
                     'name' => 'max_cart_qty',
                     'title' => 'Max Cart Qty',
                     'tooltip' => '',
                     'mandatory' => false,
                     'noteditable' => false,
                     'index' => false,
                     'locked' => false,
                     'style' => '',
                     'permissions' => NULL,
                     'datatype' => 'data',
                     'relationType' => false,
                     'invisible' => false,
                     'visibleGridView' => false,
                     'visibleSearch' => false,
                  )),
                ),
                 'locked' => false,
              )),
            ),
             'locked' => false,
          )),
        ),
         'locked' => false,
      )),
    ),
     'locked' => false,
  )),
   'default' => 0,
   'dao' => NULL,
));
