<?php 

return [
    "bundle" => [
        "Wvision\\Bundle\\DataDefinitionsBundle\\DataDefinitionsBundle" => TRUE,
        "Youwe\\Pimcore\\WorkflowGui\\WorkflowGuiBundle" => TRUE,
        "Divante\\MagentoIntegrationBundle\\DivanteMagentoIntegrationBundle" => TRUE
    ]
];
