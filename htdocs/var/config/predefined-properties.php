<?php 

return [
    1 => [
        "id" => 1,
        "name" => "Configurable Attributes",
        "description" => NULL,
        "key" => "configurable_attributes",
        "type" => "text",
        "data" => "",
        "config" => NULL,
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1577449415,
        "modificationDate" => 1584937202
    ],
    2 => [
        "id" => 2,
        "name" => "Delivery Mode",
        "description" => "Its is used in product Class",
        "key" => "delivery_mode",
        "type" => "select",
        "data" => "",
        "config" => "Standard Mode, Express Mode, Pickup",
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1584718143,
        "modificationDate" => 1585113239
    ],
    3 => [
        "id" => 3,
        "name" => "Websites",
        "description" => "Its is used in product Class",
        "key" => "websites",
        "type" => "select",
        "data" => "",
        "config" => "base,bmmishops,alosra,my1883,default",
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1584850365,
        "modificationDate" => 1587016906
    ],
    4 => [
        "id" => 4,
        "name" => "Mode For Store",
        "description" => "It's to list mode in store",
        "key" => "store_mode",
        "type" => "select",
        "data" => NULL,
        "config" => "Standard Mode,Express Mode",
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1584963115,
        "modificationDate" => 1584963188
    ],
    5 => [
        "id" => 5,
        "name" => "BMMIsite",
        "description" => "It's used to store code into zonegroup class name",
        "key" => "base",
        "type" => "text",
        "data" => "BMMI",
        "config" => "",
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1585053686,
        "modificationDate" => 1585121430
    ],
    6 => [
        "id" => 6,
        "name" => "Alosra",
        "description" => "It's used to store code into zonegroup class name",
        "key" => "alosra",
        "type" => "text",
        "data" => "Alosra",
        "config" => NULL,
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1585053800,
        "modificationDate" => 1585054022
    ],
    7 => [
        "id" => 7,
        "name" => "My1883",
        "description" => "It's used to store code into zonegroup class name",
        "key" => "my1883",
        "type" => "text",
        "data" => "My1883",
        "config" => NULL,
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1585053846,
        "modificationDate" => 1585054019
    ],
    8 => [
        "id" => 8,
        "name" => "BMMISites",
        "description" => NULL,
        "key" => "bmmishops",
        "type" => "text",
        "data" => "BMMI",
        "config" => "",
        "ctype" => "object",
        "inheritable" => TRUE,
        "creationDate" => 1585539659,
        "modificationDate" => 1587016985
    ]
];
