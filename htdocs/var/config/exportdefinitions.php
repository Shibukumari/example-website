<?php 

return [
    1 => [
        "fetcher" => "objects",
        "fetcherConfig" => [
            "unpublished" => FALSE
        ],
        "fetchUnpublished" => FALSE,
        "id" => 1,
        "name" => "bmmiexport",
        "provider" => "csv",
        "class" => "BMMIASLiquors",
        "configuration" => [
            "delimiter" => NULL,
            "enclosure" => NULL
        ],
        "creationDate" => 1577518128,
        "modificationDate" => 1577518171,
        "runner" => NULL,
        "stopOnException" => FALSE,
        "failureNotificationDocument" => NULL,
        "successNotificationDocument" => NULL
    ]
];
