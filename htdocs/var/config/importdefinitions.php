<?php 

return [
    1 => [
        "loader" => "primary_key",
        "objectPath" => "/Test Import",
        "cleaner" => NULL,
        "key" => NULL,
        "filter" => NULL,
        "renameExistingObjects" => FALSE,
        "relocateExistingObjects" => FALSE,
        "skipNewObjects" => FALSE,
        "skipExistingObjects" => FALSE,
        "createVersion" => FALSE,
        "omitMandatoryCheck" => FALSE,
        "forceLoadObject" => FALSE,
        "id" => 1,
        "name" => "ImportProduct",
        "provider" => "csv",
        "class" => "Test",
        "configuration" => [
            "csvExample" => NULL,
            "delimiter" => ",",
            "enclosure" => "\"",
            "csvHeaders" => "name,sku"
        ],
        "creationDate" => 1575974837,
        "modificationDate" => 1577521559,
        "mapping" => [
            [
                "primaryIdentifier" => TRUE,
                "setter" => NULL,
                "setterConfig" => NULL,
                "fromColumn" => "sku",
                "toColumn" => "sku",
                "interpreter" => NULL,
                "interpreterConfig" => NULL
            ],
            [
                "primaryIdentifier" => FALSE,
                "setter" => "key",
                "setterConfig" => [

                ],
                "fromColumn" => "name",
                "toColumn" => "name",
                "interpreter" => "asset_by_path",
                "interpreterConfig" => [
                    "path" => "/categorytest/"
                ]
            ]
        ],
        "runner" => NULL,
        "stopOnException" => FALSE,
        "failureNotificationDocument" => NULL,
        "successNotificationDocument" => NULL
    ]
];
