pimcore.registerNS("pimcore.plugin.BmmiBundleStoreBundle");

pimcore.plugin.BmmiBundleStoreBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.BmmiBundleStoreBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("BmmiBundleStoreBundle ready!");
    }
});

var BmmiBundleStoreBundlePlugin = new pimcore.plugin.BmmiBundleStoreBundle();
