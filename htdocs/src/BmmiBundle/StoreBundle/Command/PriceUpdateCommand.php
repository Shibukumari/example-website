<?php
namespace BmmiBundle\StoreBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \Datetime;

class PriceUpdateCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('pricedetails:update')->setDescription('Updating Price Details');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		// Checking Setting For Create Price
		$defaultInventory = '';

		$priceCron = DataObject\BMMISettings::getByCreatePrice('1', ['limit' => 1, 'unpublished' => false]);
		$dateObj = new DateTime();
		if (null != $priceCron && $priceCron->getCreatePrice() == '1') {

			// Get all the stores
			$storeObjOrg = new DataObject\Stores\Listing();
			$storeObjOrg->setCondition("Enabled = ?", "1");
			$storeObj = $storeObjOrg->load();
			foreach ($storeObj as $allstore) {
				$storeDetail['code'] = $allstore->getCode();
				$storeDetail['objid'] = $allstore->getO_id();
				$storeDetails[] = $storeDetail;
			}
			// Product Price For Each Store
			foreach ($storeDetails as $storeDetail) {
				$productPriceLists = new DataObject\BMMIProductPrice\Listing();
				$productPriceLists->setOrderKey("sku");
				$productPriceLists->setOrder("desc");
				$productPriceLists->setCondition("o_modificationDate >= :o_modificationDate AND StoreId = :StoreId", ["o_modificationDate" => $priceCron->getPriceUpdatedTime(), "StoreId" => $storeDetail['code']]);
				$productPriceLists->load();

				foreach ($productPriceLists as $productPriceList) {

					if ($defaultInventory != $productPriceList->getSku()) {
						$inventoryDetails = DataObject\Products::getBySku($productPriceList->getSku(), ['limit' => 1, 'unpublished' => false]);
						if (null != $inventoryDetails) {
							$product['manage_stock'] = $inventoryDetails->getManage_stock();
							$product['allow_backorders'] = $inventoryDetails->getAllow_backorders();
							$product['qty_oos'] = $inventoryDetails->getQty_oos();
							$product['qty_withdecimals'] = $inventoryDetails->getQty_withdecimals();
							$product['enable_qty_increments'] = $inventoryDetails->getEnable_qty_increments();
							$product['increment_qty'] = $inventoryDetails->getIncrement_qty();
							$product['min_cart_qty'] = $inventoryDetails->getMin_cart_qty();
							$product['max_cart_qty'] = $inventoryDetails->getMax_cart_qty();
							$product['sku'] = $productPriceList->getSku();
							$product['storeid'] = 'default';
							$product['orderableFlag'] = $productPriceList->getOrderableFlag();
							$product['manufacturedLink'] = $productPriceList->getManufacturedLink();
							$product['price'] = $product['spl_price'] = '';
							$product['start_date'] = $product['end_date'] = '';
							$product['locationstock'] = $product['vatcode'] = $product['stock_availability'] = '';
							$product['notify_quantity_below'] = $product['status'] = $product['delivery_type'] = '';
							$allPrice['data']['default'][] = $product;
							$allPrice['success'] = true;
							$defaultInventory = $productPriceList->getSku();
						}

					}

					$product['sku'] = $productPriceList->getSku();
					$product['price'] = $productPriceList->getPrice();
					$product['spl_price'] = $productPriceList->getSpecialPrice();

					if (null != $productPriceList->getFromDate()) {
						$product['start_date'] = $dateObj->setTimestamp(strtotime($productPriceList->getFromDate()->toDateString()))->format('Y-m-d');

					} elseif (null != $productPriceList->getToDate()) {
						$product['end_date'] = $dateObj->setTimestamp(strtotime($productPriceList->getToDate()->toDateString()))->format('Y-m-d');
					} else {
						$product['start_date'] = '';
						$product['end_date'] = '';
					}

					$product['storeid'] = $productPriceList->getStoreId();
					$product['orderableFlag'] = $productPriceList->getOrderableFlag();
					$product['manufacturedLink'] = $productPriceList->getManufacturedLink();
					$product['locationstock'] = $productPriceList->getLocationStock();
					$product['vatcode'] = $productPriceList->getVatCode();
					$product['stock_availability'] = $productPriceList->getStock_availability();
					$product['notify_quantity_below'] = $productPriceList->getNotify_quantity_below();
					if ($productPriceList->getStore_Status() == 1) {
						$product['status'] = "Enable";
					} else {
						$product['status'] = "Disable";
					}
					$product['delivery_type'] = $productPriceList->getDelivery_type();
					$allPrice['data'][$storeDetail['code']][] = $product;
					$allPrice['success'] = true;
				}
			}

			file_put_contents($priceCron->getPriceLocation(), json_encode($allPrice));
			// Update the setting to Zero once the cron is done
			$priceCron->setCreatePrice('0');
			$priceCron->setPriceUpdatedTime('0');
			$priceCron->save();
		}

	}

}
?>