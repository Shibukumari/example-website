<?php
namespace BmmiBundle\StoreBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PriceInProductCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('priceinproduct:update')->setDescription('Updating Price In Product');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		// Checking Setting For Create Price
		$priceCron = DataObject\BMMISettings::getByCreatePriceInProducts('1', ['limit' => 1, 'unpublished' => false]);
		if (null != $priceCron && $priceCron->getCreatePriceInProducts() == '1') {
			// Product Price For Each Store
			$productPriceLists = new DataObject\BMMIProductPrice\Listing();
			$productPriceLists->setCondition("o_modificationDate >= :o_modificationDate", ["o_modificationDate" => $priceCron->getPriceUpdatedProduct()]);
			$productPriceLists->load();
			$skuList = array();
			foreach ($productPriceLists as $productPriceList) {
				

				$productObj = DataObject\Products::getBySku($productPriceList->getSku(), ['limit' => 1, 'unpublished' => true]);

				if (in_array($productPriceList->getSku(), $skuList) == false) {
					$productPriceBysku = new DataObject\BMMIProductPrice\Listing();
					$productPriceBysku->setCondition("Sku = ?", $productPriceList->getSku());
					$productPriceBysku->load();
					if (null != $productPriceBysku->getObjects() && null != $productObj) {
						$productObj->setPrice_details($productPriceBysku->getObjects());
						$productObj->save();
					}
					array_push($skuList, $productPriceList->getSku());
				}

			}
			// Update the setting to Zero once the cron is done
			$priceCron->setCreatePriceInProducts('0');
			$priceCron->setPriceUpdatedProduct('0');
			$priceCron->save();
		}

	}

}
?>