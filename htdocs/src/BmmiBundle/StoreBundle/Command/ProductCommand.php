<?php
namespace BmmiBundle\StoreBundle\Command;

use BmmiBundle\StoreBundle\Website\ProductValidation;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\ApplicationLogger;
use Pimcore\Log\FileObject;
use Pimcore\Model\Asset;
use Pimcore\Model\Asset\Image;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('productdetails:update')->setDescription('Updating Products');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		$logger = \Pimcore\Log\ApplicationLogger::getInstance("Product Import", true); /* Object for creating log */

		$settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/
		$datetypearray = array('inputFormat', 'numericFormat', 'textareaFormat', 'selectFormat', 'wysiwygFormat', 'booleanSelectFormat');
		/*Read the file from FTP*/
		echo "Initialization" . PHP_EOL;
		try {
			$host = $settings->getFtp_host();
			$port = $settings->getPort();
			$username = $settings->getFtp_username();
			$privateKey = $settings->getFtp_privatekey();
			$logfileDir = $settings->getLogDir();
			$assetlogfileDir = $settings->getAssetlogDir();
			$productArchive = $settings->getBasePath();
			$sftp = new SFTP($host, $port);
			$Key = new RSA();
			$Key->loadKey($privateKey);
			if (!$sftp->login($username, $Key)) {
				throw new \Exception("SFTP Not Connected.");
			}

			$remoteDir = $settings->getRemote_dir();
			$localDir = $settings->getLocal_dir_path();
			if (!$sftp->get($remoteDir, $localDir)) {
				throw new \Exception("File Not available in server.");
			}
			//$sftp->put($localDir, $valfromget);
		} catch (\Exception $e) {
			echo "Error in FTP / File Not available in server" . PHP_EOL;
			$logger->log("ERROR", $e->getMessage());
			die;
		}

		try {

			if (($h = fopen("{$localDir}", "r")) !== FALSE) {

				while (($data = fgetcsv($h, 10000, ";")) !== FALSE) {
					$csvdata[] = $data;
				}

				fclose($h);
			}

			$keys = array_shift($csvdata);

			$csvfinaldata = array();

			foreach ($csvdata as $i => $row) {

				if (count($keys) != count($row)) {

					throw new \Exception("Count of Key and Row is mismatch : " . ($i + 1));
				} else {
					$csvfinaldata[$i] = array_combine($keys, $row);

				}
			}
		} catch (\Exception $e) {

			$fileObject = new FileObject('some error in CSV');

			$logger->log("ERROR", $e->getMessage(), ['fileObject' => $fileObject]);
		}

		$simple_product = 'Pimcore\\Model\\DataObject\\Products';

		$fieldDatatypeObj = new $simple_product();
		$fieldDatatype = $fieldDatatypeObj->getClass()->getFieldDefinitions();
		echo "Validation Started" . PHP_EOL;
		$getres = new ProductValidation();
		$errortxt = $getres->checkAttribute($csvfinaldata, $fieldDatatype);
		if (null != $errortxt) {
			$tfileName = date("d_m_Y_H_i_s", time());
			$logLocation = $logfileDir . $tfileName . '_product_import_log.txt';
			$sftp->put($logLocation, $errortxt);
			$sftp->chmod(0777, $logLocation);
			$logAsset = new Asset();
			$logAsset->setFilename($tfileName . '_product_import_log.txt');
			$logAsset->setData(file_get_contents($logLocation));
			$logAsset->setParent(Asset::getByPath($assetlogfileDir));
			$logAsset->save();
			echo PHP_EOL . "Please check the validation log" . PHP_EOL;
			die;
		}

		if (null != $csvfinaldata) {

			foreach ($csvfinaldata as $key => $eachproduct) {

				try {
					echo "Importing " . $eachproduct['sku'] . PHP_EOL;
					$productObj = $simple_product::getBySku($eachproduct['sku'], ['limit' => 1, 'unpublished' => true]);
					$eachproduct['is_active_in_pim'] = 'yes';
					$eachproduct['visibility'] = "Catalog, Search";

					if (isset($eachproduct['status'])) {
						if ($eachproduct['status'] == '' || $eachproduct['status'] == 'INACTIVE') {
							$eachproduct['status'] = 0;
						} elseif ($eachproduct['status'] == 'ACTIVE') {
							$eachproduct['status'] = 1;
						} else {
							$eachproduct['status'] = 1;
						}
					}

					if (!isset($eachproduct['product_badge']) || null == $eachproduct['product_badge']) {
						$eachproduct['product_badge'] = 'New';
					}

					if (null != $productObj) {

						foreach ($eachproduct as $fieldName => $value) {
							$value = trim($value);
							if (method_exists($productObj, 'get' . ucfirst($fieldName))) {
								if ($fieldName != "published" && $fieldName != "status") {

									$datetypeCol = $fieldDatatype[$fieldName]->getFieldType() . 'Format';
									if (null == $datetypeCol) {
										continue;
									}

									$funCheck = in_array($datetypeCol, $datetypearray);
									if (!$funCheck) {
										$value = $this->$datetypeCol($value, $key, $fieldName, $eachproduct['websites']);
									}
								}

								$productObj->set($fieldName, $value);
							}
						}
						if (isset($eachproduct['published'])) {
							$productObj->setPublished($eachproduct['published']);
						}

						$productObj->setStatus(true);
						$productObj->save();
						$logger->log("INFO", "Product is Updated :" . ($key + 1));

					} else {

						if (isset($eachproduct['product_folder'])) {
							$productParentObj = DataObject::getByPath('/BMMI/Products/' . $eachproduct['product_folder']);
							$productParentId = $productParentObj->getO_id();
						} else {
							echo "Skiping the import, Please check the folder name : " . ($key + 1);
							continue;
						}

						$new_product = new $simple_product();
						//$new_product_obj = $new_product->create($eachproduct);
						foreach ($eachproduct as $fieldName => $value) {
							$value = trim($value);
							if (method_exists($new_product, 'get' . ucfirst($fieldName))) {
								if ($fieldName != "published" && $fieldName != "status") {
									$datetypeCol = $fieldDatatype[$fieldName]->getFieldType() . 'Format';
									if (null == $datetypeCol) {
										continue;
									}
									$funCheck = in_array($datetypeCol, $datetypearray);
									if (!$funCheck) {
										$value = $this->$datetypeCol($value, $key, $fieldName, $eachproduct['websites']);

									}
								}

								$new_product->set($fieldName, $value);
							}
						}
						$new_product->setParentId($productParentId);
						$new_product->setStatus(true);
						$new_product->setKey($eachproduct['sku']);
						if (isset($eachproduct['published'])) {
							$new_product->setPublished($eachproduct['published']);
						}

						$new_product->save();
						$logger->log("INFO", "New Product Created :" . ($key + 1));
					}

				} catch (\Exception $e) {
					$logger->log("ERROR", $e->getMessage());

				}

			}
			echo "Imported" . PHP_EOL;
			$tfileName = date("d_m_Y_H_i_s", time());
			$archiveFile = $productArchive . '/' . $tfileName . '_product_import_archive.csv';

			$sftp->get($remoteDir, $archiveFile);
			$sftp->delete($remoteDir, false);
		} else {

		}

	}
	public function manyToManyObjectRelationFormat($attrValue, $key, $fieldName, $websites) {
		if ($attrValue != '' && $fieldName == 'category_ids') {

			$categorys = explode("||", trim($attrValue, "||"));

			$logger = \Pimcore\Log\ApplicationLogger::getInstance("Product Import", true); /* Object for creating log */
			$category_ids = array();
			foreach ($categorys as $category) {

				$siteName = explode('/', trim($category, "/"));
				$classname = 'Pimcore\\Model\\DataObject\\Category';
				$categoryObj = $classname::getByPath(trim($category));

				if (null != $categoryObj) {
					$category_ids[] = $categoryObj;

				}

			}
			return $category_ids;
		} else {
			return $attrValue;
		}
	}
	public function datetimeFormat($attrValue, $key, $fieldName, $websites) {
		if ($attrValue != '') {
			$dateValue = new \Carbon\Carbon();
			$dateValue->setTimestamp(strtotime($attrValue));
			return $dateValue;
		} else {
			return NULL;
		}

	}
	public function dateFormat($attrValue, $key, $fieldName, $websites) {
		if ($attrValue != '') {
			$dateValue = new \Carbon\Carbon();
			$dateValue->setTimestamp(strtotime($attrValue));
			return $dateValue;
		} else {
			return NULL;
		}

	}

	public function imageFormat($attrValue, $key, $fieldName, $websites) {

		if ($attrValue != '') {
			$image_value = Image::getByPath(trim($attrValue));
			if (null != $image_value) {
				return $image_value;
			} else {
				$logger = \Pimcore\Log\ApplicationLogger::getInstance("Product Import", true);
				$logger->log("WARNING", $fieldName . ' Image Not Found : ' . ($key + 1));
				return NULL;

			}
		}

	}

	public function imageGalleryFormat($attrValue, $key, $fieldName, $websites) {
		$all_media_gallery = explode("||", trim($attrValue, "||"));
		$items = [];
		if (null != $all_media_gallery) {
			foreach ($all_media_gallery as $media_gallery) {

				$each_media_gallery = new \Pimcore\Model\DataObject\Data\Hotspotimage();
				$each_media_gallery->setImage(Image::getByPath(trim($media_gallery)));
				$items[] = $each_media_gallery;

			}
		}

		if (null != $items) {
			$value = new \Pimcore\Model\DataObject\Data\ImageGallery($items);
			return $value;

		} else {
			$logger = \Pimcore\Log\ApplicationLogger::getInstance("Product Import", true);
			$logger->log("WARNING", $fieldName . ' Gallery Image Not Found : ' . ($key + 1));
			return NULL;
		}

	}

	public function multiselectFormat($attrValue, $key, $fieldName, $websites) {
		if ($attrValue != '') {
			$attrValue = explode("||", trim($attrValue, "||"));
			return $attrValue;
		}
	}

}
?>