<?php
namespace BmmiBundle\StoreBundle\Command;

use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\ApplicationLogger;
use Pimcore\Log\FileObject;
use Pimcore\Model\Asset\Image;
use Pimcore\Model\DataObject;
use Pimcore\Model\Asset;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BmmiBundle\StoreBundle\Website\ProductValidation;

class BlockCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('blockdetails:update')->setDescription('Updating Block');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		
		$settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/
		$datetypearray = array('inputFormat', 'numericFormat', 'textareaFormat', 'selectFormat', 'wysiwygFormat', 'booleanSelectFormat');
		/*Read the file from FTP*/
		try {
			$host = $settings->getFtp_host();
			$port = $settings->getPort();
			$username = $settings->getFtp_username();
			$privateKey = $settings->getFtp_privatekey();
			$logfileDir = $settings->getLogDir();
			$assetlogfileDir = $settings->getAssetlogDir();
			$sftp = new SFTP($host, $port);
			$Key = new RSA();
			$Key->loadKey($privateKey);

			if (!$sftp->login($username, $Key)) {
				throw new \Exception("SFTP Not Connected.");
			}

			$remoteDir = $settings->getRemote_dir();
			$localDir = $settings->getLocal_dir_path();

			if (!$sftp->get($remoteDir, $localDir)) {
				throw new \Exception("File Not available in server.");
			}
			//$sftp->put($localDir, $valfromget);
		} catch (\Exception $e) {
			print_r("error");
			die;
		}

		try {

			if (($h = fopen("{$localDir}", "r")) !== FALSE) {

				while (($data = fgetcsv($h, 100, ";")) !== FALSE) {
					$csvdata[] = $data;
				}

				fclose($h);
			}

			$keys = array_shift($csvdata);


			$csvfinaldata = array();
						

			foreach ($csvdata as $i => $row) {

				if (count($keys) != count($row)) {
					continue;
				} else {
					$csvfinaldata[$i] = array_combine($keys, $row);


				}
			}
		} catch (\Exception $e) {

			print_r("ERROR");
			die;

		}


		if (null != $csvfinaldata) {

			foreach ($csvfinaldata as $key => $eachproduct) {

				try {
					

					$productObj = new DataObject\Blocks\Listing();
					$productObj->setCondition("o_key = ?", $eachproduct['filename']); 
					
					$data = $productObj->load();


					if (null != $data) {

					continue;
						

					} else {
						
							$productParentObj = DataObject::getByPath('/BMMI/AddressPicker');
							$productParentId = $productParentObj->getO_id();
						
						

						$new_product = new DataObject\Blocks();

						
						$new_product->setBlockNumber($eachproduct['BlockNumber']);
							$new_product->setBlockName($eachproduct['BlockName']);
							$new_product->setRoad($eachproduct['Road']);
							$new_product->setBuilding($eachproduct['Building']);
							$new_product->setRegion($eachproduct['Region']);
							$new_product->setCountry($eachproduct['Country']);
							$new_product->setLatitude($eachproduct['Latitude']);
							$new_product->setLongitude($eachproduct['Longitude']);

							$new_product->setPublished($eachproduct['published']);
							$new_product->setParentId($productParentId);
						$new_product->setKey($eachproduct['filename']);
						$new_product->save();
					}

				} catch (\Exception $e) {

				}

			}
		} else {

		}

	}




}
?>