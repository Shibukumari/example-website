<?php
namespace BmmiBundle\StoreBundle\Command;

use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\ApplicationLogger;
use Pimcore\Log\FileObject;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InventoryCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('inventoryprice:update')->setDescription('Updating Inventory');
	}
	protected function execute(InputInterface $input, OutputInterface $output) {

		$logger = \Pimcore\Log\ApplicationLogger::getInstance("Inventory Import", true); /* Object for creating log */
		$settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/

		echo "Initialization" . PHP_EOL;
		try {
			$host = $settings->getFtp_host();
			$port = $settings->getPort();
			$username = $settings->getFtp_username();
			$privateKey = $settings->getFtp_privatekey();

			$productArchive = $settings->getInventoryBasePath();
			$sftp = new SFTP($host, $port);
			$Key = new RSA();
			$Key->loadKey($privateKey);
			if (!$sftp->login($username, $Key)) {
				throw new \Exception("SFTP Not Connected.");
			}

			$remoteDir = $settings->getInventoryRemotePath();
			$localDir = $settings->getInventoryLocalPath();

			if (!$sftp->get($remoteDir, $localDir)) {
				throw new \Exception("File Not available in server.");
			}
			//$sftp->put($localDir, $valfromget);
		} catch (\Exception $e) {
			echo "Error in FTP / File Not available in server" . PHP_EOL;
			$logger->log("ERROR", $e->getMessage());
			die;
		}

		try {

			if (($h = fopen("{$localDir}", "r")) !== FALSE) {

				while (($data = fgetcsv($h, 10000, ";")) !== FALSE) {
					$csvdata[] = $data;
				}

				fclose($h);
			}

			$keys = array_shift($csvdata);

			$csvfinaldata = array();

			foreach ($csvdata as $i => $row) {

				if (count($keys) != count($row)) {

					throw new \Exception("Count of Key and Row is mismatch : " . ($i + 1));
				} else {
					$csvfinaldata[$i] = array_combine($keys, $row);

				}
			}
		} catch (\Exception $e) {

			$fileObject = new FileObject('some error in CSV');

			$logger->log("ERROR", $e->getMessage(), ['fileObject' => $fileObject]);
		}

		$productPriceObj = 'Pimcore\\Model\\DataObject\\BMMIProductPrice';
		$fieldDatatypeObj = new $productPriceObj();
		$fieldDatatype = $fieldDatatypeObj->getClass()->getFieldDefinitions();

		if (null != $csvfinaldata) {
			foreach ($csvfinaldata as $key => $eachproduct) {
				if (isset($eachproduct['GOLD_CODE'])) {

					$dateValue = $dateValue2 = '';
					$eachproduct['Sku'] = $eachproduct['GOLD_CODE'];
					$eachproduct['StoreId'] = $eachproduct['LOCATION_ID'];
					if (isset($eachproduct['LOCATION_STOCK'])) {
						$eachproduct['LocationStock'] = $eachproduct['LOCATION_STOCK'];
					}
					if (isset($eachproduct['SU_SELLING_PRICE'])) {
						$eachproduct['Price'] = $eachproduct['SU_SELLING_PRICE'];
					}
					if (isset($eachproduct['SPECIAL_PRICE'])) {
						$eachproduct['SpecialPrice'] = $eachproduct['SPECIAL_PRICE'];
					}
					if (isset($eachproduct['SPECIAL_PRICE_START'])) {
						$eachproduct['FromDate'] = $this->dateFormat($eachproduct['SPECIAL_PRICE_START']);
					}
					if (isset($eachproduct['SPECIAL_PRICE_END'])) {
						$eachproduct['ToDate'] = $this->dateFormat($eachproduct['SPECIAL_PRICE_END']);
					}

					if (isset($eachproduct['VAT_CODE'])) {
						$eachproduct['VatCode'] = $eachproduct['VAT_CODE'];
					}
					if (isset($eachproduct['ORDERABLE_FLAG'])) {
						$eachproduct['OrderableFlag'] = $eachproduct['ORDERABLE_FLAG'];
					}

					if (isset($eachproduct['MANUFACTURED_LINK'])) {
						$eachproduct['ManufacturedLink'] = $eachproduct['MANUFACTURED_LINK'];
					}

					try {

						echo "Importing " . $eachproduct['Sku'] . PHP_EOL;

						$list = new DataObject\BMMIProductPrice\Listing();
						$list->setCondition("o_key = " . $list->quote(strtolower($eachproduct['Sku'] . '-' . $eachproduct['StoreId'])));
						$list->setUnpublished(true);
						$list->setLimit(1);
						$productObjData = $list->load();

						if (null != $productObjData[0]) {

							$productObj = $productObjData[0];
							foreach ($eachproduct as $fieldName => $value) {

								if (method_exists($productObj, 'get' . ucfirst($fieldName))) {
									$productObj->set($fieldName, $value);
								}
							}
							if ($productObj->get('LocationStock') > $productObj->get('notify_quantity_below')) {
								$productObj->set('stock_availability', 'In Stock');
							} else {
								$productObj->set('stock_availability', 'Out of Stock');
							}
							if (isset($eachproduct['published'])) {
								$productObj->setPublished($eachproduct['published']);
							}
							if (isset($eachproduct['delivery_type'])) {
								$productObj->setDelivery_type(explode(",", $eachproduct['delivery_type']));
							}

							$productObj->save();
							$logger->log("INFO", "Inventory is Updated :" . ($key + 1));

						} else {

							$priceParentObj = DataObject::getByPath('/BMMI/ProductPrice');
							$priceParentId = $priceParentObj->getO_id();

							$new_product = new $productPriceObj();
							$new_product_obj = $new_product->create($eachproduct);
							$new_product_obj->setParentId($priceParentId);
							$new_product_obj->setKey(strtolower($eachproduct['Sku'] . '-' . $eachproduct['StoreId']));
							if (isset($eachproduct['published'])) {
								$new_product_obj->setPublished($eachproduct['published']);
							}
							if (isset($eachproduct['delivery_type'])) {
								$new_product_obj->setDelivery_type(explode(",", $eachproduct['delivery_type']));
							}
							if (!isset($eachproduct['notify_quantity_below'])) {
								$eachproduct['notify_quantity_below'] = 3;
								$new_product_obj->set('notify_quantity_below', $eachproduct['notify_quantity_below']);
							}
							if (!isset($eachproduct['Store_Status'])) {
								$eachproduct['Store_Status'] = 0;
								$new_product_obj->set('Store_Status', $eachproduct['Store_Status']);
							}
							if (!isset($eachproduct['notify_quantity_below']) && ($eachproduct['LocationStock'] > $eachproduct['notify_quantity_below'])) {
								$new_product_obj->set('stock_availability', 'In Stock');
							} else {
								$new_product_obj->set('stock_availability', 'Out of Stock');
							}

							$new_product_obj->save();
							$logger->log("INFO", "New Inventory Created :" . ($key + 1));
						}

					} catch (\Exception $e) {
						$logger->log("ERROR", $e->getMessage());

					}
				} elseif (isset($eachproduct['Oracle_Code'])) {
					$allChildSku = array();
					$productMaster = new DataObject\Products\Listing();
					$productMaster->setCondition("oracle_ref = ?", $eachproduct['Oracle_Code']);
					$productMasterData = $productMaster->load();
					if ($productMasterData) {
						foreach ($productMasterData as $productMasterValue) {
							$allChildSku[] = $productMasterValue->getSku();

						}
					}

					if (!empty($allChildSku)) {
						foreach ($allChildSku as $ChildSku) {

							$list = new DataObject\BMMIProductPrice\Listing();
							$list->setCondition("o_key = " . $list->quote(strtolower($ChildSku . '-' . $eachproduct['StoreId'])));
							$list->setUnpublished(true);
							$list->setLimit(1);
							$productObjData = $list->load();
							if (null != $productObjData[0]) {
								$productObj = $productObjData[0];
								$productObj->setNotify_quantity_below($eachproduct['notify_quantity_below']);
								$productObj->setDelivery_type(explode(",", $eachproduct['delivery_type']));
								$productObj->setStore_Status($eachproduct['Store_Status']);

								$productObj->setPublished(1);
								$productObj->save();
								$logger->log("INFO", "Inventory is Updated :" . ($key + 1));

							} else {

								$logger->log("INFO", "NO Inventory For :" . ($key + 1));
							}

						}
					}

				}
			}
			echo "Imported" . PHP_EOL;
			$tfileName = date("d_m_Y_H_i_s", time());
			$archiveFile = $productArchive . '/' . $tfileName . '_inventory_import_archive.csv';
			$sftp->get($remoteDir, $archiveFile);
			$sftp->delete($remoteDir, false);
		} else {

		}
	}

	public function dateFormat($attrValue) {
		if ($attrValue != '') {
			$dateValue = new \Carbon\Carbon();
			$dateValue->setTimestamp(strtotime($attrValue));
			return $dateValue;
		} else {
			return NULL;
		}

	}

}