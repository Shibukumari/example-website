<?php
namespace BmmiBundle\StoreBundle\Command;

use DateInterval;
use DatePeriod;
use DateTime;
use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Folder;
use Pimcore\Model\User;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\DriverManager;
use \PDO;

class SlotsCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('slotdetails:update')->setDescription('Updating Slot Details');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$setting = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
		$expressModeUrl = $setting->getExpressModeUrl();
		$standardModeUrl = $setting->getStandardModeUrl();
		$pickupModeUrl = $setting->getPickupModeUrl();
		$dbhost = $setting->getDb_host();
		$dbuser = $setting->getDb_user();
		$dbpass = $setting->getDb_pass();
		$dbname = $setting->getDb_name();
		$connectionParams = array('dbname' => $dbname,'user' => $dbuser,'password' => $dbpass,'host' => $dbhost,'driver' => 'pdo_mysql');

		$conn = DriverManager::getConnection($connectionParams);
		

		if (null != $standardModeUrl && null != $expressModeUrl && null != $pickupModeUrl) {

			
			$userId = User::getByName($setting->getUsername())->getId();
			$basePath = $setting->getSlotBasePath();

			\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);

			$todayDate = date("Y-m-d");
			$startDate = date('Y-m-d', strtotime($todayDate));
			//$startDate = date('Y-m-d', strtotime($todayDate . ' + 8 days'));
			$endDate = date('Y-m-d', strtotime($todayDate . ' + 30 days'));
			$nextFewDate = $this->getDatesFromRange($startDate, $endDate);

			$storeObjOrg = new DataObject\Stores\Listing();
			$storeObjOrg->setCondition("Enabled = ?", "1");
			$storeObj = $storeObjOrg->load();

			foreach ($storeObj as $allstore) {
				$storeDetail['code'] = $allstore->getCode();
				$storeDetail['objid'] = $allstore->getO_id();
				$storeDetails[] = $storeDetail;
			}

			if (null != $storeDetails) {
				foreach ($storeDetails as $storeDetail) {
					$storeObjId = $storeDetail['objid'];
					$holidayLists = new DataObject\BMMIHolidays\Listing();
					$holidayLists->setCondition("StoreId like '%," . $storeObjId . ",%'");
					$allHolidays = $holidayLists->load();

					if (null != $allHolidays) {
						foreach ($allHolidays as $allHoliday) {
							foreach ($allHoliday->getDateList() as $DateList) {
								
								$currentDate = strtotime($DateList['Date']->getData()->date);
								$slotclassId = new DataObject\BMMISlotLists();

								$sql = "UPDATE object_query_" . $slotclassId->getO_classId() . " SET Enable = 0 WHERE Date = :Date";
								$stmt = $conn->prepare($sql);
								$stmt->bindValue(':Date', $currentDate, PDO::PARAM_INT);
								$stmt->execute();
								$hoildaysDateFormat[] = date('Y-m-d', strtotime($DateList['Date']->getData()->date));

							}
						}
						$storeAvailableDates = array_diff($nextFewDate, $hoildaysDateFormat);
					} else {
						$storeAvailableDates = $nextFewDate;
					}
					
					foreach ($storeAvailableDates as $storeAvailableDate) {
						$day = date("l", strtotime($storeAvailableDate));
						$slotLists = new DataObject\BMMISlots\Listing();

						$slotLists->addConditionParam("StoreId__id = ?", $storeDetail['objid'], "AND");
						$slotLists->setCondition("Day = ?", $day);
						$daySlotLists = $slotLists->load();
						
						if (null != $daySlotLists) {
							foreach ($daySlotLists as $daySlotList) {
															
								$timingSlots = $daySlotList->getSlotTiming();


								if (!empty($timingSlots)) {
									if ($daySlotList->getDeliveryMode() == 'Pickup') {
										
										foreach ($timingSlots as $slotNumber => $timingSlot) {
										

										$slotListObj = new DataObject\BMMISlotLists();
										$className = 'SlotLists';
										$slotMode = $daySlotList->getDeliveryMode();
										$slotId = $storeDetail['code'] . date("Ymd", strtotime($storeAvailableDate)) . $slotNumber . $daySlotList->getO_id() . $slotMode[0];

										$folderExists = DataObject::getByPath($basePath . $className . '/' . $slotMode . "/" . $storeDetail["code"]);

										$parentFolderId = Folder::getByPath($basePath . $className . '/' . $slotMode)->getId();

										if (null != $folderExists) {

											
											$folderId = Folder::getByPath($basePath . $className . '/' . $slotMode . '/' . $storeDetail["code"])->getId();

										} else {

											$folderId = $this->createFolder($storeDetail["code"], $parentFolderId, $userId, $slotMode, $className, $basePath);

										}
										$slotPriorTime = -($timingSlot['SlotPickingTime']->getData() + $timingSlot['SlotPriorTime']->getData()) . ' minutes';
										$slotPickerTime = -$timingSlot['SlotPickingTime']->getData() . ' minutes';
										$checkingSlot = DataObject\BMMISlotLists::getBySlotId($slotId, ['limit' => 1, 'unpublished' => false]);
										if (null != $checkingSlot) {
											continue;
										}

										$slotListObj->setKey(\Pimcore\File::getValidFilename($slotId));
										$slotListObj->setParentId($folderId);
										$dateValue = new \Carbon\Carbon();
										$dateValue->setTimestamp(strtotime($storeAvailableDate));
										$slotListObj->setDate($dateValue);
										$slotListObj->setStoreId($storeDetail['code']);
										$slotListObj->setDeliveryMode($slotMode);
										$slotListObj->setSlotId($slotId);
										$slotListObj->setRealFrom($timingSlot['From']->getData());
										$slotListObj->setRealTo($timingSlot['To']->getData());

										$slotListObj->setSlotPriorTime(date('H:i', strtotime($slotPriorTime, strtotime($timingSlot['From']->getData()))));
										$slotListObj->setSlotPickingTime(date('H:i', strtotime($slotPickerTime, strtotime($timingSlot['From']->getData()))));
										$slotListObj->setMaxOrderCount($timingSlot['MaxOrderCount']->getData());
										$slotListObj->setCharges($timingSlot['Charges']->getData());
										//$slotListObj->setZoneGroup($zoneGroupList);
										$slotListObj->setEnable(1);
										$slotListObj->setPublished(1);
										$slotListObj->save();
										
									}
									
									}else{
										
										foreach ($timingSlots as $slotNumber => $timingSlot) {
										$includeZoneGroup = array();
										$allZoneGroupList = array();
										$includeZoneGroup = $timingSlot['ZoneGroup']->getData();

										if (empty($includeZoneGroup)) {
											$settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/

											foreach ($settings->getZoneNames() as $allzoneGroups) {


												$allZoneGroupObj = 'Pimcore\\Model\\DataObject\\' . $allzoneGroups['zoneClassNames']->getData() . '\\Listing';

												$allZoneGroupObjs = new $allZoneGroupObj();
												$allZoneGroupList[] = $allZoneGroupObjs->load();

											}

											foreach ($allZoneGroupList as $zoneGroupObj) {

												foreach ($zoneGroupObj as $zoneGroup) {
													
													$includeZoneGroup[] = $zoneGroup->getZoneGroupName() . ' - ' . $zoneGroup->getO_className();

												}
											}
										}
										if ($excludeZonedata = $timingSlot['ZoneGroupExclude']) {
											$excludeZoneGroup = $excludeZonedata->getData();
											$includeZoneGroup = array_diff($includeZoneGroup, $excludeZoneGroup);
										}



								foreach ($includeZoneGroup as $zoneGroupArray => $zoneGroupList) {

										$slotListObj = new DataObject\BMMISlotLists();
										$className = 'SlotLists';
										$slotMode = $daySlotList->getDeliveryMode();
										$slotId = $storeDetail['code'] . date("Ymd", strtotime($storeAvailableDate)) . $zoneGroupArray . $slotNumber . $daySlotList->getO_id() . $slotMode[0];

										$folderExists = DataObject::getByPath($basePath . $className . '/' . $slotMode . "/" . $storeDetail["code"]);

										
										$parentFolderId = Folder::getByPath($basePath . $className . '/' . $slotMode)->getId();
										if (null != $folderExists) {

											
											$folderId = Folder::getByPath($basePath . $className . '/' . $slotMode . '/' . $storeDetail["code"])->getId();

										} else {


											$folderId = $this->createFolder($storeDetail["code"], $parentFolderId, $userId, $slotMode, $className, $basePath);

										}
										$slotPriorTime = -($timingSlot['SlotPickingTime']->getData() + $timingSlot['SlotPriorTime']->getData()) . ' minutes';
										$slotPickerTime = -$timingSlot['SlotPickingTime']->getData() . ' minutes';
										$checkingSlot = DataObject\BMMISlotLists::getBySlotId($slotId, ['limit' => 1, 'unpublished' => false]);
										if (null != $checkingSlot) {
											continue;
										}

										$slotListObj->setKey(\Pimcore\File::getValidFilename($slotId));
										$slotListObj->setParentId($folderId);
										$dateValue = new \Carbon\Carbon();
										$dateValue->setTimestamp(strtotime($storeAvailableDate));
										$slotListObj->setDate($dateValue);
										$slotListObj->setStoreId($storeDetail['code']);
										$slotListObj->setDeliveryMode($slotMode);
										$slotListObj->setSlotId($slotId);
										$slotListObj->setRealFrom($timingSlot['From']->getData());
										$slotListObj->setRealTo($timingSlot['To']->getData());

										$slotListObj->setSlotPriorTime(date('H:i', strtotime($slotPriorTime, strtotime($timingSlot['From']->getData()))));
										$slotListObj->setSlotPickingTime(date('H:i', strtotime($slotPickerTime, strtotime($timingSlot['From']->getData()))));
										$slotListObj->setMaxOrderCount($timingSlot['MaxOrderCount']->getData());
										$slotListObj->setCharges($timingSlot['Charges']->getData());
										$slotListObj->setZoneGroup($zoneGroupList);
										$slotListObj->setMinCartPrice($timingSlot['MinCartPrice']->getData());
										$slotListObj->setEnable(1);
										$slotListObj->setPublished(1);
										$slotListObj->save();
										}
									}
								}
									

								}
							}
						} else {
							continue;
						}

					}

				}
			}

		}

	}

	public function getDatesFromRange($start, $end, $format = 'Y-m-d') {

		$array = array();
		$interval = new DateInterval('P1D'); // Variable that store the date interval of period 1 day

		$realEnd = new DateTime($end);
		$realEnd->add($interval);

		$period = new DatePeriod(new DateTime($start), $interval, $realEnd);

		foreach ($period as $date) {
			$array[] = $date->format($format);
		}

		return $array;
	}

	public function createFolder($folderId, $parentFolderId, $userId, $slotMode, $classname, $basePath) {

		$folder = DataObject\Folder::create(array("o_parentId" => $parentFolderId, "o_creationDate" => time(), "o_userOwner" => $userId, "o_userModification" => $userId, "o_key" => $folderId, "o_published" => true));
		$folder->setCreationDate(time());
		$folder->setUserOwner($userId);
		$folder->setUserModification($userId);
		try {
			$folder->save();

			$folderObjId = Folder::getByPath($basePath . $classname . '/' . $slotMode . '/' . $folderId)->getId();
		} catch (\Exception $e) {
			return 0;
		}
		return $folderObjId;

	}

	public function createModeFolder($modeParentId, $userId, $fullModeUrl) {

		$folderName = basename($fullModeUrl);

		$folder = DataObject\Folder::create(array("o_parentId" => $modeParentId, "o_creationDate" => time(), "o_userOwner" => $userId, "o_userModification" => $userId, "o_key" => $folderName, "o_published" => true));
		$folder->setCreationDate(time());
		$folder->setUserOwner($userId);
		$folder->setUserModification($userId);
		try {
			$folder->save();

			$folderObjId = Folder::getByPath($fullModeUrl);
		} catch (\Exception $e) {
			return 0;
		}
		return 1;

	}
}
?>