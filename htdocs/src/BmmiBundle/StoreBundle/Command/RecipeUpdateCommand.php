<?php
namespace BmmiBundle\StoreBundle\Command;

use BmmiBundle\StoreBundle\Website\RecipeValidation;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\ApplicationLogger;
use Pimcore\Model\Asset;
use Pimcore\Model\Asset\Image;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Data\BlockElement;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RecipeUpdateCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('recipedetails:update')->setDescription('Updating Recipe Details');
	}
	protected function execute(InputInterface $input, OutputInterface $output) {
		$logger = \Pimcore\Log\ApplicationLogger::getInstance("Product Import", true); /* Object for creating log */
		$settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/
		$datetypearray = array('inputFormat', 'numericFormat', 'textareaFormat', 'selectFormat', 'wysiwygFormat', 'booleanSelectFormat', 'manyToOneRelation');
		try {
			$host = $settings->getFtp_host();
			$port = $settings->getPort();
			$username = $settings->getFtp_username();
			$privateKey = $settings->getFtp_privatekey();
			$logfileDir = $settings->getLogDir();
			$assetlogfileDir = $settings->getAssetlogDir();
			$productArchive = $settings->getRecipeArchive();
			$sftp = new SFTP($host, $port);
			$Key = new RSA();
			$Key->loadKey($privateKey);

			if (!$sftp->login($username, $Key)) {
				throw new \Exception("SFTP Not Connected.");
			}

			$remoteDir = $settings->getRecipeRemoteURL();
			$localDir = $settings->getRecipeLocalURL();

			if (!$sftp->get($remoteDir, $localDir)) {
				throw new \Exception("File Not available in server.");
			}

		} catch (\Exception $e) {
			print_r("error");
			die;
		}

		try {

			if (($h = fopen("{$localDir}", "r")) !== FALSE) {

				while (($data = fgetcsv($h, 1000, ";")) !== FALSE) {
					$csvdata[] = $data;
				}

				fclose($h);
			}

			$keys = array_shift($csvdata);

			$csvfinaldata = array();
			foreach ($csvdata as $i => $row) {

				if (count($keys) != count($row)) {
					throw new \Exception("Count of Key and Row is mismatch : " . ($i + 1));
				} else {
					$csvfinaldata[$i] = array_combine($keys, $row);

				}
			}
		} catch (\Exception $e) {

			print_r("ERROR");
			die;

		}

		$simple_recipe = 'Pimcore\\Model\\DataObject\\Recipe';
		$simple_product = 'Pimcore\\Model\\DataObject\\Products';

		$fieldDatatypeObj = new $simple_recipe();
		$fieldDatatype = $fieldDatatypeObj->getClass()->getFieldDefinitions();

		echo "Validation Started" . PHP_EOL;
		$getres = new RecipeValidation();
		$errortxt = $getres->checkAttribute($csvfinaldata, $fieldDatatype);
		if (null != $errortxt) {
			$tfileName = date("d_m_Y_H_i_s", time());
			$logLocation = $logfileDir . $tfileName . '_recipe_import_log.txt';
			$sftp->put($logLocation, $errortxt);
			$sftp->chmod(0777, $logLocation);
			$logAsset = new Asset();
			$logAsset->setFilename($tfileName . '_recipe_import_log.txt');
			$logAsset->setData(file_get_contents($logLocation));
			$logAsset->setParent(Asset::getByPath($assetlogfileDir));
			$logAsset->save();
			echo PHP_EOL . "Please check the validation log" . PHP_EOL;
			die;
		}

		if (null != $csvfinaldata) {
			foreach ($csvfinaldata as $key => $eachproduct) {
				try {
					echo "Importing " . $eachproduct['sku'] . PHP_EOL;
					$productObj = $simple_recipe::getBySku($eachproduct['sku'], ['limit' => 1, 'unpublished' => true]);
					$eachproduct['is_active_in_pim'] = 'yes';
					if (null != $productObj) {
						$allIngredientsData = array();
						$instructionsData = array();
						foreach ($eachproduct as $fieldName => $value) {
							$value = trim($value);
							if (method_exists($productObj, 'get' . ucfirst($fieldName))) {
								if ($fieldName != "published" && $fieldName != "status") {

									$datetypeCol = $fieldDatatype[$fieldName]->getFieldType() . 'Format';
									if (null == $datetypeCol) {
										continue;
									}

									$funCheck = in_array($datetypeCol, $datetypearray);

									if (!$funCheck) {
										$value = $this->$datetypeCol($value, $key, $fieldName, $eachproduct['websites']);
									}
								}
								$productObj->set($fieldName, $value);
							} else {
								if ($fieldName == 'steps') {
									$allStepsValues = explode("||", trim($value, "||"));
									foreach ($allStepsValues as $stepsValue) {
										$stepsData = ["steps" => new BlockElement('steps', 'textarea', $stepsValue)];

										$instructionsData[] = $stepsData;
									}
									$productObj->setInstructions($instructionsData);
								}

								if ($fieldName == 'DisplayName') {
									$allDisplayValues = explode("||", trim($value, "||"));
									$allProductValues = explode("||", trim($eachproduct['Product'], "||"));
									$allQuantityValues = explode("||", trim($eachproduct['Quantity'], "||"));

									$displayValues = array_filter($allDisplayValues);
									$productValues = array_filter($allProductValues);
									$quantityValues = array_filter($allQuantityValues);

									$mergedIngredients = array_map(function () {return func_get_args();}, $displayValues, $productValues, $quantityValues);

									foreach ($mergedIngredients as $mergedIngredient) {

										$ingredientProduct = $simple_product::getBySku(trim($mergedIngredient[1]), ['limit' => 1, 'unpublished' => false]);
										//print_r($mergedIngredient[1]);
										$ingredientsData = [
											"DisplayName" => new BlockElement('DisplayName', 'input', $mergedIngredient[0]),
											"Product" => new BlockElement('Product', 'manyToOneRelation', $ingredientProduct),
											"Quantity" => new BlockElement('Quantity', 'number', trim($mergedIngredient[2])),
										];
										$allIngredientsData[] = $ingredientsData;
									}

									$productObj->setIngredients($allIngredientsData);
								}
							}
						}

						$productObj->setPublished(1);

						$productObj->setStatus(true);

						$productObj->save();
						$logger->log("INFO", "Product is Updated :" . ($key + 1));

					} else {
						if (isset($eachproduct['recipe_folder'])) {
							$productParentObj = DataObject::getByPath($eachproduct['recipe_folder']);
							$productParentId = $productParentObj->getO_id();
						} else {
							echo "Skiping the import, Please check the folder path : " . ($key + 1);
							continue;
						}
						$new_recipe = new $simple_recipe();

						foreach ($eachproduct as $fieldName => $value) {
							$value = trim($value);
							if (method_exists($new_recipe, 'get' . ucfirst($fieldName))) {
								if ($fieldName != "published" && $fieldName != "status") {
									$datetypeCol = $fieldDatatype[$fieldName]->getFieldType() . 'Format';
									if (null == $datetypeCol) {
										continue;
									}
									$funCheck = in_array($datetypeCol, $datetypearray);
									if (!$funCheck) {
										$value = $this->$datetypeCol($value, $key, $fieldName, $eachproduct['websites']);

									}
								}

								$new_recipe->set($fieldName, $value);

							} else {
								if ($fieldName == 'steps') {
									$allStepsValues = explode("||", trim($value, "||"));
									foreach ($allStepsValues as $stepsValue) {
										$stepsData = ["steps" => new BlockElement('steps', 'textarea', $stepsValue)];

										$instructionsData[] = $stepsData;
									}
									$new_recipe->setInstructions($instructionsData);
								}

								if ($fieldName == 'DisplayName') {
									$allDisplayValues = explode("||", trim($value, "||"));
									$allProductValues = explode("||", trim($eachproduct['Product'], "||"));
									$allQuantityValues = explode("||", trim($eachproduct['Quantity'], "||"));

									$displayValues = array_filter($allDisplayValues);
									$productValues = array_filter($allProductValues);
									$quantityValues = array_filter($allQuantityValues);

									$mergedIngredients = array_map(function () {return func_get_args();}, $displayValues, $productValues, $quantityValues);

									foreach ($mergedIngredients as $mergedIngredient) {

										$ingredientProduct = $simple_product::getBySku(trim($mergedIngredient[1]), ['limit' => 1, 'unpublished' => false]);
										//print_r($mergedIngredient[1]);
										$ingredientsData = [
											"DisplayName" => new BlockElement('DisplayName', 'input', $mergedIngredient[0]),
											"Product" => new BlockElement('Product', 'manyToOneRelation', $ingredientProduct),
											"Quantity" => new BlockElement('Quantity', 'number', trim($mergedIngredient[2])),
										];
										$allIngredientsData[] = $ingredientsData;
									}

									$new_recipe->setIngredients($allIngredientsData);
								}
							}

						}
						$new_recipe->setPublished(1);
						$new_recipe->setParentId($productParentId);
						$new_recipe->setKey($eachproduct['sku']);
						$new_recipe->setStatus(true);
						$new_recipe->save();
						$logger->log("INFO", "Product is Created :" . ($key + 1));
					}

				} catch (\Exception $e) {
					$logger->log("ERROR", $e->getMessage());

				}
			}

			echo "Imported" . PHP_EOL;
			$tfileName = date("d_m_Y_H_i_s", time());
			$archiveFile = $productArchive . '/' . $tfileName . '_recipe_import_archive.csv';

			$sftp->get($remoteDir, $archiveFile);
			$sftp->delete($remoteDir, false);

		} else {

		}
	}
	public function manyToManyObjectRelationFormat($attrValue, $key, $fieldName, $websites) {
		if ($attrValue != '' && $fieldName == 'category_ids') {

			$categorys = explode("||", trim($attrValue, "||"));

			$logger = \Pimcore\Log\ApplicationLogger::getInstance("Product Import", true); /* Object for creating log */
			$category_ids = array();
			foreach ($categorys as $category) {

				$siteName = explode('/', trim($category, "/"));
				$classname = 'Pimcore\\Model\\DataObject\\Category';
				$categoryObj = $classname::getByPath(trim($category));

				if (null != $categoryObj) {
					$category_ids[] = $categoryObj;

				}

			}
			return $category_ids;
		} else {
			return $attrValue;
		}
	}

	public function imageFormat($attrValue, $key, $fieldName, $websites) {

		if ($attrValue != '') {
			$image_value = Image::getByPath(trim($attrValue));
			if (null != $image_value) {
				return $image_value;
			} else {
				$logger = \Pimcore\Log\ApplicationLogger::getInstance("Product Import", true);
				$logger->log("WARNING", $fieldName . ' Image Not Found : ' . ($key + 1));
				return NULL;

			}
		}

	}

	public function multiselectFormat($attrValue, $key, $fieldName, $websites) {
		if ($attrValue != '') {
			$attrValue = explode("||", trim($attrValue, "||"));
			return $attrValue;
		}
	}

}