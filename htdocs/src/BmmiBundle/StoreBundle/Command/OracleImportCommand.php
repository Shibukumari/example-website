<?php
namespace BmmiBundle\StoreBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OracleImportCommand extends AbstractCommand {
	protected function configure() {
		$this->setName('oracleinventory:update')->setDescription('Updating Oracle Inventory');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(false);
		$settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/
		$url = $settings->getOracleUrl();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $settings->getOracleKey(), 'Content-Type:application/xml', 'accept: */*'));

		$result = curl_exec($ch);
		$xml = simplexml_load_string($result) or die("Error: Cannot create object");

		$encodeValue = json_encode($xml);
		$inArrayFormat = json_decode($encodeValue, true);

		$productPriceObj = 'Pimcore\\Model\\DataObject\\BMMIProductPrice';
		foreach ($inArrayFormat['Data'] as $eachproduct) {

			$allChildSku = array();
			$productMaster = new DataObject\Products\Listing();
			$productMaster->setCondition("oracle_ref = ?", $eachproduct['ITEM_CODE']);
			$productMasterData = $productMaster->load();
			if (!empty($productMasterData)) {

				foreach ($productMasterData as $productMasterValue) {
					$allSku['sku'] = $productMasterValue->getSku();
					$allSku['uomsize'] = $productMasterValue->getUomsize();
					$allChildSku[] = $allSku;
				}
			}

			if (!empty($allChildSku)) {
				foreach ($allChildSku as $ChildSku) {

					$list = new DataObject\BMMIProductPrice\Listing();
					$list->setCondition("o_key = " . $list->quote(strtolower($ChildSku['sku'] . '-' . $eachproduct['ORG_CODE'])));
					$list->setUnpublished(false);
					$list->setLimit(1);
					$productObjData = $list->load();

					if (null != $productObjData[0]) {
						$productObj = $productObjData[0];

						$storeDetails = $settings->getPriceUpdate();
						$linkedPrice = new DataObject\BMMIProductPrice\Listing();
						$linkedPrice->setCondition("o_key = ? AND Store_Status = ?", [strtolower($ChildSku['sku'] . '-' . $storeDetails), 1]);
						$linkedPrice->setUnpublished(false);
						$linkedPrice->setLimit(1);
						$linkedDetails = $linkedPrice->load();

						if (!empty($linkedDetails)) {
							//$productObj->set('Price', $linkedDetails[0]->getPrice());
							//$productObj->set('SpecialPrice', $linkedDetails[0]->getSpecialPrice());
							//$productObj->set('FromDate', $linkedDetails[0]->getFromDate());
							//$productObj->set('ToDate', $linkedDetails[0]->getToDate());
							//$productObj->set('VatCode', $linkedDetails[0]->getVatCode());
							//$productObj->set('OrderableFlag', $linkedDetails[0]->getOrderableFlag());
							//$productObj->set('ManufacturedLink', $linkedDetails[0]->getManufacturedLink());
							$productObj->set('Store_Status', $linkedDetails[0]->getStore_Status());

							if (null != $eachproduct['ON_HAND_QTY'] && $eachproduct['UNITS_PUOM'] != null && $ChildSku['uomsize'] == 0) {
								$eachproduct['LocationStock'] = $eachproduct['ON_HAND_QTY'] * $eachproduct['UNITS_PUOM'];
								$productObj->set('LocationStock', $eachproduct['LocationStock']);
							} elseif (null != $eachproduct['ON_HAND_QTY'] && $eachproduct['UNITS_PUOM'] != null && $ChildSku['uomsize'] != 0) {
								$unitValue = $eachproduct['ON_HAND_QTY'] / $ChildSku['uomsize'];
								$eachproduct['LocationStock'] = floor($unitValue * $eachproduct['UNITS_PUOM']);
								$productObj->set('LocationStock', $eachproduct['LocationStock']);
							}
							if ($productObj->get('LocationStock') > $productObj->get('notify_quantity_below')) {
								$productObj->set('stock_availability', 'In Stock');
							} else {
								$productObj->set('stock_availability', 'Out of Stock');
							}
							$productObj->setPublished(1);
							$productObj->save();
							echo $ChildSku['sku'] . '-' . $eachproduct['ORG_CODE'] . ' Imported ';
						}

					} else {

						$priceParentObj = DataObject::getByPath('/BMMI/ProductPrice');
						$priceParentId = $priceParentObj->getO_id();
						$eachproduct['Sku'] = $ChildSku['sku'];

						$new_product = new $productPriceObj();

						$storeDetails = $settings->getPriceUpdate();
						$linkedPrice = new DataObject\BMMIProductPrice\Listing();

						$linkedPrice->setCondition("o_key = ? AND Store_Status = ?", [strtolower($ChildSku['sku'] . '-' . $storeDetails), 1]);
						$linkedPrice->setUnpublished(false);
						$linkedPrice->setLimit(1);
						$linkedDetails = $linkedPrice->load();

						if (!empty($linkedDetails)) {
							$new_product->set('Price', $linkedDetails[0]->getPrice());
							$new_product->set('SpecialPrice', $linkedDetails[0]->getSpecialPrice());
							$new_product->set('FromDate', $linkedDetails[0]->getFromDate());
							$new_product->set('ToDate', $linkedDetails[0]->getToDate());
							$new_product->set('VatCode', $linkedDetails[0]->getVatCode());
							$new_product->set('OrderableFlag', $linkedDetails[0]->getOrderableFlag());
							$new_product->set('ManufacturedLink', $linkedDetails[0]->getManufacturedLink());
							$new_product->set('notify_quantity_below', '12');
							$new_product->set('Store_Status', '1');
							$new_product->setDelivery_type(['Standard Mode']);
							if (null != $eachproduct['ON_HAND_QTY'] && $eachproduct['UNITS_PUOM'] != null && $ChildSku['uomsize'] == 0) {
								$eachproduct['LocationStock'] = $eachproduct['ON_HAND_QTY'] * $eachproduct['UNITS_PUOM'];
								$new_product->set('LocationStock', $eachproduct['LocationStock']);
							} elseif (null != $eachproduct['ON_HAND_QTY'] && $eachproduct['UNITS_PUOM'] != null && $ChildSku['uomsize'] != 0) {
								$unitValue = $eachproduct['ON_HAND_QTY'] / $ChildSku['uomsize'];
								$eachproduct['LocationStock'] = floor($unitValue * $eachproduct['UNITS_PUOM']);
								$new_product->set('LocationStock', $eachproduct['LocationStock']);
							}

							if ($new_product->get('LocationStock') > $new_product->get('notify_quantity_below')) {
								$new_product->set('stock_availability', 'In Stock');
							} else {
								$new_product->set('stock_availability', 'Out of Stock');
							}

							$new_product->set('StoreId', $eachproduct['ORG_CODE']);
							$new_product->set('Sku', $eachproduct['Sku']);
							$new_product->setParentId($priceParentId);
							$new_product->setKey(strtolower($ChildSku['sku'] . '-' . $eachproduct['ORG_CODE']));
							$new_product->setPublished(1);
							$new_product->save();
							echo $ChildSku['sku'] . '-' . $eachproduct['ORG_CODE'] . ' Imported ';
						}

					}

				}
			}
		}
		//close cURL resource
		curl_close($ch);

	}
}