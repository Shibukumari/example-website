<?php
namespace BmmiBundle\StoreBundle\Command;

use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
use Pimcore\Console\AbstractCommand;
use Pimcore\Log\ApplicationLogger;
use Pimcore\Log\FileObject;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AssetsCommand extends AbstractCommand {
   protected function configure() {
      $this->setName('image:update')->setDescription('Updating Images');
   }

   protected function execute(InputInterface $input, OutputInterface $output) {

      $logger = \Pimcore\Log\ApplicationLogger::getInstance("Image Import", true); /* Object for creating log */

      $settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/

      $dir = $settings->getDirURL();
      $originalDir = $settings->getDirURL();
      $storeDir = "/";
      try {
         $logger->log("INFO", "Image Import is started");
         $this->dirToArray($dir, $storeDir, $originalDir);
         $logger->log("INFO", "Image Import is completed");
      }catch (\Exception $e) {
         $logger->log("ERROR", "Something Wrong");
         die;
      }
   }

   function dirToArray($dir, $storeDir, $originalDir) { 
   
   $result = array(); 

   $cdir = scandir($dir); 
   
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 

            $result[$value] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value , $storeDir, $originalDir); 

         } 
         else 
         { 
            //$result[] = $value; 
            $currentLocation = $dir . DIRECTORY_SEPARATOR . $value;
            $fileLocation = str_replace($originalDir, $storeDir , $dir);

            $newAsset = new Asset();
            $newAsset->setFilename($value);

            $oldAsset = Asset::getByPath($fileLocation . DIRECTORY_SEPARATOR . $value);
            if(null != $oldAsset){
               if($oldAsset->getFilename() == $value){
                  $oldAsset->delete();
               }
            }
            

            $newAsset->setData(file_get_contents($dir . DIRECTORY_SEPARATOR . $value));
            $newAsset->setParent(Asset::getByPath($fileLocation));
            $newAsset->save();
            unlink($currentLocation);
            
         } 
      } 
   } 

} 

}