<?php

namespace BmmiBundle\StoreBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class BmmiBundleStoreBundle extends AbstractPimcoreBundle {
	public function getJsPaths() {
		return [
			'/bundles/bmmibundlestore/js/pimcore/startup.js',
		];
	}

	public function deliveryMode() {
		return [
			'Standard Mode', 'Express Mode', 'Pickup',
		];
	}
}