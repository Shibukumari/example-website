<?php

namespace BmmiBundle\StoreBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends FrontendController
{
    /**
     * @Route("/bmmi_bundle_store")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from bmmi_bundle_store');
    }
}
