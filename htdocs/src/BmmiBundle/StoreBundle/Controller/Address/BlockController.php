<?php
namespace BmmiBundle\StoreBundle\Controller\Address;

use BmmiBundle\StoreBundle\Website\DataBaseConnector;
use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlockController extends AbstractRestController {

	/**
	 * @Route("/addresspicker", methods={"POST"})
	 *
	 * end point for getting address details
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/block/addresspicker?apikey=[API-KEY]
	 *      returns the address details of particular block,road,building
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function addressPickerAction(Request $request) {

		$blockDetails = json_decode($request->getContent(), true);
		$instance = DataBaseConnector::getInstance();
		$DBconnection = $instance->connectDB();

		$blockdataObj = new \Pimcore\Model\DataObject\Blocks();

		if (isset($blockDetails['brb'])) {

			if ($blockDetails['exactly'] != true) {

				if ($blockDetails['search'] == 'block') {

					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE BlockNumber LIKE :BlockNumber OR BlockName LIKE :BlockName LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);

					$queryStatement->bindValue(':BlockNumber', '%' . ltrim($blockDetails['brb']['block'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', '%' . $blockDetails['brb']['block'] . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);

					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();

				} elseif ($blockDetails['search'] == 'road') {
					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Road LIKE :Road AND (BlockNumber = :BlockNumber OR BlockName = :BlockName) LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);
					$queryStatement->bindValue(':BlockNumber', ltrim($blockDetails['brb']['block'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', $blockDetails['brb']['block'], \PDO::PARAM_STR);
					$queryStatement->bindValue(':Road', '%' . ltrim($blockDetails['brb']['road'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();

				} elseif ($blockDetails['search'] == 'building') {
					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Building LIKE :Building AND Road = :Road AND (BlockNumber = :BlockNumber OR BlockName = :BlockName) LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);
					$queryStatement->bindValue(':BlockNumber', ltrim($blockDetails['brb']['block'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', $blockDetails['brb']['block'], \PDO::PARAM_STR);
					$queryStatement->bindValue(':Road', ltrim($blockDetails['brb']['road'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':Building', '%' . ltrim($blockDetails['brb']['building'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();
				} else {

					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Building LIKE :Building AND Road LIKE :Road AND (BlockNumber LIKE :BlockNumber OR BlockName LIKE :BlockName) LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);
					$queryStatement->bindValue(':BlockNumber', '%' . ltrim($blockDetails['brb']['block'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', '%' . $blockDetails['brb']['block'] . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':Road', '%' . ltrim($blockDetails['brb']['road'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':Building', '%' . ltrim($blockDetails['brb']['building'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();

				}

			} else {

				if ($blockDetails['search'] == 'block') {
					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE BlockNumber = :BlockNumber OR BlockName = :BlockName LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);
					$queryStatement->bindValue(':BlockNumber', ltrim($blockDetails['brb']['block'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', $blockDetails['brb']['block'], \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);

					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();
				} elseif ($blockDetails['search'] == 'road') {
					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Road = :Road AND (BlockNumber = :BlockNumber OR BlockName = :BlockName) LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);
					$queryStatement->bindValue(':BlockNumber', ltrim($blockDetails['brb']['block'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', $blockDetails['brb']['block'], \PDO::PARAM_STR);
					$queryStatement->bindValue(':Road', ltrim($blockDetails['brb']['road'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();

				} elseif ($blockDetails['search'] == 'building') {

					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Building = :Building AND Road = :Road AND (BlockNumber = :BlockNumber OR BlockName = :BlockName) LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);
					$queryStatement->bindValue(':BlockNumber', ltrim($blockDetails['brb']['block'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', $blockDetails['brb']['block'], \PDO::PARAM_STR);
					$queryStatement->bindValue(':Road', ltrim($blockDetails['brb']['road'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':Building', ltrim($blockDetails['brb']['building'], "0"), \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();
				} else {
					$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Building LIKE :Building AND Road LIKE :Road AND (BlockNumber LIKE :BlockNumber OR BlockName LIKE :BlockName) LIMIT :limit";
					$queryStatement = $DBconnection->prepare($sql);
					$queryStatement->bindValue(':BlockNumber', '%' . ltrim($blockDetails['brb']['block'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':BlockName', '%' . $blockDetails['brb']['block'] . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':Road', '%' . ltrim($blockDetails['brb']['road'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':Building', '%' . ltrim($blockDetails['brb']['building'], "0") . '%', \PDO::PARAM_STR);
					$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
					$queryStatement->execute();
					$blockData = $queryStatement->fetchAll();
				}

			}

			if (!empty($blockData)) {

				return $this->createSuccessResponse($blockData);

			} else {

				$errorMessage = "Data Not Found";
				return $this->createErrorResponse($errorMessage);
			}
		} elseif (isset($blockDetails['br'])) {
			$blockDetails['br'] = strtok($blockDetails['br'], " ");

			$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE BlockNumber LIKE :BlockNumber OR BlockName LIKE :BlockName LIMIT :limit";
			$queryStatement = $DBconnection->prepare($sql);
			$queryStatement->bindValue(':BlockNumber', '%' . ltrim($blockDetails['br'], "0") . '%', \PDO::PARAM_STR);
			$queryStatement->bindValue(':BlockName', '%' . $blockDetails['br'] . '%', \PDO::PARAM_STR);
			$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
			$queryStatement->execute();

			$blockData = $queryStatement->fetchAll();

			if (!empty($blockData)) {

				return $this->createSuccessResponse($blockData);

			} else {
				$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Road LIKE :Road LIMIT :limit";
				$queryStatement = $DBconnection->prepare($sql);
				$queryStatement->bindValue(':Road', '%' . ltrim($blockDetails['br'], "0") . '%', \PDO::PARAM_STR);
				$queryStatement->bindValue(':limit', 10, \PDO::PARAM_INT);
				$queryStatement->execute();
				$blockData = $queryStatement->fetchAll();

				if (!empty($blockData)) {
					return $this->createSuccessResponse($blockData);
				} else {
					$errorMessage = "No Data For : " . $blockDetails['br'];
					return $this->createErrorResponse($errorMessage);
				}

			}

		} elseif (isset($blockDetails['reverse'])) {

			$sql = "SELECT BlockNumber,BlockName,Road,Building,Region,Country,Latitude,Longitude FROM object_" . $blockdataObj->getO_classId() . " WHERE Latitude LIKE :Latitude AND Longitude LIKE :Longitude";

			$queryStatement = $DBconnection->prepare($sql);
			$queryStatement->bindValue(':Latitude', $this->numberFormatPrecision($blockDetails['reverse']['y']) . '%', \PDO::PARAM_STR);
			$queryStatement->bindValue(':Longitude', $this->numberFormatPrecision($blockDetails['reverse']['x']) . '%', \PDO::PARAM_STR);
			$queryStatement->execute();
			$blockData = $queryStatement->fetchAll();

			if (!empty($blockData)) {

				return $this->createSuccessResponse($blockData);

			} else {

				$errorMessage = "Blocks Not Found";
				return $this->createErrorResponse($errorMessage);
			}
		} else {
			$errorMessage = "Blocks Not Found";
			return $this->createErrorResponse($errorMessage);
		}

	}
	function numberFormatPrecision($number, $precision = 3, $separator = '.') {
		$numberParts = explode($separator, $number);
		$response = $numberParts[0];
		if (count($numberParts) > 1) {
			$response .= $separator;
			$response .= substr($numberParts[1], 0, $precision);
		}
		return $response;
	}
}