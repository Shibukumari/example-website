<?php

namespace BmmiBundle\StoreBundle\Controller\Rest;

use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractRestController {
	/**
	 * @Route("/slots", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  POST http://[YOUR-DOMAIN]/webservice/rest/bmmi/slots?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $store_id
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function slotsAction(Request $request) {
		$slotInfo = json_decode($request->getContent(), true);

		$todayDate = date("Y-m-d");
		$daysCount = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);

		$endDate = strtotime($todayDate . ' + ' . $daysCount->getSlotDayCount() . ' days');

		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$slotLists = new DataObject\BMMISlotLists\Listing();
		if ($slotInfo['mode'] == 'Express Mode') {
			$slotLists->addConditionParam("Date = ?", strtotime($todayDate), "AND");
		}else{
			$slotLists->addConditionParam("Date >= ?", strtotime($todayDate), "AND");
			$slotLists->addConditionParam("Date <= ?", $endDate, "AND");
		}
		
		$slotLists->addConditionParam("DeliveryMode = ?", $slotInfo['mode'], "AND");
		if ($slotInfo['mode'] != 'Pickup') {
			$slotLists->addConditionParam("ZoneGroup = ?", $slotInfo['zone_group'], "AND");
		}
		$slotLists->addConditionParam("Enable = ?", 1);
		$slotLists->setCondition("StoreId = ?", $slotInfo['storeid']);
		$slotLists->setUnpublished(false);
		$slotLists->load();
		if (null != $slotLists) {
			$currentTime = date('H:i', strtotime("now"));
			$slotData = array();
			foreach ($slotLists as $timingSlot) {

				$loopDate = date('Y-m-d', strtotime($timingSlot->getDate()->date));
				if ($todayDate == $loopDate) {

					if (date('H:i', strtotime('+' . $timingSlot->getSlotPriorTime() . ' minutes', strtotime($currentTime))) <= $timingSlot->getRealFrom()) {

						$slotData['from'] = $timingSlot->getRealFrom();
						$slotData['to'] = $timingSlot->getRealTo();
						$slotData['ordercount'] = $timingSlot->getMaxOrderCount();
						$slotData['charges'] = $timingSlot->getCharges();
						$slotData['slotid'] = $timingSlot->getSlotId();
						$slotData['zonegroup'] = $timingSlot->getZoneGroup();
						$slotData['slot_prior_time'] = $timingSlot->getSlotPriorTime();
						$slotData['slot_picking_time'] = $timingSlot->getSlotPickingTime();
						$slotData['deliverymode'] = $timingSlot->getDeliveryMode();
						$slotData['min_cart_price'] = $timingSlot->getMinCartPrice();
						$slotData['date'] = $loopDate;
						$allSlotData[] = $slotData;
					}
				} else {
					$slotData['from'] = $timingSlot->getRealFrom();
					$slotData['to'] = $timingSlot->getRealTo();
					$slotData['ordercount'] = $timingSlot->getMaxOrderCount();
					$slotData['charges'] = $timingSlot->getCharges();
					$slotData['slotid'] = $timingSlot->getSlotId();
					$slotData['zonegroup'] = $timingSlot->getZoneGroup();
					$slotData['slot_prior_time'] = $timingSlot->getSlotPriorTime();
					$slotData['slot_picking_time'] = $timingSlot->getSlotPickingTime();
					$slotData['deliverymode'] = $timingSlot->getDeliveryMode();
					$slotData['min_cart_price'] = $timingSlot->getMinCartPrice();
					$slotData['date'] = $loopDate;
					$allSlotData[] = $slotData;
				}

			}

			$slotDetails['day'] = $allSlotData;

			if (null != $allSlotData) {
				$promotionData = new DataObject\DeliveryPromotions\Listing();
				$promotionData->addConditionParam("Websites LIKE ?", '%,'. $slotInfo['website'] . ',%', "AND");
				$promotionData->setCondition("DeliveryType = ?", $slotInfo['mode']);
				$allpromotionData = $promotionData->load();
				
				if (null != $allpromotionData) {
					foreach ($allpromotionData as $promotionValue) {
						$data['fromHours'] = $promotionValue->getFromHours();
						$data['toHours'] = $promotionValue->getToHours();
						$data['cartPrice'] = $promotionValue->getPrice();
						$data['discount'] = $promotionValue->getDiscount();
						$response[] = $data;
					}
					$slotDetails['promotion'] = $response;
				}else{
				 //$slotDetails['promotion'] = '';
				}
					return $this->createSuccessResponse($slotDetails);
				} else {
					$slotDetails = "No slots available";
					return $this->createErrorResponse($slotDetails);
				}

		} else {
			$slotDetails = 'Invalid Store ID';
			return $this->createErrorResponse($slotDetails);
		}

	}

	/**
	 * @Route("/nextslots", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/bmmi/nextslots?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function nextSlotsAction(Request $request) {

		$slotInfo = json_decode($request->getContent(), true);
		$todayDate = date("Y-m-d");
		$daysCount = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
		$endDate = strtotime($todayDate . ' + ' . $daysCount->getSlotDayCount() . ' days');
		$slotInfo['mode'] = "Express Mode";

		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$slotLists = new DataObject\BMMISlotLists\Listing();
		$slotLists->addConditionParam("Date >= ?", strtotime($todayDate), "AND");
		$slotLists->addConditionParam("Date <= ?", $endDate, "AND");
		$slotLists->addConditionParam("Enable = ?", 1, "AND");
		$slotLists->addConditionParam("StoreId = ?", $slotInfo['storeid'], "AND");
		$slotLists->addConditionParam("ZoneGroup = ?", $slotInfo['zone_group'], "AND");
		$slotLists->setCondition("DeliveryMode IN (?)", [["Express Mode", "Standard Mode"]]);
		$slotLists->setUnpublished(false);

		$slotLists->load();

		if (null != $slotLists) {
			$currentTime = date('H:i', strtotime("now"));

			foreach ($slotLists as $timingSlot) {

				$mode = $timingSlot->getDeliveryMode();

				$loopDate = date('Y-m-d', strtotime($timingSlot->getDate()->date));

				if (date('Y-m-d') == $loopDate) {

					if (date('H:i', strtotime('+' . $timingSlot->getSlotPriorTime() . ' minutes', strtotime($currentTime))) <= $timingSlot->getRealFrom()) {

						$slotData['from'] = $timingSlot->getRealFrom();
						$slotData['to'] = $timingSlot->getRealTo();
						$slotData['ordercount'] = $timingSlot->getMaxOrderCount();
						$slotData['charges'] = $timingSlot->getCharges();
						$slotData['slotid'] = $timingSlot->getSlotId();
						$slotData['date'] = $loopDate;
						$slotData['min_cart_price'] = $timingSlot->getMinCartPrice();
						$allSlotData[$mode][] = $slotData;

					}
				} else {
					$slotData['from'] = $timingSlot->getRealFrom();
					$slotData['to'] = $timingSlot->getRealTo();
					$slotData['ordercount'] = $timingSlot->getMaxOrderCount();
					$slotData['charges'] = $timingSlot->getCharges();
					$slotData['slotid'] = $timingSlot->getSlotId();
					$slotData['date'] = $loopDate;
					$slotData['min_cart_price'] = $timingSlot->getMinCartPrice();
					$allSlotData[$mode][] = $slotData;
				}

			}

			$slotDetails['data'] = $allSlotData;

			$slotDetails['message'] = "Success";
		} else {
			$slotDetails['data'] = 0;
			$slotDetails['message'] = 'Invalid Store ID';
		}

		return new JsonResponse($slotDetails);

	}

	/**
	 * @Route("/slotsdetails", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/rest/bmmi/slotsdetails?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function slotsDetailsAction(Request $request) {

		$slotIds = json_decode($request->getContent(), true);

		if (is_array($slotIds) && !empty($slotIds)) {
			$slotObj = new DataObject\BMMISlotLists\Listing();
			$slotObj->setCondition("SlotId IN (?)", [$slotIds['slotids']]);
			$allSlotDetails = $slotObj->load();
			if (null != $allSlotDetails) {
				$slotData = array();
				foreach ($allSlotDetails as $allSlotDetail) {
					$slotData['from'] = $allSlotDetail->getRealFrom();
					$slotData['to'] = $allSlotDetail->getRealTo();
					$slotData['charges'] = $allSlotDetail->getCharges();
					$slotData['pickingtime'] = $allSlotDetail->getSlotPickingTime();
					$slotData['priortime'] = $allSlotDetail->getSlotPriorTime();
					$slotData['zonegroup'] = $allSlotDetail->getZoneGroup();
					$slotData['deliverymode'] = $allSlotDetail->getDeliveryMode();
					$slotData['storeid'] = $allSlotDetail->getStoreId();
					$slotData['min_cart_price'] = $allSlotDetail->getMinCartPrice();
					$slotData['date'] = date('Y-m-d', strtotime($allSlotDetail->getDate()->date));
					$allSlotData['data'][$allSlotDetail->getSlotId()] = $slotData;
				}

				$allSlotData['success'] = true;
			} else {
				$allSlotData['success'] = false;
				$allSlotData['message'] = "Invalid SlotId";
			}

		} else {
			$allSlotData['success'] = false;
			$allSlotData['message'] = "Not an array format";
		}
		return new JsonResponse($allSlotData);

	}

	/**
	 * @Route("/dayslot", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/rest/bmmi/dayslot?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function daySlotAction(Request $request) {

		$dateDetails = json_decode($request->getContent(), true);

		if (isset($dateDetails['date'])) {
			$slotTimeStamp = strtotime($dateDetails['date']);
		} else {
			$slotTimeStamp = strtotime('today');
		}
		$slotObj = new DataObject\BMMISlotLists\Listing();
		$slotObj->addConditionParam("StoreId = ?", $dateDetails['storeid'], "AND");
		$slotObj->setCondition("Date = ?", $slotTimeStamp);

		$allSlotDetails = $slotObj->load();
		if (null != $allSlotDetails) {
			foreach ($allSlotDetails as $timingSlot) {

				$slotData['from'] = $timingSlot->getRealFrom();
				$slotData['to'] = $timingSlot->getRealTo();
				$slotData['slotid'] = $timingSlot->getSlotId();
				$slotData['ordercount'] = $timingSlot->getMaxOrderCount();
				$slotData['charges'] = $timingSlot->getCharges();
				$slotData['zonegroup'] = $timingSlot->getZoneGroup();
				$slotData['slot_prior_time'] = $timingSlot->getSlotPriorTime();
				$slotData['slot_picking_time'] = $timingSlot->getSlotPickingTime();
				$slotData['deliverymode'] = $timingSlot->getDeliveryMode();
				$slotData['min_cart_price'] = $timingSlot->getMinCartPrice();
				$slotData['date'] = date("Y-m-d", $slotTimeStamp);

				$allSlotData[] = $slotData;
			}

			return $this->createSuccessResponse($allSlotData);
		} else {
			$message = "No slots available";
			return $this->createErrorResponse($message);
		}

	}

}
