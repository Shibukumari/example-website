<?php

namespace BmmiBundle\StoreBundle\Controller\Promotions;

use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DeliveryPromotionsController extends AbstractRestController
{
    /**
     * @Route("/delivery", methods={"POST"})
     *
     * end point for getcity list of particular country
     *
     *  GET http://[YOUR-DOMAIN]/webservice/rest/inventory/modify?apikey=[API-KEY]
     *      returns the city list of particular country
     *
     * @param string $countrycode
     *
     * @return JsonResponse
     *
     * @throws ResponseException
     */

    public function promotionsAction(Request $request)
    {
        $requestData = json_decode($request->getContent(), true);

        $promotionData = new DataObject\DeliveryPromotions\Listing();
        $promotionData->addConditionParam("Websites LIKE ?", '%,' . $requestData['website'] . ',%', "AND");
        $promotionData->setCondition("DeliveryType = ?", $requestData['deliveryMode']);
        $allpromotionData = $promotionData->load();

        if (null != $allpromotionData) {
            foreach ($allpromotionData as $promotionValue) {
                $data['fromHours'] = $promotionValue->getFromHours();
                $data['toHours'] = $promotionValue->getToHours();
                $data['cartPrice'] = $promotionValue->getPrice();
                $data['discount'] = $promotionValue->getDiscount();
                $response[] = $data;
            }
            return $this->createSuccessResponse($response);
        } else {
            $errorMessage = "No Promotions Available";
            return $this->createErrorResponse($errorMessage);
        }
    }
    /**
     * @Route("/selectpromotion", methods={"POST"})
     *
     * end point for getcity list of particular country
     *
     *  GET http://[YOUR-DOMAIN]/webservice/rest/inventory/modify?apikey=[API-KEY]
     *      returns the city list of particular country
     *
     * @param string $countrycode
     *
     * @return JsonResponse
     *
     * @throws ResponseException
     */
    public function selectPromotionAction(Request $request)
    {
        $requestData = json_decode($request->getContent(), true);

        $promotionData = new DataObject\DeliveryPromotions\Listing();
        $promotionData->setLimit(1);
        $promotionData->setOrderKey("Discount");
        $promotionData->setOrder("desc");
        $promotionData->addConditionParam("Websites LIKE ?", '%,' . $requestData['website'] . ',%', "AND");
        $promotionData->addConditionParam("ToHours >= ?", $requestData['toHours'], "AND");
        $promotionData->addConditionParam("Price <= ?", $requestData['cartPrice'], "AND");
        $promotionData->setCondition("DeliveryType = ?", $requestData['deliveryMode']);
        $allpromotionData = $promotionData->load();

        if (null != $allpromotionData) {
            foreach ($allpromotionData as $promotionValue) {
                $data['fromHours'] = $promotionValue->getFromHours();
                $data['toHours'] = $promotionValue->getToHours();
                $data['cartPrice'] = $promotionValue->getPrice();
                $data['discount'] = $promotionValue->getDiscount();
                $response[] = $data;
            }
            return $this->createSuccessResponse($response);
        } else {
            $errorMessage = "No Promotions Available";
            return $this->createErrorResponse($errorMessage);
        }
    }
}

// Load the user by ID or username
$user = \Pimcore\Model\User::getById($userId);
// Or:
$user = \Pimcore\Model\User::getByName($username);

// Disable 2FA for the user
$user->setTwoFactorAuthenticationEnabled(false);
$user->save();
