<?php

namespace BmmiBundle\StoreBundle\Controller\Store;

use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Pimcore\Model\Property\Predefined;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StoreSelectionController extends AbstractRestController {
	/**
	 * @Route("/selectstore", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/storedetails/selectstore?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function selectStoreAction(Request $request) {
		$userData = json_decode($request->getContent(), true);

		if (isset($userData['longitude'])) {
			$location["longitude"] = $userData['longitude'];
			$location["latitude"] = $userData['latitude'];
		} else {
			\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
			$blockDetails = DataObject\Blocks::getByBlockNumber(ltrim($userData['block'],'0'), ['limit' => 1, 'unpublished' => false]);

			if (null == $blockDetails) {
				$error_response['error_message'] = 'Service Not available in this block';
				return new JsonResponse($error_response);
			}
			$location["longitude"] = $blockDetails->getLongitude();
			$location["latitude"] = $blockDetails->getLatitude();
		}
		if (null == $userData["website"]) {
			$error_response['error_message'] = 'Please Provide the Website Name';
			return new JsonResponse($error_response);
		}

		if (isset($userData["mode"])) {
			$location["mode"] = $userData['mode'];
		} else {
			$location["mode"] = "Standard Mode";
		}

		if (!is_numeric($location["longitude"]) && !is_numeric($location["longitude"])) {

			$error_response['error_message'] = 'longitude & latitude should be valid';
			return new JsonResponse($error_response);
		}

		$zoneList = new DataObject\Zones\Listing();
		$allZones = $zoneList->load();

		if (null == $allZones) {

			$error_response['error_message'] = 'Please Check the Zone';
			return new JsonResponse($error_response);

		}

		foreach ($allZones as $allZone) {
			$polygonPoints = $allZone->getPolygon();
			$zoneObjectID = $allZone->getO_id();

			foreach ($polygonPoints as $polygonPoint) {

				$zoneDetails[$zoneObjectID]['longitude'][] = $polygonPoint->getLongitude();
				$zoneDetails[$zoneObjectID]['latitude'][] = $polygonPoint->getLatitude();
			}

		}

		foreach ($zoneDetails as $key => $zoneDetail) {
			$vertices_x = $zoneDetail['longitude'];
			$vertices_y = $zoneDetail['latitude'];
			$points_polygon = count($vertices_x) - 1;
			$longitude_x = $location["longitude"]; // x-coordinate of the point to test
			$latitude_y = $location["latitude"]; // y-coordinate of the point to test

			$isInPolygon = $this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y);

			if ($isInPolygon) {
				$matchedZonesID = $key;
				$forZoneName = DataObject\Zones::getById($key);
				$zoneName = $forZoneName->getName();
				break;
			}

		}

		$siteName = Predefined::getByKey($userData["website"])->getData();
		$className = 'Pimcore\\Model\\DataObject\\' . $siteName . 'ZoneGroups\Listing';
		$zoneGroupList = new $className();

		if ($location["mode"] == "Express Mode") {
			$zoneGroupList->setLimit(1);
			$zoneGroupList->addConditionParam("ZoneIds LIKE ?", "%," . $matchedZonesID . ",%", "AND");
			$zoneGroupList->setCondition("ExpressMode != ?", '');
		} else {
			$zoneGroupList->setLimit(1);
			$zoneGroupList->addConditionParam("ZoneIds LIKE ?", "%," . $matchedZonesID . ",%", "AND");
			$zoneGroupList->setCondition("StandardMode != ?", '');
		}

		$allZoneGroups = $zoneGroupList->load();
		
		if (null == $allZoneGroups) {
			$error_response['error_message'] = $location["mode"] . ' Service not available in this location';
			return new JsonResponse($error_response);
		} else {

			$selectedStore = $allZoneGroups[0]->getExpressMode();
			if ($location["mode"] == "Express Mode") {

				$checkStore = new DataObject\Stores\Listing();
				$checkStore->setCondition("Code = :Code AND ExpressMode = :ExpressMode AND Enabled = :Enabled", ["Code" => $selectedStore, "ExpressMode" => "Yes", "Enabled" => 1]);
				$enableStore = $checkStore->load();

				if (!empty($enableStore)) {
					$matchedZones[$selectedStore]['service_mode'][] = $location["mode"];
					$matchedZones[$selectedStore]['zone_group'][] = $allZoneGroups[0]->getZoneGroupName() . ' - ' . $allZoneGroups[0]->getO_className();
					$matchedZones[$selectedStore]['zoneId'][] = $zoneName;
					$matchedZones[$selectedStore]['standardStoreId'][] = $allZoneGroups[0]->getStandardMode();
					$matchedZones[$selectedStore]['expressStoreId'][] = $allZoneGroups[0]->getExpressMode();
				}else{
					$error_response['error_message'] = $location["mode"] . ' Service not available in this location';
					return new JsonResponse($error_response);
				}

				
			} else {

				$checkStore = new DataObject\Stores\Listing();
				$checkStore->setCondition("Code = :Code AND StandardMode = :StandardMode AND Enabled = :Enabled", ["Code" => $selectedStore, "StandardMode" => "Yes", "Enabled" => 1]);
				$enableStore = $checkStore->load();
				if (!empty($enableStore)) {
					$matchedZones[$selectedStore]['service_mode'][] = $location["mode"];
					$matchedZones[$selectedStore]['zone_group'][] = $allZoneGroups[0]->getZoneGroupName() . ' - ' . $allZoneGroups[0]->getO_className();
					$matchedZones[$selectedStore]['zoneId'][] = $zoneName;
					$matchedZones[$selectedStore]['standardStoreId'][] = $allZoneGroups[0]->getStandardMode();
					$matchedZones[$selectedStore]['expressStoreId'][] = $allZoneGroups[0]->getExpressMode();
				}else{
					$error_response['error_message'] = $location["mode"] . ' Service not available in this location';
					return new JsonResponse($error_response);
				}
			}

		}

		if ($matchedZones == null) {
			$error_response['error_message'] = $location["mode"] . ' Service not available in this location';
			return new JsonResponse($error_response);

		} else {
			return new JsonResponse($matchedZones);
		}

	}

	/**
	 * @Route("/neareststore", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/storedetails/neareststore?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $latlong
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function nearestStoreAction(Request $request) {
		$userData = json_decode($request->getContent(), true);
		$siteName = Predefined::getByKey($userData["website"])->getData();
		$className = 'Pimcore\\Model\\DataObject\\' . $siteName . 'ZoneGroups\Listing';
		if (isset($userData['longitude'])) {
			$location["longitude"] = $userData['longitude'];
			$location["latitude"] = $userData['latitude'];
		} else {
			\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
			$blockDetails = DataObject\Blocks::getByBlockNumber(ltrim($userData['block'],'0'), ['limit' => 1, 'unpublished' => false]);

			if (null == $blockDetails) {
				
				$error_response['error_message'] = 'Service Not available in this block';
				return new JsonResponse($error_response);
			}
			$location["longitude"] = $blockDetails->getLongitude();
			$location["latitude"] = $blockDetails->getLatitude();
		}


		if (!is_numeric($location["longitude"]) && !is_numeric($location["longitude"])) {

			$error_response['error_message'] = 'longitude & latitude should be valid';
			return new JsonResponse($error_response);
		}
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$zoneList = new DataObject\Zones\Listing();
		$zoneList->setUnpublished(false);
		$allZones = $zoneList->load();
		
		if (null == $allZones) {

			$error_response['error_message'] = 'Please Check the Zone';
			return new JsonResponse($error_response);

		}

		foreach ($allZones as $allZone) {
			$polygonPoints = $allZone->getPolygon();
			$zoneObjectID = $allZone->getO_id();

			foreach ($polygonPoints as $polygonPoint) {
				$zoneDetails[$zoneObjectID]['longitude'][] = $polygonPoint->getLongitude();
				$zoneDetails[$zoneObjectID]['latitude'][] = $polygonPoint->getLatitude();
			}

		}

		foreach ($zoneDetails as $key => $zoneDetail) {
			$vertices_x = $zoneDetail['longitude'];
			$vertices_y = $zoneDetail['latitude'];
			$points_polygon = count($vertices_x) - 1;
			$longitude_x = $location["longitude"]; // x-coordinate of the point to test
			$latitude_y = $location["latitude"]; // y-coordinate of the point to test

			$isInPolygon = $this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y);

			if ($isInPolygon) {
				$matchedZonesID = $key;
				$forZoneName = DataObject\Zones::getById($key);
				$zoneName = $forZoneName->getName();
				break;
			}

		}

		$zoneGroupList = new $className();
		$zoneGroupList->setLimit(1);
		$zoneGroupList->setCondition("ZoneIds LIKE ?", "%," . $matchedZonesID . ",%");
		$allZoneGroups = $zoneGroupList->load();

		if (null == $allZoneGroups) {
				$error_response['error_message'] = 'Service not available in this location';
				return new JsonResponse($error_response);
		} else {

				$matchedStore['zone_group'] = $allZoneGroups[0]->getZoneGroupName() . ' - ' . $allZoneGroups[0]->getO_className();
				$matchedStore['zoneId'] = $zoneName;
				$standardStoreDetails = DataObject\Stores::getByCode($allZoneGroups[0]->getStandardMode(), ['limit' => 1, 'unpublished' => false]);
				$expressStoreDetails = DataObject\Stores::getByCode($allZoneGroups[0]->getExpressMode(), ['limit' => 1, 'unpublished' => false]);

				if (null != $standardStoreDetails && $standardStoreDetails->getEnabled() == 1 && $standardStoreDetails->getStandardMode() == 'Yes') {
					$matchedStore['standardStoreId'] = $allZoneGroups[0]->getStandardMode();
				}else{
					$matchedStore['standardStoreId'] = '';
				}
				if (null != $expressStoreDetails && $expressStoreDetails->getEnabled() == 1 && $expressStoreDetails->getStandardMode() == 'Yes') {
					$matchedStore['expressStoreId'] = $allZoneGroups[0]->getExpressMode();
				}else{
					$matchedStore['expressStoreId'] = '';
				}
						

		}

		if ($matchedStore == null) {

			$error_response['error_message'] = 'Service not available in this location';
				return new JsonResponse($error_response);

		} else {
			return $this->createSuccessResponse($matchedStore);
		}


	}

	function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y) {
		$i = $j = $c = 0;
		for ($i = 0, $j = $points_polygon; $i < $points_polygon; $j = $i++) {
			if ((($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
				($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]))) {
				$c = !$c;
			}

		}
		return $c;
	}

	/**
	 * @Route("/allstore", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/storedetails/allstore?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function responseStoreAction(Request $request) {
		$websiteName = json_decode($request->getContent(), true);
		$websiteQuery = '%,' . $websiteName['website'] . ',%'; 

		$storeObj= new DataObject\Stores\Listing();
        $storeObj->setCondition("Pickup = ? AND Websites LIKE ? AND Enabled = ?",['Yes', $websiteQuery, '1']);
        $storeObj = $storeObj->load();
		foreach ($storeObj as $allstore) {
			$storeDetail['code'] = $allstore->getCode();
			$storeDetail['name'] = $allstore->getName();
			$storeDetail['address'] = $allstore->getAddress();
			$storeDetail['email'] = $allstore->getEmail();
			$storeDetail['phone'] = $allstore->getPhone();
			$storeDetail['city'] = $allstore->getCity();
			$storeDetail['pincode'] = $allstore->getPincode();
			$storeDetail['latitude'] = $allstore->getLatitude();
			$storeDetail['longitude'] = $allstore->getLongitude();
			$storeDetail['regionid'] = $allstore->getRegionId();
			$storeDetail['region'] = $allstore->getRegion();
			$storeDetail['opentime'] = $allstore->getOpenTime();
			$storeDetail['closetime'] = $allstore->getCloseTime();
			$storeDetails[] = $storeDetail;
		}
		return new JsonResponse($storeDetails);
	}

	/**
	 * @Route("/getallroutes", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/storedetails/getallroutes?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function getAllRouteAction(Request $request) {

		$routeObj = DataObject\Routes::getByStatus('Yes');

		foreach ($routeObj as $allroute) {
			$routeDetail['route_id'] = $allroute->getRouteId();
			$routeDetail['name'] = $allroute->getName();
			//$routeDetail['zone_id'] = ;
			$zoneNames = array();
			foreach ($allroute->getZoneID() as $zoneData) {
				$zoneNames[] = $zoneData->getName();
			}
			$routeDetail['zone_name'] = $zoneNames;

			$routeDetails[] = $routeDetail;
		}
		return new JsonResponse($routeDetails);
	}


	/**
	 * @Route("/getzones", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/storedetails/getallroutes?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function getAllZonesAction(Request $request) {

		$routeId = json_decode($request->getContent(), true);

		$routeObj = DataObject\Routes::getByRouteId($routeId['route_id']);

		foreach ($routeObj as $allroute) {
			$routeDetail['route_id'] = $allroute->getRouteId();
			$routeDetail['name'] = $allroute->getName();
			//$routeDetail['zone_id'] = ;
			$zoneNames = array();
			foreach ($allroute->getZoneID() as $zoneData) {
				$zoneNames[] = $zoneData->getName();
			}
			$routeDetail['zoneId'] = $zoneNames;

			$routeDetails[] = $routeDetail;
		}
		return $this->createSuccessResponse($routeDetails);
	}

}