<?php

namespace BmmiBundle\StoreBundle\Controller\Catalog;

use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Pimcore\Model\Property\Predefined;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends AbstractRestController {
	/**
	 * @Route("/categories", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/rest/catalog/categories?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function categoriesAction(Request $request) {
		
		$categoryID = json_decode($request->getContent(), true);

		if (null != $categoryID['parentId']) {

			$categoryData = new DataObject\Category\Listing();
			if ($categoryID['parentId'] != 0) {
				$categoryData->setCondition("o_parentId = ?", $categoryID['parentId']);
			}else{
				$findParentId = DataObject\Category::getById($categoryID['id'], ['limit' => 1, 'unpublished' => false]);
				$categoryID['parentId'] = $findParentId->getO_parentId();
				$categoryData->setCondition("o_parentId = ?", $categoryID['parentId']);
			}
			
			$allCategoryData = $categoryData->load();
			if (null != $allCategoryData) {
				foreach ($allCategoryData as $eachCategoryData) {
					$categoryDetail['name'] = $eachCategoryData->getName();
					$categoryDetail['id'] = $eachCategoryData->getO_id();
					$categoryDetail['index'] = $eachCategoryData->getO_index();

					$categoryDetails[] = $categoryDetail;
				}
			}else{

				$message['msg'] = 'No categories for this ID';
				return $this->createErrorResponse($message);
			}

		}else{
			$message['msg'] = 'parentId is missing';
			return $this->createErrorResponse($message);

		}


		return $this->createSuccessResponse($categoryDetails);
			
	}


}