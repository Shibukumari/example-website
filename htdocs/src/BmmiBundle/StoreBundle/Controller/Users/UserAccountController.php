<?php
namespace BmmiBundle\StoreBundle\Controller\Users;

use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserAccountController extends AbstractRestController {
	/**
	 * @Route("/pickeraccount", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/users/pickeraccount?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function pickerAccountAction(Request $request) {
		$userDetails = json_decode($request->getContent(), true);
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$username = $userDetails['username'];
		if ($userDetails['type'] == "login") {
			$password = $userDetails['password'];
			$storeId = $userDetails['store_id'];

			if (null != $username && null != $password) {

				$pickerAccount = DataObject\BMMIPickers::getByUserName($username, ['limit' => 1, 'unpublished' => false]);

				if (null != $pickerAccount) {
					$usernameDB = $pickerAccount->getUserName();
					$passwordDB = $pickerAccount->getPassword();
					if ($passwordDB == md5($password . 'bmmisalt')) {
						$pickerAccount->setIsLoggedIn(1);
						$pickerAccount->save();
						$response = $this->loginChange($pickerAccount, $storeId);
						$response['data']['username'] = $pickerAccount->getUserName();
						$response['data']['picker_id'] = $pickerAccount->getPickerId();
						$response['data']['store_code'] = $storeId;
						$response['data']['name'] = $pickerAccount->getName();
						$response['data']['address'] = $pickerAccount->getAddress();
						$response['data']['email'] = $pickerAccount->getemail();
						$response['data']['phone'] = $pickerAccount->getPhoneNumber();
						$response['data']['IsLoggedIn'] = $pickerAccount->getIsLoggedIn();

					} else {
						$response = $this->invalidPass();
					}

				} else {
					$response = $this->invalidUser();
				}
			} else {
				$response = $this->invalidUserPass();

			}
			return new JsonResponse($response);
		} else {
			if (null != $username) {
				$pickerUser = DataObject\BMMIPickers::getByUserName($username, ['limit' => 1, 'unpublished' => false]);
				if (null != $pickerUser) {
					$response = $this->logoutMsg($pickerUser);

				} else {
					$response = $this->invalidUser();
				}

			} else {
				$response = $this->invalidUser();

			}
			return new JsonResponse($response);

		}

	}

	/**
	 * @Route("/manageraccount", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/users/manageraccount?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function managerAccountAction(Request $request) {
		$userDetails = json_decode($request->getContent(), true);
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$username = $userDetails['username'];

		if ($userDetails['type'] == "login") {
			$password = $userDetails['password'];
			$storeId = $userDetails['store_id'];
			if (null != $username && null != $password) {

				$managerAccount = DataObject\BMMIHubManagers::getByUserName($username, ['limit' => 1, 'unpublished' => false]);

				if (null != $managerAccount) {
					$usernameDB = $managerAccount->getUserName();

					$passwordDB = $managerAccount->getPassword();
					if ($passwordDB == md5($password . 'bmmisalt')) {
						$response['message'] = 'Loggedin successfully';
						$response['code'] = 1;
						$response['data']['username'] = $managerAccount->getUserName();
						$response['data']['hub_manager_id'] = $managerAccount->getHubManagerId();
						$response['data']['name'] = $managerAccount->getName();
						$response['data']['store_code'] = $storeId;
						$response['data']['address'] = $managerAccount->getAddress();
						$response['data']['email'] = $managerAccount->getemail();
						$response['data']['phone'] = $managerAccount->getPhoneNumber();

					} else {

						$response = $this->invalidPass();
					}

				} else {
					$response = $this->invalidUser();
				}
			} else {
				$response = $this->invalidUserPass();
			}
			return new JsonResponse($response);
		} else {

			if (null != $username) {

				$managerAccount = DataObject\BMMIHubManagers::getByUserName($username, ['limit' => 1, 'unpublished' => false]);

				if (null != $managerAccount) {

					//$managerAccount->setStoreId([]);
					$managerAccount->save();
					$response['code'] = 1;
					$response['message'] = 'Loggedout successfully';

				} else {
					$response = $this->invalidUser();
				}

			} else {
				$response = $this->invalidUser();

			}
			return new JsonResponse($response);

		}

	}

	/**
	 * @Route("/rideraccount", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/users/rideraccount?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function riderAccountAction(Request $request) {
		$userDetails = json_decode($request->getContent(), true);
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$username = $userDetails['username'];
		if ($userDetails['type'] == "login") {
			$password = $userDetails['password'];
			$storeId = $userDetails['store_id'];
			if (null != $username && null != $password) {

				$riderAccount = DataObject\BMMIRiders::getByUserName($username, ['limit' => 1, 'unpublished' => false]);

				if (null != $riderAccount) {
					$usernameDB = $riderAccount->getUserName();
					$passwordDB = $riderAccount->getPassword();
					if ($passwordDB == md5($password . 'bmmisalt')) {
						$riderAccount->setIsLoggedIn(1);
						$riderAccount->save();

						$response = $this->loginChange($riderAccount, $storeId);
						$response['data']['username'] = $riderAccount->getUserName();
						$response['data']['rider_id'] = $riderAccount->getRiderId();
						$response['data']['store_code'] = $storeId;
						$response['data']['name'] = $riderAccount->getName();
						$response['data']['address'] = $riderAccount->getAddress();
						$response['data']['email'] = $riderAccount->getemail();
						$response['data']['phone'] = $riderAccount->getPhoneNumber();
						$response['data']['IsLoggedIn'] = $riderAccount->getIsLoggedIn();

					} else {
						$response = $this->invalidPass();
					}

				} else {
					$response = $this->invalidUser();
				}
			} else {
				$response = $this->invalidUserPass();

			}
			return new JsonResponse($response);
		} else {
			if (null != $username) {
				$riderAccount = DataObject\BMMIRiders::getByUserName($username, ['limit' => 1, 'unpublished' => false]);
				if (null != $riderAccount) {

					$response = $this->logoutMsg($riderAccount);

				} else {
					$response = $this->invalidUser();
				}

			} else {

				$response = $this->invalidUser();
			}
			return new JsonResponse($response);

		}

	}

	/**
	 * @Route("/pickerlist", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/rest/users/pickerlist?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function listPickersAction(Request $request) {

		$storeDetails = json_decode($request->getContent(), true);
		if ($storeDetails['storeid'] != '') {
			\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
			$storeObj = DataObject\Stores::getByCode($storeDetails['storeid'], ['limit' => 1, 'unpublished' => false]);

			if (null != $storeObj) {
				$storeObjId = $storeObj->getO_id();
				$pickerList = new DataObject\BMMIPickers\Listing();
				$pickerList->addConditionParam("StoreId like '%," . $storeObjId . ",%'", "AND");
				$pickerList->setCondition("IsLoggedIn = ?", "1");

				$allPickers = $pickerList->load();
				foreach ($allPickers as $allPicker) {
					$picker['username'] = $allPicker->getUserName();
					$picker['picker_id'] = $allPicker->getPickerId();
					$picker['name'] = $allPicker->getName();
					$picker['address'] = $allPicker->getAddress();
					$picker['email'] = $allPicker->getemail();
					$picker['phone'] = $allPicker->getPhoneNumber();
					$picker['IsLoggedIn'] = $allPicker->getIsLoggedIn();
					$pickerDetails[] = $picker;
				}

				return $this->createSuccessResponse($pickerDetails);
			} else {
				$response = 'Invalid Store ID';
				return $this->createErrorResponse($response);
			}

		} else {
			$response = 'Invalid Store ID';
			return $this->createErrorResponse($response);
		}
	}

	/**
	 * @Route("/riderlist", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/rest/users/riderlist?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function listRidersAction(Request $request) {

		$storeDetails = json_decode($request->getContent(), true);
		if ($storeDetails['storeid'] != '') {
			\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
			$storeObj = DataObject\Stores::getByCode($storeDetails['storeid'], ['limit' => 1, 'unpublished' => false]);
			$storeObjId = $storeObj->getO_id();

			if (null != $storeObjId) {
				$riderList = new DataObject\BMMIRiders\Listing();
				$riderList->addConditionParam("StoreId like '%," . $storeObjId . ",%'", "AND");
				$riderList->setCondition("IsLoggedIn = ?", "1");

				$allRiders = $riderList->load();

				foreach ($allRiders as $allRider) {
					$rider['username'] = $allRider->getUserName();
					$rider['rider_id'] = $allRider->getRiderId();
					$rider['name'] = $allRider->getName();
					$rider['address'] = $allRider->getAddress();
					$rider['email'] = $allRider->getemail();
					$rider['phone'] = $allRider->getPhoneNumber();
					if (null != $allRider->getVehicleId()) {
						$rider['vehicleid'] = $allRider->getVehicleId()[0]->getVehicleId();

					} else {
						$rider['vehicleid'] = '';
					}

					$rider['IsLoggedIn'] = $allRider->getIsLoggedIn();
					$riderDetails[] = $rider;

				}

				$response['data'] = $riderDetails;
				$response['message'] = 'Success';
				return new JsonResponse($response);
			} else {
				$response['data'] = null;
				$response['message'] = 'Invalid Store ID';
				return new JsonResponse($response);
			}

		} else {
			$response['data'] = null;
			$response['message'] = 'Invalid Store ID';
			return new JsonResponse($response);
		}
	}

	/**
	 * @Route("/vehiclelist", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/rest/users/riderlist?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function vehicleListAction(Request $request) {

		$storeDetails = json_decode($request->getContent(), true);
		if ($storeDetails['storeid'] != '') {
			\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
			$storeObj = DataObject\Stores::getByCode($storeDetails['storeid'], ['limit' => 1, 'unpublished' => false]);
			$storeObjId = $storeObj->getO_id();

			if (null != $storeObjId) {
				$vehicleList = new DataObject\BMMIVehicles\Listing();
				$vehicleList->setCondition("StoreId like '%," . $storeObjId . ",%'");

				$allVehicles = $vehicleList->load();

				foreach ($allVehicles as $allVehicle) {

					$vehicle['vehiclename'] = $allVehicle->getVehicleName();
					$vehicle['vehicletype'] = $allVehicle->getVehicleType();
					$vehicle['vehicleid'] = $allVehicle->getVehicleId();
					$vehicle['distancehrs'] = $allVehicle->getDistanceHrs();
					$vehicle['minweight'] = $allVehicle->getMinWeight();
					$vehicle['maxweight'] = $allVehicle->getMaxWeight();

					$vehicleDetails[$allVehicle->getVehicleId()] = $vehicle;
					$vehicle = [];

				}

				$response['data'] = $vehicleDetails;
				$response['success'] = true;
				return new JsonResponse($response);
			} else {
				$response['success'] = false;
				$response['message'] = 'Invalid Store ID';
				return new JsonResponse($response);
			}

		} else {
			$response['success'] = false;
			$response['message'] = 'Invalid Store ID';
			return new JsonResponse($response);
		}
	}


		/**
	 * @Route("/storemaster", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/users/storemaster?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function storeMasterAction(Request $request) {
		$userDetails = json_decode($request->getContent(), true);
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$username = $userDetails['username'];
		if ($userDetails['type'] == "login") {
			$password = $userDetails['password'];
			$storeId = $userDetails['store_id'];
			if (null != $username && null != $password) {

				$managerAccount = DataObject\BMMIStoreMaster::getByUserName($username, ['limit' => 1, 'unpublished' => false]);

				if (null != $managerAccount) {
					$usernameDB = $managerAccount->getUserName();

					$passwordDB = $managerAccount->getPassword();
					if ($passwordDB == md5($password . 'bmmisalt')) {
						$managerAccount->setIsLoggedIn(1);
						$managerAccount->save();
						$response = $this->loginChange($managerAccount, $storeId);
						$response['data']['username'] = $managerAccount->getUserName();
						$response['data']['storemasterId'] = $managerAccount->getStoreMasterId();
						$response['data']['name'] = $managerAccount->getName();
						$response['data']['address'] = $managerAccount->getAddress();
						$response['data']['email'] = $managerAccount->getemail();
						$response['data']['phone'] = $managerAccount->getPhoneNumber();
						$response['data']['store_code'] = $storeId;

					} else {

						$response = $this->invalidPass();
					}

				} else {
					$response = $this->invalidUser();
				}
			} else {
				$response = $this->invalidUserPass();
			}
			return new JsonResponse($response);
		} else {

			if (null != $username) {

				$managerAccount = DataObject\BMMIStoreMaster::getByUserName($username, ['limit' => 1, 'unpublished' => false]);

				if (null != $managerAccount) {

					$managerAccount->setStoreId([]);
					$response = $this->logoutMsg($managerAccount);

				} else {
					$response = $this->invalidUser();
				}

			} else {
				$response = $this->invalidUser();

			}
			return new JsonResponse($response);

		}

	}


	public function loginChange($accountObj, $storeId) {
		$response['message'] = 'Loggedin successfully';
		$response['code'] = 1;
		$storeDetails = DataObject\Stores::getByCode($storeId, ['limit' => 1, 'unpublished' => false]);
		$accountObj->setStoreId([DataObject\Stores::getById($storeDetails->getO_id())]);
		$accountObj->save();

		return $response;
	}

	public function invalidUserPass() {
		$response['code'] = 0;
		$response['error_message'] = 'Invalid Username';
		return $response;
	}
	public function invalidUser() {
		$response['code'] = 0;
		$response['error_message'] = 'Invalid Username';
		return $response;
	}

	public function logoutMsg($accountObj) {
		//$accountObj->setStoreId(null);
		$accountObj->setIsLoggedIn(0);
		$accountObj->save();
		$response['code'] = 1;
		$response['message'] = 'Loggedout successfully';
		return $response;
	}
	public function invalidPass() {
		$response['code'] = 0;
		$response['error_message'] = 'Invalid Password';
		return $response;
	}
}
