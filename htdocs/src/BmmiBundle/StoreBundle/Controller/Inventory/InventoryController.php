<?php

namespace BmmiBundle\StoreBundle\Controller\Inventory;

use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Pimcore\Model\Property\Predefined;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use BmmiBundle\StoreBundle\Website\DataBaseConnector;

class InventoryController extends AbstractRestController {
	/**
	 * @Route("/modify", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/rest/inventory/modify?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */
	public function modifyAction(Request $request) {
		$orderData = json_decode($request->getContent(), true);
		$instance = DataBaseConnector::getInstance();
		$DBconnection = $instance->connectDB();
		$productsObj = new \Pimcore\Model\DataObject\Products();

		$response = array();
		foreach ($orderData['items'] as $item) {

			$baseSku = explode("_",$item['sku']);
			
			$sql = "SELECT sku, uomsize, su, erp_su_link FROM object_". $productsObj->getO_classId()." WHERE su LIKE :su";
			$queryStatement = $DBconnection->prepare($sql);
			$queryStatement->bindValue(':su', $baseSku[0] . '%', \PDO::PARAM_STR);
            
            $queryStatement->execute();
            $productData = $queryStatement->fetchAll();
            if (null == $productData) {
            	$item['sku']=$item['sku'];
				$item['qty']=0;
				$response[] = $item;
				continue;
            }
			$soldSkuDetails = array_search($item['sku'], array_column($productData, 'sku'));
			$eachUnits = $productData[$soldSkuDetails]['uomsize'] * $item['qty'];
			
			foreach ($productData as $eachProducts) {				
				$soldUnits = $eachUnits / $eachProducts['uomsize'];
				$inventoryData = new DataObject\BMMIProductPrice\Listing();
				$inventoryData->addConditionParam("StoreId = ?", $orderData['source_code'], "AND");
				$inventoryData->setCondition("Sku = ?", $eachProducts['sku']);
				$currentData = $inventoryData->load();
				if (null != $currentData) {
					foreach ($currentData as $updateData) {	

						if ($orderData['type'] == 'order_placed') {
							$currentStock = $updateData->getLocationStock() - $soldUnits;
						}else{
							$currentStock = $updateData->getLocationStock() + $soldUnits;
						}
						
						$updateData->setLocationStock($currentStock);
						$updateData->save();

					}
					$inventory['sku']=$eachProducts['sku'];
					$inventory['qty']=$currentStock;
				}else{
					$inventory['sku']=$eachProducts['sku'];
					$inventory['qty']=0;
				}
				$response[] = $inventory;
			}

			

		}

		return $this->createSuccessResponse($response);
			
	}


}