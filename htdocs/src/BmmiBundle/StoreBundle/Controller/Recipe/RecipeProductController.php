<?php

namespace BmmiBundle\StoreBundle\Controller\Recipe;

use Pimcore\Bundle\AdminBundle\Controller\Rest\AbstractRestController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Http\Exception\ResponseException;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecipeProductController extends AbstractRestController {
	/**
	 * @Route("/productids", methods={"POST"})
	 *
	 * end point for getcity list of particular country
	 *
	 *  GET http://[YOUR-DOMAIN]/webservice/recipe/productids?apikey=[API-KEY]
	 *      returns the city list of particular country
	 *
	 * @param string $countrycode
	 *
	 * @return JsonResponse
	 *
	 * @throws ResponseException
	 */

	public function productIdsAction(Request $request) {
		$recipeId = json_decode($request->getContent(), true);

		if (null !== $recipeId['sku']) {
			\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
			$recipeDetails = DataObject\Recipe::getBySku($recipeId['sku'], ['limit' => 1, 'unpublished' => false]);
			$productData = $recipeDetails->getProduct();

			foreach ($productData as $productIds) {
				$allsku[] = $productIds->getSku();
			}
			$response['sku'] = $allsku;
			$response['message'] = 'success';
		} else {
			$response['message'] = 'Recipe SKU is incorrect';
		}
		return new JsonResponse($response);

	}

}

?>