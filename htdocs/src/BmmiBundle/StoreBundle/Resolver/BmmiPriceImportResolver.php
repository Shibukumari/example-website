<?php
namespace BmmiBundle\StoreBundle\Resolver;

use Pimcore\DataObject\Import\Resolver\AbstractResolver;
use \Pimcore\Model\DataObject;

class BmmiPriceImportResolver extends AbstractResolver {
	public function resolve(\stdClass $config, int $parentId, array $rowData) {
		$idColumn = $this->getIdColumn($config);
		$cellData = $rowData[$idColumn];
		$storeid = $rowData[1];
		$list = new DataObject\BMMIProductPrice\Listing();
		$list->setCondition("o_key = " . $list->quote(strtolower($cellData . '-' . $storeid)));
		$list->setLimit(1);
		$list = $list->load();

		if ($list) {
			$object = $list[0];
			
			$object->setPublished(1);
			return $object;
		} else { 
			if (null != $cellData && null != $storeid) {
				$object = new DataObject\BMMIProductPrice();
				$object->setKey(\Pimcore\File::getValidFilename(strtolower($cellData . '-' . $storeid)));
				$object->setParentId($parentId);
				$object->setPublished(1);
				$object->save();
				return $object;
			} else {
				return null;
			}

		}

	}
}

?>

