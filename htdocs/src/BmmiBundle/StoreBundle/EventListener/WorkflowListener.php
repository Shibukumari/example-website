<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Doctrine\DBAL\DriverManager;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;
use \PDO;

class WorkflowListener {

	public function onPreUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "Products") {
					$workflowStatus = $e->getObject()->getWorkflowState();
					$e->getObject()->setCurrentStatus($workflowStatus[0]);
					$setting = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
					$dbhost = $setting->getDb_host();
					$dbuser = $setting->getDb_user();
					$dbpass = $setting->getDb_pass();
					$dbname = $setting->getDb_name();
					if (null != $e->getObject()->getWorkflowState()) {
						//$statusUpdate = $e->getObject()->getWorkflowState()[0];
						$statusUpdate = "new_created";
					} else {
						$statusUpdate = "new_created";
					}
					$connectionParams = array(
						'dbname' => $dbname,
						'user' => $dbuser,
						'password' => $dbpass,
						'host' => $dbhost,
						'driver' => 'pdo_mysql',
					);
					$conn = DriverManager::getConnection($connectionParams);
					$sql = "UPDATE object_query_" . $e->getObject()->getO_classId() . " SET CurrentStatus = '" . $statusUpdate . "' WHERE oo_id = :oo_id";

					$stmt = $conn->prepare($sql);
					$stmt->bindValue(':oo_id', $e->getObject()->getO_id(), PDO::PARAM_INT);
					try {
						$check = $stmt->execute();

					} catch (\Exception $e) {
						echo "something wrong" . $e->getMessage();
					}

				}

			}

		}
	}
}