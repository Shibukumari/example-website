<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;
class PriceUpdateWebsiteListener {

	public function onPostUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "BMMIProductPrice") {
					$dataSku = $e->getObject()->getSku();
					$storeid = $e->getObject()->getStoreId();
					
					$productMaster = DataObject\Products::getBySku(trim($dataSku), ['limit' => 1, 'unpublished' => true]);
					
					if (null != $productMaster) {
						$parentSku = $productMaster->getSu();
						$packSize = $productMaster->getPack();

						if ($parentSku != null) {
							$stockLocation = new DataObject\BMMIProductPrice\Listing();
							$stockLocation->addConditionParam("Sku = ?", $parentSku, "AND");
							$stockLocation->setCondition("StoreId = ?", $storeid);
							$stockLocation = $stockLocation->load();
							if (null != $stockLocation[0]) {
								$stockCount = $stockLocation[0]->getLocationStock();
								if ($stockCount > 0 && $packSize > 1) {
									$packNumber = $stockCount / $packSize;
									$e->getObject()->setLocationStock(floor($packNumber));
								}
							}
							
							
						}
					}
					

				}

			}

		}
	}
}