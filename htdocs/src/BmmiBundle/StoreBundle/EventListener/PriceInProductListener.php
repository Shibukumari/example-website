<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;

class PriceInProductListener {

	public function onPostUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "BMMIProductPrice" && $e->getObject()->getO_published()) {

					$priceInProductCron = DataObject\BMMISettings::getByCreatePriceInProducts('0', ['limit' => 1, 'unpublished' => false]);

					if (null != $priceInProductCron && $priceInProductCron->getCreatePriceInProducts() == '0') {
						$priceInProductCron->setCreatePriceInProducts('1');
						$priceInProductCron->setPriceUpdatedProduct(time());
						$priceInProductCron->save();
					}

				}

			}

		}
	}
}