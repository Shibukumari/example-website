<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;

class InventoryDeleteListener {

	public function onPostDelete(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "BMMIProductPrice" && $e->getObject()->getO_published()) {
					$magentoDetails = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
					$url = $magentoDetails->getMagentoUrlInventory();

					//create a new cURL resource
					$ch = curl_init($url);

					//setup request to send json via POST

					$data = array(
						"sku" => $e->getObject()->getSku(),
						"storeId" => $e->getObject()->getStoreId()
					);

					$storeDetails = json_encode($data);
					curl_setopt($ch, CURLOPT_POST, true);
					//attach encoded JSON string to the POST fields
					curl_setopt($ch, CURLOPT_POSTFIELDS, $storeDetails);

					//set the content type to application/json
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $magentoDetails->getMagentoKey(), 'Content-Type:application/json', 'accept: application/json'));

					//return response instead of outputting
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					//execute the POST request
					$result = curl_exec($ch);
					
					curl_close($ch);
				}

			}

		}
	}
}