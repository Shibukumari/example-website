<?php
namespace BmmiBundle\StoreBundle\EventListener;
  
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject;

class PriceListener {
     
    public function onPostUpdate (ElementEventInterface $e) {
       
        if ($e instanceof DataObjectEvent) {
            if('folder' != $e->getObject()->getO_type()) {
                if($e->getObject()->getO_className() == "BMMIProductPrice" && $e->getObject()->getO_published()){
                        
                        $priceCron = DataObject\BMMISettings::getByCreatePrice('0', ['limit' => 1,'unpublished' => false]);

                        if(null != $priceCron && $priceCron->getCreatePrice() == '0'){
                            $priceCron->setCreatePrice('1');
                            $priceCron->setPriceUpdatedTime(time());
                            $priceCron->save();
                        }

                }

            }

        }
    }
}