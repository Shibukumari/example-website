<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Bundle\AdminBundle\Security\User\UserLoader;
use Pimcore\Model\DataObject;
use Symfony\Component\EventDispatcher\GenericEvent;

class SearchListener {

	/**
	 * @var UserLoader
	 */
	protected $userLoader;

	public function __construct(UserLoader $userLoader) {
		$this->userLoader = $userLoader;
	}

	public function checkPermissions(GenericEvent $event) {

		$object = $event->getArgument("object");

		if ($object instanceof DataObject\Products) {

			$data = $event->getArgument("data");

			$user = $this->userLoader->getUser();

			/*$userPermission = $user->getPermissions();
				            $roleObject =  UserRole::getById(14);
				            $rolePermission = $roleObject->getPermissions();
			*/
			if ($object->getWorkflowState() == null) {
				$object->setWorkflowState('new_created');
			}
			if ((!$user || !$user->isAllowed($object->getWorkflowState())) && !$user->isAdmin()) {

				$data['userPermissions']['save'] = false;
				$data['userPermissions']['publish'] = false;
				$data['userPermissions']['unpublish'] = false;
				$data['userPermissions']['delete'] = false;
				$data['userPermissions']['create'] = false;
				$data['userPermissions']['rename'] = false;

			}

			$event->setArgument("data", $data);
		}

	}

}