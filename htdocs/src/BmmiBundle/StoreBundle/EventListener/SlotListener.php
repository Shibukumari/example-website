<?php
namespace BmmiBundle\StoreBundle\EventListener;
  
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject;

class SlotListener {
     
    public function onPostUpdate (ElementEventInterface $e) {
       
        if ($e instanceof DataObjectEvent) {
            if('folder' != $e->getObject()->getO_type()) {
                if(($e->getObject()->getO_className() == "BMMISlots" || $e->getObject()->getO_className() == "BMMIHolidays") && $e->getObject()->getO_published()){
                        
                        $slotCron = DataObject\BMMISettings::getByCreateSlots('0', ['limit' => 1,'unpublished' => false]);

                        if(null != $slotCron && $slotCron->getCreateSlots() == '0'){
                            $slotCron->setCreateSlots('1');
                            $slotCron->save();
                        }


                }

            }

        }
    }
}