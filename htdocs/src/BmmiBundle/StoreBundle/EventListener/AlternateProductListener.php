<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;

class AlternateProductListener {

	public function onPostUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "Products") {
					$e->getObject()->setprice(0);

					
					if ("inactive" == $e->getObject()->getWorkflowState() || "phased_out" == $e->getObject()->getWorkflowState()) {

						$e->getObject()->setStatus(false);

					}

					if (null == $e->getObject()->getWorkflowState()) {
						$e->getObject()->setWorkflowState('new_created');
					} 

				}

			}

		}
	}
}