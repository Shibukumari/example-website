<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;

class StoreListener {

	public function onPostUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "Stores" && $e->getObject()->getO_published()) {
					$magentoDetails = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
					$url = $magentoDetails->getMagentoUrl();

					//create a new cURL resource
					$ch = curl_init($url);

					//setup request to send json via POST

					$data = array(
						'source_code' => $e->getObject()->getCode(),
						"name" => $e->getObject()->getName(),
						"email" => $e->getObject()->getEmail(),
						"contact_name" => $e->getObject()->getContactPerson(),
						"enabled" => $e->getObject()->getEnabled(),
						"description" => $e->getObject()->getDescription(),
						"latitude" => $e->getObject()->getLatitude(),
						"longitude" => $e->getObject()->getLongitude(),
						"country_id" => $e->getObject()->getCountry(),
						"region_id" => $e->getObject()->getRegionId(),
						"region" => $e->getObject()->getRegion(),
						"city" => $e->getObject()->getCity(),
						"street" => $e->getObject()->getAddress(),
						"postcode" => $e->getObject()->getPincode(),
						"phone" => $e->getObject()->getPhone(),
					);
					$storeDetails = json_encode(array("source" => $data));
					curl_setopt($ch, CURLOPT_POST, true);
					//attach encoded JSON string to the POST fields
					curl_setopt($ch, CURLOPT_POSTFIELDS, $storeDetails);

					//set the content type to application/json
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $magentoDetails->getMagentoKey(), 'Content-Type:application/json', 'accept: application/json'));

					//return response instead of outputting
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					//execute the POST request
					$result = curl_exec($ch);

					//close cURL resource
					curl_close($ch);
				}

			}

		}
	}
}