<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;

class BundleListener {

	public function onPostUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "BundleProducts" && $e->getObject()->getO_published()) {
					$magentoDetails = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
					$url = $magentoDetails->getMagentoUrlBundle();

					//create a new cURL resource
					$ch = curl_init($url);

					//setup request to send json via POST
					$bundles = $e->getObject()->getBundle();

					if (null != $bundles) {
						foreach ($bundles as $bundle) {
							$product['title'] = $bundle['Title']->getData();
							$product['max_qty'] = $bundle['Count']->getData();
							$allProducts = $bundle['ProductList']->getData();
							$product['associated_sku'] = array();
							foreach ($allProducts as $allProduct) {
								$product['associated_sku'][] = $allProduct->getSku();
							}
							$allSkuDetails[] = $product;
						}
					}

					$data = array(
						'sku' => $e->getObject()->getSku(),
						"type" => $e->getObject()->getBundleType(),
						"from_time" => $e->getObject()->getFromTime()->toDateString(),
						"to_time" => $e->getObject()->getToTime()->toDateString(),
						"promotions" => $allSkuDetails,

					);
					$storeDetails = json_encode(array("promotion" => $data));
					curl_setopt($ch, CURLOPT_POST, true);
					//attach encoded JSON string to the POST fields
					curl_setopt($ch, CURLOPT_POSTFIELDS, $storeDetails);

					//set the content type to application/json
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $magentoDetails->getMagentoKey(), 'Content-Type:application/json', 'accept: application/json'));

					//return response instead of outputting
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					//execute the POST request
					$result = curl_exec($ch);

					//close cURL resource
					curl_close($ch);
				}

			}

		}
	}
}