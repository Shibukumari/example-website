<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;

class CategoryWebsiteListener {

	public function onPostUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "Category") {
					
					if (strtolower(trim($e->getObject()->getO_parent()->getO_path(), '/')) == 'base') {
						$e->getObject()->setWebsites('default');
					}else{
						$e->getObject()->setWebsites(strtolower(trim($e->getObject()->getO_parent()->getO_path(), '/')));
					}
					
					

				}

			}

		}
	}
}