<?php
namespace BmmiBundle\StoreBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject;

class RecipeListener {

	public function onPostUpdate(ElementEventInterface $e) {

		if ($e instanceof DataObjectEvent) {
			if ('folder' != $e->getObject()->getO_type()) {
				if ($e->getObject()->getO_className() == "Recipe" && $e->getObject()->getO_published() == 1) {

					$magentoDetails = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
					$url = $magentoDetails->getMagentoUrlRecipe();

					//create a new cURL resource
					$ch = curl_init($url);

					//setup request to send json via POST
					$allrecipe = $e->getObject();

					$recipeDetail = array();

					if ($allrecipe->getSku() != '' && $allrecipe->getName() != '' && $allrecipe->getDescription()) {

						$recipeDetail['sku'] = $allrecipe->getSku();
						$recipeDetail['recipeInfo']['name'] = $allrecipe->getName();
						$recipeDetail['recipeInfo']['description'] = $allrecipe->getDescription();

						$recipeDetail['recipeInfo']['serve_numbers'] = $allrecipe->getServenumbers();

						if (null != $allrecipe->getTimerequired()) {
							$totalhourstxt = '';
							if ($allrecipe->getTimerequired() >= 60) {
								$totalhours = intdiv($allrecipe->getTimerequired(), 60);
								$totalhourstxt = ($totalhours == 1 ? $totalhours . ' hour ' : $totalhours . ' hours ');
							}

							$totalmin = ($allrecipe->getTimerequired() % 60);
							$totalmintxt = ($totalmin == 1 ? $totalmin . ' min' : $totalmin . ' mins');
							$recipeDetail['recipeInfo']['time_required'] = $totalhourstxt . $totalmintxt;

						}
						if (null != $allrecipe->getInstructions()) {
							foreach ($allrecipe->getInstructions() as $instruction) {
								$allinstructions[] = $instruction['steps']->getData();
							}
							$recipeDetail['recipeInfo']['instructions'] = $allinstructions;
						}

						if ($allrecipe->getIngredients() != '') {
							foreach ($allrecipe->getIngredients() as $Ingredients) {
								$ingredients['display_name'] = $Ingredients['DisplayName']->getData();
								$ingredients['product_sku'] = $Ingredients['Product']->getData()->getSku();
								$ingredients['quantity'] = $Ingredients['Quantity']->getData();

								$recipeDetail['recipeInfo']['ingredients'][] = $ingredients;

							}
						}

						$recipeDetails = json_encode(array("data" => $recipeDetail));

						curl_setopt($ch, CURLOPT_POST, true);
						//attach encoded JSON string to the POST fields
						curl_setopt($ch, CURLOPT_POSTFIELDS, $recipeDetails);

						//set the content type to application/json
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $magentoDetails->getMagentoKey(), 'Content-Type:application/json', 'accept: application/json'));

						//return response instead of outputting
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						//execute the POST request
						$result = curl_exec($ch);

						//close cURL resource
						curl_close($ch);
					}
				}

			}

		}
	}
}