<?php
namespace BmmiBundle\StoreBundle\Website;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;

class SlotOptionsProvider implements SelectOptionsProviderInterface {
	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return array
	 */
	public function getOptions($context, $fieldDefinition) {
		$storeObj = DataObject\Stores::getByPickup('Yes');
		foreach ($storeObj as $allstore) {
			$selectOption['key'] = $allstore->getCode() . ' - ' . $allstore->getName();
			$selectOption['value'] = $allstore->getCode();
			$selectOptions[] = $selectOption;
		}
		return $selectOptions;
	}

	/**
	 * Returns the value which is defined in the 'Default value' field
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return mixed
	 */
	public function getDefaultValue($context, $fieldDefinition) {
		return $fieldDefinition->getDefaultValue();
	}

	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return bool
	 */
	public function hasStaticOptions($context, $fieldDefinition) {
		return true;
	}

}
?>