<?php
namespace BmmiBundle\StoreBundle\Website;

use Pimcore\Model\DataObject;
use Doctrine\DBAL\DriverManager;
use \PDO;

class DataBaseConnector{

	private static $instance = null;

	private $DBconn;
  	

	private function __construct()
  	{
  		$setting = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]);
  		$dbhost = $setting->getDb_host();
  		$dbuser = $setting->getDb_user();
  		$dbpass = $setting->getDb_pass();
  		$dbname = $setting->getDb_name();
    	$connectionParams = array('dbname' => $dbname,'user' => $dbuser,'password' => $dbpass,'host' => $dbhost,'driver' => 'pdo_mysql',);
		$this->DBconn = DriverManager::getConnection($connectionParams);
  	}


	public static function getInstance()
  	{
  		if(!self::$instance)
	    {
	      self::$instance = new DataBaseConnector();
	    }

	    return self::$instance;

  	}

  	public function connectDB()
  	{
    	return $this->DBconn;
  	}

}