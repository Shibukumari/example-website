<?php
namespace BmmiBundle\StoreBundle\Website;

use Pimcore\Model\Asset\Image;

class ValidationFields {

	public function inputFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty ' . PHP_EOL;
		}
	}

	public function numericFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty ' . PHP_EOL;
		} elseif (!is_numeric($csvValue) && null != $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should be number ' . PHP_EOL;
		}
	}

	public function textareaFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty ' . PHP_EOL;
		}
	}

	public function wysiwygFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty ' . PHP_EOL;
		}
	}

	public function booleanSelectFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty ';
		} elseif (1 != $csvValue && 0 != $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should be 1 or 0' . PHP_EOL;
		}
	}

	public function selectFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty' . PHP_EOL;
		}
	}

	public function multiselectFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty' . PHP_EOL;
		}
	}

	public function imageFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($csvValue != '') {
			$image_value = Image::getByPath($csvValue);
			if (null == $image_value) {
				return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' not found;' . $csvValue . PHP_EOL;
			}
		}

	}
	public function imageGalleryFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($csvValue != '') {
			$all_media_gallery = explode("||", trim($csvValue, "||"));

			if (!empty($all_media_gallery)) {
				foreach ($all_media_gallery as $media_gallery) {

					$image_value = Image::getByPath($media_gallery);
					if (null == $image_value) {
						return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' not found;' . $media_gallery . PHP_EOL;
					}

				}
			}
		}

	}

	public function dateFormat($csvKey, $csvValue, $mandatoryCol = 0, $coloumNumber) {
		if ($mandatoryCol == 1 && null == $csvValue) {
			return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should not be empty ' . PHP_EOL;
		} elseif ($csvValue != '') {
			$dateValue = new \Carbon\Carbon();
			$dateValue->setTimestamp(strtotime($csvValue));
			if (!is_numeric($csvValue)) {
				return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' should be date ' . PHP_EOL;
			}
		}
	}

	public function manyToManyObjectRelationFormat($csvKey, $attrValue, $mandatoryCol = 0, $coloumNumber) {
		if ($attrValue != '' && $csvKey == 'category_ids') {

			$categorys = explode("||", trim($attrValue, "||"));

			foreach ($categorys as $category) {

				$classname = 'Pimcore\\Model\\DataObject\\Category';
				$categoryObj = $classname::getByPath(trim($category));

				if (null == $categoryObj) {
					return 'CSV record ' . $coloumNumber . ';' . $csvKey . ' path is not correct;' . $category . PHP_EOL;

				}

			}
		}
	}

}
?>