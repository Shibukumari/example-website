<?php
namespace BmmiBundle\StoreBundle\Website;

use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
use Pimcore\Model\Property\Predefined;

class WebsitesOptionsProvider implements SelectOptionsProviderInterface {
	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return array
	 */
	public function getOptions($context, $fieldDefinition) {

		$websites = Predefined::getByKey('websites')->getConfig();
		$allWebsites = explode(",", $websites);
		foreach ($allWebsites as $allWebsite) {
			$selectOption['key'] = ucfirst(trim($allWebsite));
			$selectOption['value'] = trim($allWebsite);
			$selectOptions[] = $selectOption;
		}
		return $selectOptions;
	}

	/**
	 * Returns the value which is defined in the 'Default value' field
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return mixed
	 */
	public function getDefaultValue($context, $fieldDefinition) {
		return $fieldDefinition->getDefaultValue();
	}

	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return bool
	 */
	public function hasStaticOptions($context, $fieldDefinition) {
		return true;
	}

}
?>