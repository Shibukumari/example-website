<?php
namespace BmmiBundle\StoreBundle\Website;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;

class ZoneGroupProvider implements SelectOptionsProviderInterface {
	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return array
	 */
	public function getOptions($context, $fieldDefinition) {
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$settings = DataObject\BMMISettings::getByEnable('1', ['limit' => 1, 'unpublished' => false]); /*Server config details*/

		foreach ($settings->getZoneNames() as $allzoneGroups) {


			$allZoneGroupObj = 'Pimcore\\Model\\DataObject\\' . $allzoneGroups['zoneClassNames']->getData() . '\\Listing';

			$allZoneGroupObjs = new $allZoneGroupObj();
			$allZoneGroupObjs->setUnpublished(false);
			$allZoneGroupList[] = $allZoneGroupObjs->load();

		}

		foreach ($allZoneGroupList as $zoneGroupObj) {

			foreach ($zoneGroupObj as $zoneGroup) {
				if (null != $zoneGroup->getZoneGroupName()) {
					$selectOption['key'] = $zoneGroup->getZoneGroupName() . ' - ' . $zoneGroup->getO_className();
					$selectOption['value'] = $zoneGroup->getZoneGroupName() . ' - ' . $zoneGroup->getO_className();
					$selectOptions[] = $selectOption;
				}
				
			}
		}
		return $selectOptions;
	}

	/**
	 * Returns the value which is defined in the 'Default value' field
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return mixed
	 */
	public function getDefaultValue($context, $fieldDefinition) {
		return $fieldDefinition->getDefaultValue();
	}

	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return bool
	 */
	public function hasStaticOptions($context, $fieldDefinition) {
		return true;
	}

}
?>