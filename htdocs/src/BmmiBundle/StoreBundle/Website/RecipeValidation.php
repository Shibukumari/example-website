<?php
namespace BmmiBundle\StoreBundle\Website;

class RecipeValidation {
	public $errorReport = '';
	public function checkAttribute($csvfinaldata, $fieldDatatype) {
		\Pimcore\Model\DataObject\AbstractObject::setHideUnpublished(true);
		$allSelectOptions = array();
		$simple_product_class = 'Pimcore\\Model\\DataObject\\Products';
		foreach ($csvfinaldata as $csvId => $productData) {
			echo 'Validating ' . $csvId . ' ';
			if (!isset($productData['sku'])) {
				$this->$errorReport .= 'CSV record have SKU / PATH issue with ' . ($csvId + 2) . PHP_EOL;
				continue;
			}
			//|| !isset($productData['fullpath'])
			foreach ($productData as $csvKey => $csvValue) {

				$csvValue = trim($csvValue);
				$valueValidate = new ValidationFields();
				$coloumNumber = $csvId + 2;

				try {

					if ($csvKey != 'steps' && $csvKey != 'DisplayName' && $csvKey != 'Product' && $csvKey != 'Quantity' && $csvKey != 'recipe_folder') {
						if (isset($fieldDatatype[$csvKey])) {
							$datetypeCol = $fieldDatatype[$csvKey]->getFieldType() . 'Format';
							$mandatoryCol = $fieldDatatype[$csvKey]->getMandatory();
						} else {
							continue;
						}

					} else {

						if ($csvKey == 'DisplayName') {
							$allDisplayValues = explode("||", trim($csvValue, "||"));
							$allProductValues = explode("||", trim($productData['Product'], "||"));
							$allQuantityValues = explode("||", trim($productData['Quantity'], "||"));

							$displayValues = array_filter($allDisplayValues);
							$productValues = array_filter($allProductValues);
							$quantityValues = array_filter($allQuantityValues);

							if ((count($displayValues) == count($productValues)) && (count($displayValues) == count($quantityValues))) {
								foreach ($productValues as $productkey => $productData) {
									$productObj = $simple_product_class::getBySku(trim($productData), ['limit' => 1, 'unpublished' => false]);
									if (null == $productObj) {
										$this->$errorReport .= 'CSV record ' . $coloumNumber . '; Please Check Product Is Exist;' . $productData . PHP_EOL;
										continue;
									}
								}
							} else {
								$this->$errorReport .= 'CSV record ' . $coloumNumber . '; DisplayName : Product : Quantity count is not equal;' . PHP_EOL;
								continue;
							}

						} elseif ($csvKey == 'recipe_folder') {
							$folderExists = \Pimcore\Model\DataObject::getByPath($csvValue);
							$parentFolderId = \Pimcore\Model\DataObject\Folder::getByPath(dirname($csvValue))->getId();

							if (null == $folderExists) {

								$this->$errorReport .= 'CSV record ' . $coloumNumber . '; Please check the folder path;' . $csvValue . PHP_EOL;
							} else {
								echo "not";
							}
						} else {
							continue;
						}

					}
				} catch (\Exception $e) {
					$this->$errorReport .= 'CSV record have issue with ' . $csvKey . ' \n';
				}

				if (null == $datetypeCol) {continue;}

				if ($datetypeCol == 'selectFormat' && $csvValue != null) {
					if ($fieldDatatype[$csvKey]->getoptionsProviderClass() != null) {
						$className = preg_replace("/[^a-zA-Z]/", "", $csvKey);

						if (!isset($allSelectOptions[$className])) {
							$attributeName = 'Pimcore\\Model\\DataObject\\' . ucfirst($className) . '\\Listing';
							$selectLists = new $attributeName();
							$attrSelectLists = $selectLists->load();

							if (isset($attrSelectLists[0])) {

								foreach ($attrSelectLists as $optionList) {

									$selectOption['value'] = $optionList->getSelectValue();
									$selectOptions[] = $selectOption;
									$allSelectOptions[$className] = $selectOptions;

								}

							}
						}
						if (is_array($allSelectOptions[$className])) {
							if (!in_array($csvValue, array_column($allSelectOptions[$className], 'value'))) {

								$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in select list;' . $csvValue . PHP_EOL;
							}
						}

					} else {

						if (!in_array($csvValue, array_column($fieldDatatype[$csvKey]->getOptions(), 'value'))) {
							$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in select list;' . $csvValue . PHP_EOL;
						}
					}

				}

				if ($datetypeCol == 'multiselectFormat' && $csvValue != null) {
					$allcsvValues = explode("||", trim($csvValue, "||"));

					if ($fieldDatatype[$csvKey]->getoptionsProviderClass() != null) {
						$className = preg_replace("/[^a-zA-Z]/", "", $csvKey);
						if (!isset($allSelectOptions[$className])) {
							$attributeName = 'Pimcore\\Model\\DataObject\\' . ucfirst($className) . '\\Listing';
							$selectLists = new $attributeName();
							$attrSelectLists = $selectLists->load();

							if (isset($attrSelectLists[0])) {

								foreach ($attrSelectLists as $optionList) {

									$selectOption['value'] = $optionList->getSelectValue();
									$selectOptions[] = $selectOption;
									$allSelectOptions[$className] = $selectOptions;

								}

							}
						}

					}

					foreach ($allcsvValues as $allcsvValue) {

						if ($fieldDatatype[$csvKey]->getoptionsProviderClass() != null) {
							if (is_array($allSelectOptions[$className])) {
								if (!in_array($allcsvValue, array_column($allSelectOptions[$className], 'value'))) {
									$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in multiselect list;' . $allcsvValue . PHP_EOL;
									continue;
								}
							}

						} else {
							if (!in_array($allcsvValue, array_column($fieldDatatype[$csvKey]->getOptions(), 'value'))) {
								$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in multiselect list;' . $allcsvValue . PHP_EOL;
								continue;
							}

						}

					}

				}

				$this->$errorReport .= $valueValidate->$datetypeCol($csvKey, $csvValue, $mandatoryCol, $coloumNumber);

			}
		}
		return $this->$errorReport;

	}
}