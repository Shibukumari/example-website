<?php
namespace BmmiBundle\StoreBundle\Website;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;

class ProductsOptionProvider implements SelectOptionsProviderInterface {
	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return array
	 */
	public function getOptions($context, $fieldDefinition) {

		$className = preg_replace("/[^a-zA-Z]/", "", $fieldDefinition->getName());
		$attributeName = 'Pimcore\\Model\\DataObject\\' . ucfirst($className) . '\\Listing';
		$selectLists = new $attributeName();
		$attrSelectLists = $selectLists->load();
		if (isset($attrSelectLists[0])) {
		foreach ($attrSelectLists as $attrList) {

			$selectOption['key'] = $attrList->getSelectValue();
			$selectOption['value'] = $attrList->getSelectValue();
			$selectOptions[] = $selectOption;
		}
		return $selectOptions;
		}
		
	}

	/**
	 * Returns the value which is defined in the 'Default value' field
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return mixed
	 */
	public function getDefaultValue($context, $fieldDefinition) {
		return $fieldDefinition->getDefaultValue();
	}

	/**
	 * @param $context array
	 * @param $fieldDefinition Data
	 * @return bool
	 */
	public function hasStaticOptions($context, $fieldDefinition) {
		return true;
	}

}
?>