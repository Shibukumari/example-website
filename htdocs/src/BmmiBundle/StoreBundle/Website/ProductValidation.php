<?php
namespace BmmiBundle\StoreBundle\Website;

use BmmiBundle\StoreBundle\Website\ValidationFields;

class ProductValidation {

	public $errorReport = '';
	public function checkAttribute($csvfinaldata, $fieldDatatype) {
		$allSelectOptions = array();
		foreach ($csvfinaldata as $csvId => $productData) {
			echo 'Validating ' . $csvId . ' ';
			if (!isset($productData['sku']) || !isset($productData['product_folder'])) {
				$this->$errorReport .= 'CSV record have SKU / FOLDER NAME issue with ' . ($csvId + 2) . PHP_EOL;
				continue;
			}

			foreach ($productData as $csvKey => $csvValue) {
				$csvValue = trim($csvValue);
				try {
					if ($csvKey != 'status' && $csvKey != 'workflowState') {
						if (isset($fieldDatatype[$csvKey])) {
							$datetypeCol = $fieldDatatype[$csvKey]->getFieldType() . 'Format';
							$mandatoryCol = $fieldDatatype[$csvKey]->getMandatory();
						} else {
							continue;
						}

					} else {
						continue;
					}

				} catch (\Exception $e) {
					$this->$errorReport .= 'CSV record have issue with ' . $csvKey . ' \n';
				}

				if (null == $datetypeCol) {continue;}

				$valueValidate = new ValidationFields();
				$coloumNumber = $csvId + 2;

				if ($csvKey == 'product_folder') {
					if ($csvValue != "BMMIShops" || $csvValue != "Alosra") {
						$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not correct.' . PHP_EOL;
						continue;
					} else {
						continue;
					}
				}

				if ($datetypeCol == 'selectFormat' && $csvValue != null) {
					if ($fieldDatatype[$csvKey]->getoptionsProviderClass() != null) {
						$className = preg_replace("/[^a-zA-Z]/", "", $csvKey);

						if (!isset($allSelectOptions[$className])) {
							$attributeName = 'Pimcore\\Model\\DataObject\\' . ucfirst($className) . '\\Listing';
							$selectLists = new $attributeName();
							$attrSelectLists = $selectLists->load();

							if (isset($attrSelectLists[0])) {

								foreach ($attrSelectLists as $optionList) {

									$selectOption['value'] = $optionList->getSelectValue();
									$selectOptions[] = $selectOption;
									$allSelectOptions[$className] = $selectOptions;

								}

							}
						}
						if (is_array($allSelectOptions[$className])) {
							if (!in_array($csvValue, array_column($allSelectOptions[$className], 'value'))) {

								$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in select list;' . $csvValue . PHP_EOL;
							}
						}

					} else {

						if (!in_array($csvValue, array_column($fieldDatatype[$csvKey]->getOptions(), 'value'))) {
							$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in select list;' . $csvValue . PHP_EOL;
						}
					}

				}

				if ($datetypeCol == 'multiselectFormat' && $csvValue != null) {
					$allcsvValues = explode("||", trim($csvValue, "||"));

					if ($fieldDatatype[$csvKey]->getoptionsProviderClass() != null) {
						$className = preg_replace("/[^a-zA-Z]/", "", $csvKey);
						if (!isset($allSelectOptions[$className])) {
							$attributeName = 'Pimcore\\Model\\DataObject\\' . ucfirst($className) . '\\Listing';
							$selectLists = new $attributeName();
							$attrSelectLists = $selectLists->load();

							if (isset($attrSelectLists[0])) {

								foreach ($attrSelectLists as $optionList) {

									$selectOption['value'] = $optionList->getSelectValue();
									$selectOptions[] = $selectOption;
									$allSelectOptions[$className] = $selectOptions;

								}

							}
						}

					}

					foreach ($allcsvValues as $allcsvValue) {

						if ($fieldDatatype[$csvKey]->getoptionsProviderClass() != null) {
							if (is_array($allSelectOptions[$className])) {
								if (!in_array($allcsvValue, array_column($allSelectOptions[$className], 'value'))) {
									$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in multiselect list;' . $allcsvValue . PHP_EOL;
									continue;
								}
							}

						} else {
							if (!in_array($allcsvValue, array_column($fieldDatatype[$csvKey]->getOptions(), 'value'))) {
								$this->$errorReport .= 'CSV record ' . $coloumNumber . ';' . $csvKey . ' is not in multiselect list;' . $allcsvValue . PHP_EOL;
								continue;
							}

						}

					}

				}

				$this->$errorReport .= $valueValidate->$datetypeCol($csvKey, $csvValue, $mandatoryCol, $coloumNumber);

			}
		}
		return $this->$errorReport;

	}

}